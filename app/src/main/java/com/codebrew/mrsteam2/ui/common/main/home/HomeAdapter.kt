package com.codebrew.mrsteam2.ui.common.main.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.main.common.OfferFeaturesAdapter
import com.codebrew.mrsteam2.ui.common.main.offerdetails.OfferDetailsActivity
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.ui.driver.servicerequest.ServiceProgressActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.item_rv_home_offers.view.*
import kotlinx.android.synthetic.main.item_rv_home_wash.view.*
import java.io.Serializable


class HomeAdapter(private val fragment: Fragment?, private val type: String) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listOffers = ArrayList<Offers>()
    private val listRecent = ArrayList<Wash>()
    private val listUpcoming = ArrayList<Wash>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (type) {
            OFFERS -> {
                val v1 = View.inflate(parent.context, R.layout.item_rv_home_offers, null)
                ViewHolderOffers(v1)
            }
            UPCOMING_WASH -> {
                val v1 = View.inflate(parent.context, R.layout.item_rv_home_wash, null)
                ViewHolderUpcoming(v1)
            }
            else -> {
                val v1 = View.inflate(parent.context, R.layout.item_rv_home_wash, null)
                ViewHolderRecent(v1)
            }
        }
    }

    override fun getItemCount(): Int {
        return when (type) {
            OFFERS -> listOffers.size
            UPCOMING_WASH -> listUpcoming.size
            else -> listRecent.size
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (type) {
            OFFERS -> {
                (holder as ViewHolderOffers).bind(listOffers[position])
            }
            UPCOMING_WASH -> {
                (holder as ViewHolderUpcoming).bind(listUpcoming[position])
            }
            else -> {
                (holder as ViewHolderRecent).bind(listRecent[position])
            }
        }
    }

    inner class ViewHolderOffers(item: View) : RecyclerView.ViewHolder(item) {
        init {
            itemView.layoutBack.setOnClickListener {
                val res = listOffers[adapterPosition]
                val i1 = Intent(itemView.context, OfferDetailsActivity::class.java)
                i1.putExtra(OFFERS_DATA, res)
                i1.putExtra(OFFER_STATUS,true)
                item.context.startActivity(i1)
            }
        }

        fun bind(data: Offers) = with(itemView) {

            when (adapterPosition % 4) {
                0 -> {
                    layoutBack.setBackgroundResource(R.drawable.offer_basic_background)
                }
                1 -> {
                    layoutBack.setBackgroundResource(R.drawable.offer_economic_background)
                }
                2 -> {
                    layoutBack.setBackgroundResource(R.drawable.offer_vip_background)
                }
                else -> {
                    layoutBack.setBackgroundResource(R.drawable.offer_premium_background)
                }
            }

            val featuresAdapter = OfferFeaturesAdapter(true)
            rvFeatures.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            rvFeatures.adapter = featuresAdapter
            featuresAdapter.setData(data.active_sub_services ?: ArrayList())

            tvCategory.text = data.dual_name
            tvPrice.text = "$ ${data.price}"
            tvOfferValid.text = "Offer valid for next ${data.valid_days} days"
        }
    }

    inner class ViewHolderUpcoming(item: View) : RecyclerView.ViewHolder(item) {
        init {
            itemView.layoutWashBack.setOnClickListener {
                if (isConnectedToInternet(item.context, true)) {
                    val detail = listUpcoming[adapterPosition]
                    if (detail.status == ServiceStatus.COMPLETED) {

                        if (detail.is_reviewed == true)
                            item.context.startActivity(
                                Intent(itemView.context, DetailActivity::class.java)
                                    .putExtra(DETAIL_DATA, detail)
                                    .putExtra("isHomeDataUpdate", false)
                            )
                        else {
                            item.context.startActivity(
                                Intent(itemView.context, RateReviewActivity::class.java)
                                    .putExtra(DETAIL_DATA, detail)
                            )
                        }

                    } else {
                        val activity = if (PrefsManager.get().getObject(
                                SP_USER_DATA,
                                UserData::class.java
                            )?.role == UserType.DRIVER
                        )
                            ServiceProgressActivity::class.java
                        else
                            BookingActivity::class.java

                        if (fragment != null)
                            fragment.startActivityForResult(
                                Intent(itemView.context, activity)
                                    .putExtra(DETAIL_DATA, detail)
                                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), AppRequestCode.HOME_UPDATE
                            )
                        else
                            (item.context as Activity).startActivityForResult(
                                Intent(itemView.context, activity)
                                    .putExtra(DETAIL_DATA, detail)
                                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), AppRequestCode.HOME_UPDATE
                            )
                    }
                }
            }
        }

        fun bind(data: Wash) = with(itemView) {
            loadImage(
                context as Activity, ivPicture, data.user?.image_url?.original,
                data.user?.image_url?.thumbnail
            )
            tvName.text = data.user?.name
            tvDate.text = formatDate(data.created_at, "dd-MMM-yyyy").toString()
        }
    }

    inner class ViewHolderRecent(item: View) : RecyclerView.ViewHolder(item) {
        init {
            itemView.layoutWashBack.setOnClickListener {

                val detail = listRecent[adapterPosition]
                val i1 = Intent(itemView.context, DetailActivity::class.java)
                i1.putExtra(DETAIL_DATA, detail)
                i1.putExtra("isHomeDataUpdate", false)
                item.context.startActivity(i1)
            }
        }

        fun bind(data: Wash) = with(itemView) {
            loadImage(context as Activity, ivPicture, data.user?.image_url?.original, data.user?.image_url?.thumbnail)
            tvName.text = data.user?.name
            tvDate.text = formatDate(data.created_at, "dd-MMM-yyyy").toString()
        }
    }


    fun setDataOffers(list1: ArrayList<Offers>) {
        listOffers.clear()
        listOffers.addAll(list1)
        notifyDataSetChanged()
    }

    fun setDataRecent(list1: ArrayList<Wash>) {
        listRecent.clear()
        listRecent.addAll(list1)
        notifyDataSetChanged()
    }

    fun setDataUpcoming(list1: ArrayList<Wash>) {
        listUpcoming.clear()
        listUpcoming.addAll(list1)
        notifyDataSetChanged()
    }

}


