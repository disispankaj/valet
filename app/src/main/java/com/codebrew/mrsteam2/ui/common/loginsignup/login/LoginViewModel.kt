package com.codebrew.mrsteam2.ui.common.loginsignup.login


import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.LoginRequest
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginViewModel : ViewModel() {
    val loginRes by lazy { SingleLiveEvent<Resource<UserData>>() }

    fun logIn(login: LoginRequest) {

        loginRes.value = Resource.loading()

        RetrofitClient.getApi()
                .logIn(login)
                .enqueue(object : Callback<ApiResponse<UserData>> {
                    override fun onFailure(call: Call<ApiResponse<UserData>>, throwable: Throwable) {
                        loginRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<UserData>>, response: Response<ApiResponse<UserData>>) {
                        if (response.isSuccessful) {
                            loginRes.value = Resource.success(response.body()?.data)
                        } else {
                            loginRes.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }
                })
    }


}