package com.codebrew.mrsteam2.ui.common.main.home

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.Home
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.ui.common.main.home.offers.OffersActivity
import com.codebrew.mrsteam2.ui.common.main.home.recentwash.RecentWashActivity
import com.codebrew.mrsteam2.ui.common.main.home.upcomingwash.UpcomingWashActivity
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar.AddCarActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.fragment_main_home.*

class HomeFragment : Fragment() {
    private val offerAdapter = HomeAdapter(this, OFFERS)
    private val recentAdapter = HomeAdapter(this, RECENT_WASH)
    private val upcomingAdapter = HomeAdapter(this, UPCOMING_WASH)
    private lateinit var viewModel: HomeViewModel
    private lateinit var progressDialog: ProgressDialog


    private val broadCastReciver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            offerApiHit()
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.registerReceiver(broadCastReciver, IntentFilter("refreshHomeData"))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_main_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapterSetting()
        initialise()
        listeners()
        offerApiHit()
        liveData()

    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[HomeViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

        if (PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)?.role == UserType.CUSTOMER) {
            clCustomer.visible()
        } else {
            clCustomer.gone()
        }
    }

    private fun listeners() {
        ivMyCars.setOnClickListener {
            val i1 = Intent(activity as Activity, AddCarActivity::class.java)
            startActivity(i1)
        }
        tvOffersViewAll.setOnClickListener {
            val i1 = Intent(activity as Activity, OffersActivity::class.java)
            startActivityForResult(i1, AppRequestCode.HOME_UPDATE)
        }
        tvRecentWashViewAll.setOnClickListener {
            val i1 = Intent(activity as Activity, RecentWashActivity::class.java)
            startActivity(i1)
        }
        tvUpcomingWashViewAll.setOnClickListener {
            val i1 = Intent(activity as Activity, UpcomingWashActivity::class.java)
            startActivityForResult(i1, AppRequestCode.HOME_UPDATE)
        }
        swipeRefreshLayout.setOnRefreshListener {
            offerApiHit()
        }
    }

    private fun adapterSetting() {

        rvOffers.layoutManager = LinearLayoutManager(activity as Activity, RecyclerView.HORIZONTAL, false)
        rvOffers.adapter = offerAdapter

        rvUpcomingWash.layoutManager = LinearLayoutManager(activity as Activity)
        rvUpcomingWash.adapter = upcomingAdapter

        rvRecentWash.layoutManager = LinearLayoutManager(activity as Activity)
        rvRecentWash.adapter = recentAdapter
    }

    private fun offerApiHit() {
        if (isConnectedToInternet(activity as Activity, true))
            viewModel.home()
    }

    private fun liveData() {
        viewModel.homeRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    swipeRefreshLayout.isRefreshing = false

                    checkVisibility(resources.data)

                    offerAdapter.setDataOffers(resources.data?.offers ?: ArrayList())
                    recentAdapter.setDataRecent(resources.data?.recent_washes ?: ArrayList())
                    upcomingAdapter.setDataUpcoming(resources.data?.upcoming_washes ?: ArrayList())
                    (activity as MainActivity).setNotificationCount(resources.data?.unread_notifications_count.toString())
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    swipeRefreshLayout.isRefreshing = false
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }

                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })
    }

    private fun checkVisibility(data: Home?) {
        if (data?.offers_count == 0) {
            tvNoOffers.visible()
        } else {
            tvNoOffers.gone()
            if (data?.offers_count ?: 0 > 3)
                tvOffersViewAll.visible()
            else
                tvOffersViewAll.gone()
        }

        if (data?.upcoming_washes_count == 0) {
            tvNoUpcomingWash.visible()
        } else {
            tvNoUpcomingWash.gone()
            if (data?.upcoming_washes_count ?: 0 > 3)
                tvUpcomingWashViewAll.visible()
            else
                tvUpcomingWashViewAll.gone()
        }

        if (data?.recent_washes_count == 0) {
            tvNoRecentWash.visible()
        } else {
            tvNoRecentWash.gone()
            if (data?.recent_washes_count ?: 0 > 3)
                tvRecentWashViewAll.visible()
            else
                tvRecentWashViewAll.gone()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppRequestCode.HOME_UPDATE) {
                swipeRefreshLayout.isRefreshing = true
                offerApiHit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        activity?.unregisterReceiver(broadCastReciver)
    }
}