package com.codebrew.mrsteam2.ui.common.loginsignup.verifynumber

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.VerifyNumberRequest
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_signup_login.*
import kotlinx.android.synthetic.main.fragment_verify_number.*


class VerifyNumberFragment : Fragment() {

    private var argsUserId: String? = null

    private lateinit var viewModel: VerifyNumberViewModel
    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_verify_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        argsUserId = arguments?.getString(USER_ID)

        listener()
        initialise()
        liveData()

    }

    private fun listener() {
        btnContinue.setOnClickListener {
            if (etCode.text.toString().trim().length == 6) {
                verifyNumberApiHit()
            } else {
                tilCode.error = getString(R.string.str_OTP_error)
            }
        }

        tvResendOtp.setOnClickListener {
            resendOtpApiHit()
        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[VerifyNumberViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

//        (activity as LoginSignupActivity).ivBack.invisible()
    }

    private fun verifyNumberApiHit() {
        val verifyNumReq =
                VerifyNumberRequest(
                        otp = etCode.text.toString().trim(),
                        user_id = argsUserId
                )

        if (isConnectedToInternet(activity as Activity, true))
            viewModel.verifyNumber(verifyNumReq)
    }

    private fun liveData() {
        viewModel.verifyNumRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    PrefsManager.get().save(SP_USER_DATA, resources.data ?: UserData())
                    PrefsManager.get().save(ACCESS_TOKEN, resources.data?.token ?: "")

                    startActivity(
                            Intent(context, MainActivity::class.java)
                                    .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    )
                    activity?.finish()
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModel.resendOtpRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    argsUserId = resources.data?.user_id
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

    private fun resendOtpApiHit() {
        if (isConnectedToInternet(activity, true))
            viewModel.resendOtp()
    }
}