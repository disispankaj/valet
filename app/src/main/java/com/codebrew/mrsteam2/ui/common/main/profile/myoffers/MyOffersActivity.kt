package com.codebrew.mrsteam2.ui.common.main.profile.myoffers

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.common.OffersAdapter
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_my_offers.*

class MyOffersActivity : AppCompatActivity() {

    private lateinit var viewModel: MyOffersViewModel
    private lateinit var progressDialog: ProgressDialog

    private val myOffersAdapter = OffersAdapter(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_offers)
        adapterSetting()
        initialise()
        listener()
        offerApiHit()
        liveData()
    }

    private fun listener() {
        tbrMyOffers.setNavigationOnClickListener {
            onBackPressed()
        }

        swipe.isEnabled = false
    }

    private fun adapterSetting() {
        rvMyOffers.layoutManager = LinearLayoutManager(this@MyOffersActivity, RecyclerView.VERTICAL, false)
        rvMyOffers.adapter = myOffersAdapter
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[MyOffersViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }

    private fun offerApiHit() {
        if (isConnectedToInternet(this, true))
            viewModel.myOffers()
    }

    private fun liveData() {
        viewModel.myOfferRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    if (resources.data?.isNotEmpty() == true) {
                        tvNoData.visibility = View.GONE
                        myOffersAdapter.setData(resources.data ?: ArrayList())
                    } else {
                        tvNoData.visibility = View.VISIBLE
                    }

                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

    }


}