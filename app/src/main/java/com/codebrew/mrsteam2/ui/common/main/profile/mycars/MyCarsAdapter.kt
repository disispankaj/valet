package com.codebrew.mrsteam2.ui.common.main.profile.mycars

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar.AddCarActivity
import com.codebrew.mrsteam2.utils.AppRequestCode
import com.codebrew.mrsteam2.utils.EXTRA_CAR
import com.codebrew.mrsteam2.utils.VehicleType
import com.codebrew.mrsteam2.utils.getVehicleType
import kotlinx.android.synthetic.main.item_rv_my_cars.view.*

class MyCarsAdapter(private val items: ArrayList<MyCars>) : RecyclerView.Adapter<MyCarsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_rv_my_cars, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(data: MyCars) = with(itemView) {
            tvName.text = data.model
            tvYear.text = data.year.toString()
            tvPlateNumber.text = data.plate_number

            tvVehicleType.text = context.getVehicleType(data.vehicle_type
                    ?: VehicleType.SMALL.toString())

           // ivCarColor.setImageResource(context.getVehicleColor(data.color?.toInt() ?: 0))

            ivEdit.setOnClickListener {
                val i1 = Intent(context, AddCarActivity::class.java)
                        .putExtra(EXTRA_CAR, items[adapterPosition])
                (context as MyCarsActivity).startActivityForResult(i1, AppRequestCode.ADD_CAR)
            }

            ivDelete.setOnClickListener {
                (context as MyCarsActivity).deleteCar(adapterPosition)
            }
        }
    }

}

