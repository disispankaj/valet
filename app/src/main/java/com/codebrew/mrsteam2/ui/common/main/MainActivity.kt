package com.codebrew.mrsteam2.ui.common.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.home.HomeFragment
import com.codebrew.mrsteam2.ui.common.main.home.notification.NotificationActivity
import com.codebrew.mrsteam2.ui.common.main.profile.ProfileFragment
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.driver.main.history.HistoryFragment
import com.codebrew.mrsteam2.ui.driver.servicerequest.ServiceProgressActivity
import com.codebrew.mrsteam2.utils.*
import com.google.gson.Gson
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    private var userType: String? = null

    private var homeFragment: HomeFragment? = null

    private var historyFragment: HistoryFragment? = null

    private var profileFragment: ProfileFragment? = null

    private lateinit var viewModel: WashRequestViewModel

    private lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavSettings()

        homeFragment = HomeFragment()
        supportFragmentManager.beginTransaction().add(
            R.id.layoutFragmentData,
            homeFragment ?: HomeFragment()
        ).commit()

        listener()
        initialise()
        liveData()
    }

    private fun initialise() {
        AppSocket.get().init(this)
        AppSocket.get().on("bookingStatus", orderEventListener)

        viewModel = ViewModelProviders.of(this)[WashRequestViewModel::class.java]
        progressDialog = ProgressDialog(this)

        if (isConnectedToInternet(this, true))
            viewModel.myOrder()
    }

    private fun liveData() {
        viewModel.washRequests.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    for (request in it.data?.upcoming_requests ?: ArrayList()) {
                        val activity = if (PrefsManager.get().getObject(
                                SP_USER_DATA,
                                UserData::class.java
                            )?.role == UserType.DRIVER
                        )
                            ServiceProgressActivity::class.java
                        else
                            BookingActivity::class.java

                        startActivity(
                            Intent(this, activity)
                                .putExtra(ORDER_PROGRESS, Gson().toJson(request))
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        )

                        break
                    }

                    for (request in it.data?.searching_requests ?: ArrayList()) {
                        val activity = if (PrefsManager.get().getObject(
                                SP_USER_DATA,
                                UserData::class.java
                            )?.role == UserType.DRIVER
                        )
                            ServiceProgressActivity::class.java
                        else
                            BookingActivity::class.java

                        startActivity(
                            Intent(this, activity)
                                .putExtra(ORDER_PROGRESS, Gson().toJson(request))
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        )
                    }
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(it.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }

        })
    }

    private fun listener() {
        ivMainTitle.setOnClickListener {
            navMain.menu.getItem(0).isChecked = true

            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.layoutFragmentData,
                    homeFragment ?: HomeFragment()
                ).addToBackStack("").commit()
        }
        ivNotificationIcon.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }
    }

    private fun bottomNavSettings() {
        userType = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)?.role
        if (userType == UserType.CUSTOMER)
            navMain.inflateMenu(R.menu.navigation_view_items_customer)
        else
            navMain.inflateMenu(R.menu.navigation_view_items_driver)


        navMain.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_home -> {
                    supportFragmentManager.beginTransaction().replace(
                        R.id.layoutFragmentData,
                        homeFragment ?: HomeFragment()
                    ).commit()
                }
                R.id.item_booking -> {
                    if (userType == UserType.CUSTOMER) {
                        startActivity(
                            Intent(this, BookingActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        )
                    } else {
                        if (historyFragment == null)
                            historyFragment = HistoryFragment()

                        supportFragmentManager.beginTransaction()
                            .replace(
                                R.id.layoutFragmentData,
                                historyFragment ?: HistoryFragment()
                            ).commit()
                    }
                }
                R.id.item_profile -> {
                    if (profileFragment == null)
                        profileFragment = ProfileFragment()

                    supportFragmentManager.beginTransaction()
                        .replace(
                            R.id.layoutFragmentData,
                            profileFragment ?: ProfileFragment(),
                            ProfileFragment::class.java.name
                        ).commit()
                }
            }
            true
        }
    }




    private val orderEventListener = Emitter.Listener { args ->
        Log.i("SockectConnect", "Listen event MainActivity")
        runOnUiThread {
            /*if (JSONObject(args[0].toString()).has("message")) {

            } else {

            }*/

            val request = Gson().fromJson(JSONObject(args[0].toString()).toString(), Wash::class.java)

            when (request.status) {
                ServiceStatus.REQUEST, ServiceStatus.CANCLE_BY_USER ->
                    startActivity(
                        Intent(this, ServiceProgressActivity::class.java)
                            .putExtra(ORDER_PROGRESS, Gson().toJson(request))
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    )

                ServiceStatus.ACCEPT, ServiceStatus.JOURNEY_STARTED,
                ServiceStatus.WASHING_STARTED, ServiceStatus.COMPLETED ->
                    startActivity(
                        Intent(this, BookingActivity::class.java)
                            .putExtra(ORDER_PROGRESS, Gson().toJson(request))
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    )
            }


            /*  val fragment = ServiceRequestFragment()
              val bundle = Bundle()

              bundle.putString("order", Gson().toJson(order))
              fragment.arguments = bundle
              //if(supportFragmentManager?.isStateSaved == true) return
              supportFragmentManager.beginTransaction().add(android.R.id.content,
                      fragment, order.id.toString()).commitAllowingStateLoss()*/
        }
    }


    fun setNotificationCount(count: String) {
        //   tvNotificationCount.text = count
    }
}