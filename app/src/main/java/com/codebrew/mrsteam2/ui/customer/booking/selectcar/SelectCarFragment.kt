package com.codebrew.mrsteam2.ui.customer.booking.selectcar


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.HomeMarkers
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.MyCarsViewModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.service.ServiceFragment
import com.codebrew.mrsteam2.utils.*
import com.codebrew.mrsteam2.utils.location.LocationProvider
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.fragment_select_car.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 *
 */
class SelectCarFragment : Fragment(), View.OnClickListener {

    private lateinit var serviceRequest: ServiceRequestModel

    private val services = ArrayList<MyCars>()

    private lateinit var selectCarAdapter: SelectCarAdapter

    private lateinit var adapterLocation: PlaceAutocompleteAdapter

    private var mGeoDataClient: GeoDataClient? = null

    private lateinit var viewModel: MyCarsViewModel

    private var map: GoogleMap? = null

    private var lat = 0.0

    private var lng = 0.0

    private var address = ""

    private var selectedServicePosition = 0

    private var apiInProgress = false

    private var bookingActivity: BookingActivity? = null

    private var markersList = ArrayList<HomeMarkers?>()

    private var timerDrivers = Timer()

    private var currentLocation: Location? = null

    private var isCurrentLocation = false

    private var currentZoomLevel = 14f

    private val ICON_WIDTH: Int = 60

    private val ICON_HEIGHT: Int = 90

    var AUTOCOMPLETE_REQUEST_CODE = 105
    var list = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_car, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(
                activity!!,
                getString(R.string.google_maps_key),
                Locale.US
            )
        }
        initialise()
        liveData()
        setListeners()

        if (services.isEmpty() && isConnectedToInternet(activity, true))
            viewModel.myCars()
        else
            setCarsData()
    }

    private fun initialise() {
        bookingActivity = activity as BookingActivity
        bookingActivity?.ivMarker?.visible()
        mGeoDataClient = Places.getGeoDataClient(bookingActivity ?: BookingActivity())

        viewModel = ViewModelProviders.of(this)[MyCarsViewModel::class.java]

        bookingActivity?.getMapAsync()

        serviceRequest = bookingActivity?.serviceRequestModel ?: ServiceRequestModel()

        if (serviceRequest.dropoff_address?.isNotBlank() == true) {
            tvAddress.setText(serviceRequest.dropoff_address)
            lat = serviceRequest.dropoff_latitude ?: 0.0
            lng = serviceRequest.dropoff_longitude ?: 0.0
            address = serviceRequest.dropoff_address ?: ""
        }
    }

    private fun liveData() {
        viewModel.myCarRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    bookingActivity?.rlLoader?.gone()

                    services.clear()
                    services.addAll(resources.data ?: emptyList())

                    if (services.isNotEmpty()) {
                        setCarsData()
                    } else {
                        tvSelectedService.text = getString(R.string.no_car_available)
                    }
                }
                Status.ERROR -> {
                    bookingActivity?.rlLoader?.gone()
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    bookingActivity?.rlLoader?.visible()
                }
            }
        })
    }

    private fun setCarsData() {
        selectCarAdapter = SelectCarAdapter(activity as Activity, rvCompanies, services)
        rvCompanies.adapter = selectCarAdapter

        rvCompanies.setItemTransformer(
            ScaleTransformer.Builder().setMaxScale(1f)
                .setMinScale(0.9f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.CENTER) // CENTER is a default one
                .build()
        )
        rvCompanies.setSlideOnFling(true)
        rvCompanies.setSlideOnFlingThreshold(1000)
        rvCompanies?.scrollToPosition((Int.MAX_VALUE / 2) + 2)

    }

    override fun onResume() {
        super.onResume()
        startDriverTimer(0)
    }

    override fun onPause() {
        super.onPause()
        timerDrivers.cancel()
        timerDrivers.purge()
        removeAllDriverMarkers()
    }

    private fun setListeners() {
        btnNext.setOnClickListener(this)
        rvCompanies.addOnItemChangedListener(itemChangeListener)
        rvCompanies.addScrollStateChangeListener(scrollListener)
        fabMyLocation.setOnClickListener(this)

        tbMain.setNavigationOnClickListener {
            activity?.finish()
        }

        tvAddress.setOnClickListener {
            var intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, list
            ).setTypeFilter(TypeFilter.ADDRESS)
                .build(activity!!)
            startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
        }

    }

    fun onMapReady(map: GoogleMap?) {
        if (isVisible) {
            isCurrentLocation = true
            this.map = map
            this.map?.setOnCameraMoveStartedListener(onCameraMoveStarted)
            this.map?.setOnCameraMoveListener(onCameraMoveListener)
            this.map?.setOnCameraIdleListener(onCameraMoveIdle)

            updateDropOffAddress()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.fabMyLocation -> {
                LocationProvider.CurrentLocationBuilder(activity).build()
                    .getLastKnownLocation(OnSuccessListener {
                        currentLocation = it
                        if (it != null) {
                            focusOnCurrentLocation(it.latitude, it.longitude)
                        } else {
                            startLocationUpdates()
                        }
                    })
            }

            R.id.tbMain -> {

            }

            R.id.btnNext -> {
                when {
                    tvAddress.text.isEmpty() -> {
                        tvAddress.showSnack(R.string.enter_address)
                    }
                    selectedServicePosition == -1 -> {
                        tvAddress.showSnack(R.string.select_car)
                    }
                    services.isEmpty() -> {
                        tvAddress.showSnack(R.string.error_car_available)
                    }
                    isConnectedToInternet(activity, true) -> {
                        val fragment = ServiceFragment()
                        val bundle = Bundle()
                        bundle.putDouble(Constants.LATITUDE, lat)
                        bundle.putDouble(Constants.LONGITUDE, lng)
                        bundle.putString(Constants.ADDRESS, address)
                        fragment.arguments = bundle

                        serviceRequest.carId = services[selectedServicePosition].id
                        serviceRequest.carName = "${activity?.getVehicleType(
                            services[selectedServicePosition].vehicle_type
                                ?: VehicleType.SMALL.toString()
                        )} " +
                                "(${services[selectedServicePosition].model})"
                        serviceRequest.dropoff_latitude = lat
                        serviceRequest.dropoff_longitude = lng
                        serviceRequest.dropoff_address = address

                        serviceRequest.isCurrentLocation = isCurrentLocation
                        activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container, fragment)
                            ?.addToBackStack("backstack")?.commit()
                    }
                }
            }
        }
    }

    private fun startLocationUpdates() {
        val locationProvider = LocationProvider.LocationUpdatesBuilder(activity).apply {
            interval = 1000
            fastestInterval = 1000
        }.build()
        locationProvider.startLocationUpdates(object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                focusOnCurrentLocation(p0?.lastLocation?.latitude, p0?.lastLocation?.longitude)
                locationProvider.stopLocationUpdates(this)
                bookingActivity?.getMapAsync()
            }

            override fun onLocationAvailability(p0: LocationAvailability?) {
                super.onLocationAvailability(p0)
            }
        })
    }

    private fun focusOnCurrentLocation(latitude: Double?, longitude: Double?) {
        val target = LatLng(latitude ?: 0.0, longitude ?: 0.0)
        val builder = CameraPosition.Builder()
        builder.zoom(14f)
        builder.target(target)
        bookingActivity?.googleMapHome?.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()))
    }

    private val itemChangeListener = DiscreteScrollView.OnItemChangedListener<SelectCarAdapter.ViewHolder>
    { viewHolder, adapterPosition ->
        viewHolder?.setSelected(adapterPosition)
        showSelectedText(adapterPosition)
        homeApiCall()
    }


    private fun homeApiCall() {
        removeAllDriverMarkers()
        timerDrivers.cancel()
        val map = HashMap<String, String>()
        //map["category_id"] = (((selectedServicePosition) % servicesNames.size) + 1).toString()
        map["latitude"] = lat.toString()
        map["longitude"] = lng.toString()
        map["distance"] = "50000"
        if (isConnectedToInternet(activity, true)) {
            apiInProgress = true
            // presenter.homeApi(map)
            timerDrivers.cancel()
        }
    }

    private val scrollListener = object : DiscreteScrollView.ScrollStateChangeListener<SelectCarAdapter.ViewHolder> {
        override fun onScroll(
            scrollPosition: Float, currentPosition: Int,
            newPosition: Int, currentHolder: SelectCarAdapter.ViewHolder?,
            newCurrent: SelectCarAdapter.ViewHolder?
        ) {

        }

        override fun onScrollEnd(currentItemHolder: SelectCarAdapter.ViewHolder, adapterPosition: Int) {
            showSelectedText(adapterPosition)
        }

        override fun onScrollStart(currentItemHolder: SelectCarAdapter.ViewHolder, adapterPosition: Int) {
            currentItemHolder.setUnSelected(adapterPosition)
            hideSelectedText()
        }
    }

    private val onCameraMoveListener = GoogleMap.OnCameraMoveListener {
        markersList.forEach {
            if (currentZoomLevel != map?.cameraPosition?.zoom) {
                currentZoomLevel = map?.cameraPosition?.zoom ?: 14f
                scaleDownMarker(it?.marker, (currentZoomLevel / 14f))
            }
        }
    }

    private val onCameraMoveStarted = GoogleMap.OnCameraMoveStartedListener {
        tvAddress?.setText("")
        isCurrentLocation = false
    }

    private val onCameraMoveIdle = GoogleMap.OnCameraIdleListener {
        updateDropOffAddress()
        isCurrentLocation = MapUtils.getDistanceBetweenTwoPoints(
            LatLng(
                map?.cameraPosition?.target?.latitude
                    ?: 0.0, map?.cameraPosition?.target?.longitude
                    ?: 0.0
            ),
            LatLng(
                currentLocation?.latitude
                    ?: 0.0, currentLocation?.longitude ?: 0.0
            )
        ) < 1

    }

    private fun scaleDownMarker(marker: Marker?, scale: Float) {
        if ((ICON_WIDTH * scale).toInt() > 0 && (ICON_HEIGHT * scale).toInt() > 0 && isAdded) {
            Thread {
                val mapIcon: Bitmap? = getIconBitmap()
                val newBitMap: Bitmap? = Bitmap.createScaledBitmap(
                    mapIcon, (ICON_WIDTH * scale).toInt(),
                    (ICON_HEIGHT * scale).toInt(),
                    false
                )
                Handler(Looper.getMainLooper()).post {
                    try {
                        marker?.setIcon(BitmapDescriptorFactory.fromBitmap(newBitMap))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }.start()
        }
    }

    private fun getIconBitmap(): Bitmap {
        requireContext().resources
        return bookingActivity?.resources.let {
            BitmapFactory.decodeResource(
                it,
                getVehicleResId((selectedServicePosition % services.size) + 1)
            )
        }
    }

    private fun updateDropOffAddress() {
        lat = map?.cameraPosition?.target?.latitude ?: 0.0
        lng = map?.cameraPosition?.target?.longitude ?: 0.0
        activity?.runOnUiThread {
            getAddressFromLatLng(lat, lng)
        }
    }

    private fun getAddressFromLatLng(latitude: Double, longitude: Double) {
        val geocoder = Geocoder(activity, LocaleManager.getLocale(activity?.resources))
        Thread(Runnable {

            try {
                val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
                val addressString = StringBuilder()
                if (addresses.isNotEmpty()) {
                    for (i in 0 until addresses[0].maxAddressLineIndex + 1) {
                        addressString.append(addresses[0].getAddressLine(i))
                        if (i != addresses[0].maxAddressLineIndex) {
                            addressString.append(",")
                        }
                    }
                }

                Handler(Looper.getMainLooper()).post {
                    this@SelectCarFragment.address = addressString.toString()
                    tvAddress?.setText(address)

                    if (activity != null) {
                        val latLngBounds = LatLngBounds.builder()
                        latLngBounds.include(LatLng(lat + 1, lng + 1))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }).start()

    }

    private fun showSelectedText(position: Int) {
        selectedServicePosition = position % services.size
        tvSelectedService?.text =
            services[position % services.size].model + " (" + services[position % services.size].plate_number + ")"
        tvSelectedService?.animate()?.alpha(1f)?.setDuration(200)?.start()
    }

    private fun hideSelectedText() {
        selectedServicePosition = -1
        tvSelectedService?.animate()?.alpha(0f)?.setDuration(600)?.start()
    }

    override fun onDestroyView() {
        rvCompanies?.removeItemChangedListener(itemChangeListener)
        rvCompanies?.removeScrollStateChangeListener(scrollListener)
        map?.setOnCameraIdleListener(null)
        map?.setOnCameraMoveStartedListener(null)
        super.onDestroyView()
    }

    private fun startDriverTimer(delay: Long) {
        timerDrivers.cancel()
        timerDrivers = Timer()
        timerDrivers.schedule(object : TimerTask() {
            override fun run() {
            }
        }, delay)
    }


    private fun removeAllDriverMarkers() {
        map?.clear()
        markersList.forEach {
            it?.moveValueAnimate?.cancel()
            it?.rotateValueAnimator?.cancel()
        }
        markersList.clear()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                var place = Autocomplete.getPlaceFromIntent(data!!)

                tvAddress.setText(place.address)

                getAddressFromLatLng(place.latLng!!.latitude, place.latLng!!.longitude)

                Log.i("TAG", "Place: " + place.getName() + ", " + place.getId())
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                var status = Autocomplete.getStatusFromIntent(data!!)
                Log.i("TAG", status.getStatusMessage());
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }


}

