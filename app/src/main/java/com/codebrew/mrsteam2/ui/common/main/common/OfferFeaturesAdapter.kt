package com.codebrew.mrsteam2.ui.common.main.common

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.ActiveDetails
import kotlinx.android.synthetic.main.item_offers_features.view.*

class OfferFeaturesAdapter(private val allOffers: Boolean) : RecyclerView.Adapter<OfferFeaturesAdapter.ViewHolder>() {

    private val l1 = ArrayList<ActiveDetails>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_offers_features, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return l1.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(l1[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        init {
        }

        fun bind(data: ActiveDetails?) {
            if (allOffers) {
                itemView.tvFeature.text = data?.dual_title
            } else {
                itemView.tvFeature.text = data?.sub_service?.dual_title
            }
        }
    }


    fun setData(list: ArrayList<ActiveDetails>) {
        l1.clear()
        l1.addAll(list)
        notifyDataSetChanged()
    }

}


