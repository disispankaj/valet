package com.codebrew.mrsteam2.ui.customer.booking.vehicles

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.customer.webservices.models.homeapi.Product
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.utils.setRoundImageUrl
import kotlinx.android.synthetic.main.item_vehicle_type.view.*


class SelectVehicleTypeAdapter(val categoriesList: ArrayList<Category>?, private var prevSelectedPosition: Int) : RecyclerView.Adapter<SelectVehicleTypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle_type, parent, false))
    }

    override fun getItemCount(): Int {
        return categoriesList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(categoriesList?.get(position))
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            categoriesList?.get(prevSelectedPosition)?.isSelected = true
            itemView?.setOnClickListener {
                categoriesList?.get(prevSelectedPosition)?.isSelected = false
                categoriesList?.get(adapterPosition)?.isSelected = true
                prevSelectedPosition = adapterPosition
//                    itemSelectedListener?.onItemSelected(adapterPosition)
                notifyDataSetChanged()
            }

        }

        fun onBind(category: Category?) = with(itemView) {
            tvVehicleName.text = category?.name
            ivVehicleImage.setRoundImageUrl(category?.image_url)
            tvVehicleName.isSelected = category?.isSelected ?: false
            viewSelector.visibility = if (category?.isSelected == true) View.VISIBLE else View.GONE
        }
    }

    fun getSelectedPosition(): Int {
        return prevSelectedPosition
    }

    fun getSelectedCategoryBrandId(): Int {
        return categoriesList?.get(prevSelectedPosition)?.category_brand_id ?: 0
    }

    fun getSelectedCategoryBrandName(): String {
        return categoriesList?.get(prevSelectedPosition)?.name ?: ""
    }

    fun getSelectedCategoryProducts(): List<Product>? {
        return categoriesList?.get(prevSelectedPosition)?.products
    }

}