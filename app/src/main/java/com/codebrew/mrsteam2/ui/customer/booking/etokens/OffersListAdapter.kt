package com.codebrew.mrsteam2.ui.customer.booking.etokens

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.buraq24.customer.webservices.models.etokens.Etoken
import com.codebrew.mrsteam2.R
import kotlinx.android.synthetic.main.items_offers_token.view.*

class OffersListAdapter(private val context: Context?, private val tokensList: List<Etoken>?) : RecyclerView.Adapter<OffersListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.items_offers_token, parent, false))
    }

    override fun getItemCount(): Int {
        return tokensList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(tokensList?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView?.tvPrice?.setOnClickListener {
                Toast.makeText(context, R.string.coming_soon, Toast.LENGTH_SHORT).show()
//                (context as ETokensActivity).buyEToken(tokensList?.get(adapterPosition)?.organisation_coupon_id)
            }
        }

        fun onBind(token: Etoken?) = with(itemView) {
            rootView.isSelected = true
            tvDescription.isSelected = true
            tvTokenCount.isSelected = true
            tvTokenCount.text = """${token?.quantity.toString()} ${if (token?.quantity == 1) {
                context.getString(R.string.e_token)
            } else {
                context.getString(R.string.e_tokens)
            }}"""
            tvDescription.text = """${token?.category_brand_product_name}/${context.getString(R.string.e_token)}"""
            tvPrice.text = """${token?.price.toString()} ${context.getString(R.string.currency)}"""
        }
    }
}