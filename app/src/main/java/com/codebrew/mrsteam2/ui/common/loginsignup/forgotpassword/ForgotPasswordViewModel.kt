package com.codebrew.mrsteam2.ui.common.loginsignup.forgotpassword

import  androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.ForgotPasswordRequest
import com.codebrew.mrsteam2.network.responsemodels.ForgotPassword
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordViewModel : ViewModel() {
    val forgotRes by lazy { SingleLiveEvent<Resource<ApiResponse<ForgotPassword>>>() }

    fun forgotPassword(forgotPassRequest: ForgotPasswordRequest) {
        forgotRes.value = Resource.loading()

        RetrofitClient.getApi().forgotPassword(forgotPassRequest)
                .enqueue(object : Callback<ApiResponse<ForgotPassword>> {
                    override fun onFailure(call: Call<ApiResponse<ForgotPassword>>, throwable: Throwable) {
                        forgotRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<ForgotPassword>>,
                            response: Response<ApiResponse<ForgotPassword>>
                    ) {
                        if (response.isSuccessful) {
                            forgotRes.value = Resource.success(response.body())
                        } else {
                            forgotRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })


    }
}