package com.codebrew.mrsteam2.ui.customer.booking.invoice


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.customer.booking.rating.RatingFragment
import com.codebrew.mrsteam2.utils.CategoryId
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.ORDER
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_invoice.*
import java.util.*

class InvoiceFragment : Fragment() {

    private var order: Wash? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setData()
        setListeners()
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            openRatingFragment()
        }
    }

    private fun setData() {
        order = Gson().fromJson(arguments?.getString("order"), Wash::class.java)
        tvServiceName.text = when (order?.brand?.category_id) {
            CategoryId.GAS -> {
                tvCarServiceV.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvCarServiceV.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                order?.brand?.brand_name
            }
            CategoryId.WATER_TANKER -> {
                tvCarServiceV.text = String.format("%s × %s", order?.brand?.name, order?.payment?.product_quantity)
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT -> {
                tvCarServiceV.text = order?.brand?.name
                order?.brand?.brand_name
            }
            else -> {
                getString(R.string.default_category_name)
            }
        }

//        tvPrice.text = "${AppUtils.getFinalCharge(order)} ${getString(R.string.currency)}"
//        tvTotal.text = AppUtils.getFinalCharge(order) + " " + getString(R.string.currency)
        tvPrice.text = "${getFormattedDecimal(order?.payment?.final_charge?.toDouble()
                ?: 0.0)} ${getString(R.string.currency)}"
        tvTax.text = "${getFormattedDecimal(0.0)} ${getString(R.string.currency)}"
        tvGst.text = "${getFormattedDecimal(0.0)} ${getString(R.string.currency)}"
        tvCarServicePriceV.text = "${getFormattedDecimal(order?.payment?.final_charge?.toDouble()
                ?: 0.0)} ${getString(R.string.currency)}"
    }

    private fun openRatingFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = RatingFragment()
        val bundle = Bundle()
        bundle.putString(ORDER, Gson().toJson(order))
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun getFormattedDecimal(num: Double): String? {
        return String.format(Locale.US, "%.2f", num)
    }

}
