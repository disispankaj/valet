package com.codebrew.mrsteam2.ui.common.main.home.notification

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.gone
import com.codebrew.mrsteam2.utils.visible
import kotlinx.android.synthetic.main.activity_notification.*

class NotificationActivity : AppCompatActivity() {

    private lateinit var viewModel: NotificationViewModel
    private lateinit var progressDialog: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        initialise()
        listener()
        setAdapter()
        liveData()
        hitApi()


    }

    private fun hitApi() {
        viewModel.getNotification()
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[NotificationViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }


    private fun setAdapter() {
        rvNotification.layoutManager = LinearLayoutManager(this@NotificationActivity, RecyclerView.VERTICAL, false)
        rvNotification.adapter = NotificationAdapter(object : NotificationAdapter.ClickNotification {
            override fun onClick(id: String?) {
                viewModel.readNotification(id ?: "")
            }
        })
    }

    private fun listener() {
        tbrNotification.setNavigationOnClickListener {
            onBackPressed()
        }
        swipe.setOnRefreshListener {
            hitApi()
        }
    }


    private fun liveData() {

        viewModel.notificationRes.observe(this, Observer { resources ->
            resources ?: return@Observer

            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    if (resources.data?.isNotEmpty() == true) {
                        tvNoData.gone()
                        (rvNotification.adapter as NotificationAdapter).setData(resources.data)
                    } else {
                        tvNoData.visible()
                    }
                    swipe.isRefreshing = false


                }
                Status.ERROR -> {
                    swipe.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    swipe.isRefreshing = false
                    progressDialog.setLoading(true)
                }
            }

        })

    }


}