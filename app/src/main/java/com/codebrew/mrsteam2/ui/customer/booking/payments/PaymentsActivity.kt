package com.codebrew.mrsteam2.ui.customer.booking.payments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.customer.booking.etokens.ETokensActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_payments.*

class PaymentsActivity : AppCompatActivity(), View.OnClickListener {

    private var selectedPaymentType: String? = PaymentType.CASH

    //private val presenter = PaymentsPresenter()

    private val RQ_SELECT_ETOKEN = 401

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payments)
        //presenter.attachView(this)
        selectedPaymentType = if (intent.hasExtra(PAYMENT_TYPE)) {
            intent.getStringExtra(PAYMENT_TYPE)
        } else {
            PrefsManager.get().getString(PAYMENT_TYPE, PaymentType.CASH)
        }
        setSelectedPaymentMethod(selectedPaymentType)
        //    Uncomment to enable old etoken module
//        if (intent.hasExtra(CATEGORY_ID)) {
        tvViewToken.visibility = View.GONE

//            if (intent.getIntExtra(CATEGORY_ID, 0) != CategoryId.MINERAL_WATER) {
        tvToken.visibility = View.GONE
        rbToken.visibility = View.GONE
        tvViewToken.visibility = View.GONE
//
//            }
//        } else {
//            tvViewToken.visibility = View.VISIBLE
//            tvViewToken.text = getString(R.string.view_e_tokens)
//        }
        if (selectedPaymentType == PaymentType.E_TOKEN) {
            tvViewToken.visibility = View.VISIBLE
            tvViewToken.text = getString(R.string.select_e_token)
        }
        setListener()
    }

    private fun paymentDetailApiCall() {
        // presenter.paymentDetail("", "")
    }

    private fun setListener() {
        tvBack.setOnClickListener(this)
        tvToken.setOnClickListener(this)
        rbToken.setOnClickListener(this)
        tvCod.setOnClickListener(this)
        rbCod.setOnClickListener(this)
        tvCard.setOnClickListener(this)
        rbCard.setOnClickListener(this)
        tvViewToken.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvBack -> {
                finish()
            }
            R.id.tvToken, R.id.rbToken -> {
                if (intent.hasExtra(CATEGORY_ID)) {
                    setSelectedPaymentMethod(PaymentType.E_TOKEN)
                    tvViewToken.visibility = View.VISIBLE
                    tvViewToken.text = getString(R.string.select_e_token)
//                    setResult()
                } else {
                    rootView.showSnack(R.string.default_selecting_token_error)
                    setSelectedPaymentMethod(selectedPaymentType)
                }
            }
            R.id.tvCod, R.id.rbCod -> {
                setSelectedPaymentMethod(PaymentType.CASH)
                saveDefaultPaymentMethod()
                setResult()
            }
            R.id.tvCard, R.id.rbCard -> {
                setSelectedPaymentMethod(selectedPaymentType)
                rootView.showSnack(R.string.coming_soon)
//                setSelectedPaymentMethod(PaymentType.CARD)
//                saveDefaultPaymentMethod()
//                setResult()
            }
            R.id.tvViewToken -> {
                val intentToken = Intent(this, ETokensActivity::class.java)
                if (intent.hasExtra(CATEGORY_BRAND_ID)) {
                    intentToken.putExtra("want_selection", true)
                    intentToken.putExtra(CATEGORY_BRAND_ID, intent.getIntExtra(CATEGORY_BRAND_ID, 0))
                    startActivityForResult(intentToken, RQ_SELECT_ETOKEN)
                } else {
                    intentToken.putExtra("want_selection", false)
                    startActivity(intentToken)
                }
            }
        }
    }

    private fun setSelectedPaymentMethod(paymentType: String?) {
        selectedPaymentType = paymentType
        rbToken.isChecked = paymentType == PaymentType.E_TOKEN
        rbCod.isChecked = paymentType == PaymentType.CASH
        rbCard.isChecked = paymentType == PaymentType.CARD
    }

    private fun saveDefaultPaymentMethod() {
        PrefsManager.get().save(PAYMENT_TYPE, selectedPaymentType)
    }


    private fun setResult() {
        if (intent.hasExtra(CATEGORY_ID)) {
            val intent = Intent()
            intent.putExtra(PAYMENT_TYPE, selectedPaymentType)
            intent.putExtra(TOKEN_ID, tokenId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private var tokenId: Int? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_SELECT_ETOKEN && resultCode == Activity.RESULT_OK && data != null) {
            tokenId = data.getIntExtra(TOKEN_ID, 0)
            setResult()
        }
    }

    /*override fun onPaymentDetailsApiSuccess(response: PaymentDetail?) {
        response?.etoken_sum
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {

    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(this)
        }
    }
*/
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

}
