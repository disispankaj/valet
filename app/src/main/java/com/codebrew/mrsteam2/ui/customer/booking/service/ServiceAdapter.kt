package com.codebrew.mrsteam2.ui.customer.booking.service

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Services
import kotlinx.android.synthetic.main.item_select_brands.view.*


class ServiceAdapter(
    private val context: Context?, private val categories: List<Services>?,
    private var prevSelectedPosition: Int
) : RecyclerView.Adapter<ServiceAdapter.ViewHolder>() {

    private var itemSelectedListener: ItemSelectedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_select_brands, parent, false))
    }

    override fun getItemCount(): Int {
        return categories?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories?.get(position))
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            categories?.get(prevSelectedPosition)?.isSelected = true
            itemView?.tvService?.setOnClickListener {
                categories?.get(prevSelectedPosition)?.isSelected = false
                categories?.get(adapterPosition)?.isSelected = true
                prevSelectedPosition = adapterPosition
                itemSelectedListener?.onItemSelected(adapterPosition)
                notifyDataSetChanged()
            }
        }

        fun bind(categories: Services?) {

            itemView.tvService?.text = categories?.dual_name
            if (categories?.isSelected ?: false) {
                itemView.tvService?.setTextColor(ActivityCompat.getColor(context!!, R.color.white))
                itemView.tvService?.setBackgroundResource(R.drawable.back_vehicle_type_on_rect)
            } else {
                itemView.tvService?.setTextColor(ActivityCompat.getColor(context!!, R.color.black))
                itemView.tvService?.setBackgroundResource(R.drawable.back_vehicle_type_off_rect)
            }
        }
    }

    fun setItemSelectedListener(listener: ItemSelectedListener?) {
        itemSelectedListener = listener
    }

    interface ItemSelectedListener {
        fun onItemSelected(position: Int)
    }
}
