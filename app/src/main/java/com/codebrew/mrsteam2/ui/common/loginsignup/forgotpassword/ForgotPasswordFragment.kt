package com.codebrew.mrsteam2.ui.common.loginsignup.forgotpassword

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ForgotPasswordRequest
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.ui.common.main.profile.changepassword.ChangePasswordActivity
import com.codebrew.mrsteam2.utils.*
import com.facebook.login.LoginFragment
import kotlinx.android.synthetic.main.activity_signup_login.*
import kotlinx.android.synthetic.main.fragment_forgot_password.*

class ForgotPasswordFragment : Fragment() {

    private lateinit var viewModel: ForgotPasswordViewModel
    private lateinit var progressDialog: ProgressDialog

    private var countryCode: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
        initialise()
        liveData()

//        ivBack.setOnClickListener {
//                    //    startActivity(Intent(this@ForgotPasswordFragment, LoginFragment::class.java))
//        }
    }

    private fun listeners() {
        btnSend.setOnClickListener {
            validationForgotPassword()
        }

        ccpLogin.setOnCountryChangeListener {
            countryCode = ccpLogin.selectedCountryCode.toInt()
        }

    }


    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[ForgotPasswordViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

//        (activity as LoginSignupActivity).ivBack.visible()
    }



    private fun forgotPasswordApiHit() {
        val forgotPassRequest = ForgotPasswordRequest(
            input = etEmailPhone.text.toString(),
            country_code = countryCode
        )
        if (isConnectedToInternet(activity as Activity, true))
            viewModel.forgotPassword(forgotPassRequest)
    }


    private fun liveData() {
        viewModel.forgotRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    val res = resources.data?.data

                    if (res?.otp == 0) {
                        fragmentManager?.beginTransaction()
                            ?.replace(
                                R.id.layoutFragmentData,
                                LoginFragment(), LoginFragment::class.java.name
                            )
                            ?.addToBackStack(LoginFragment::class.java.name)?.commit()
                        Toast.makeText(activity as Activity, resources.data.msg, Toast.LENGTH_LONG).show()

                    } else if (res == null) {
                        Toast.makeText(activity as Activity, resources.data?.message, Toast.LENGTH_LONG).show()

                    } else {
                        val i1 = Intent(activity as Activity, ChangePasswordActivity::class.java)
                        i1.putExtra(OTP, res?.otp)
                        i1.putExtra(USER_ID, res?.user_id)
                        startActivity(i1)
                    }

                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

    }


    private fun validationForgotPassword() {
        tilEmailPhone.error = null

        return when {
            etEmailPhone.text.toString().isEmpty() -> {
                invalidStringInput(etEmailPhone, tilEmailPhone, getString(R.string.error_Forgot_pass_empty_phone_email))

            }
            (!EMAIL_FORMAT.matcher(etEmailPhone.text.toString()).matches())
                    && (!PHONE_FORMAT.matcher(etEmailPhone.text.toString()).matches()) -> {
                invalidStringInput(etEmailPhone, tilEmailPhone, getString(R.string.str_phone_email_error))
            }
            else -> forgotPasswordApiHit()
        }

    }
}
