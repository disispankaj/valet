package com.codebrew.mrsteam2.ui.driver.servicerequest.request


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.driver.servicerequest.ServiceViewHolder

class ServiceAdapter(private val listRequests: ArrayList<Wash>) : RecyclerView.Adapter<ServiceViewHolder>() {
    override fun getItemCount(): Int {
        return listRequests.size
    }

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        holder.onBind(listRequests[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        return ServiceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_all_request, parent, false))
    }

    fun addItems(listRequest: ArrayList<Wash>) {
        val oldLength = listRequest.size
        this.listRequests.addAll(listRequest)
        notifyItemRangeInserted(oldLength, listRequest.size)
    }

}