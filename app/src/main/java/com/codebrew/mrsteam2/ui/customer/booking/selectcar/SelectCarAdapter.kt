package com.codebrew.mrsteam2.ui.customer.booking.selectcar

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.utils.VehicleType
import com.codebrew.mrsteam2.utils.invisible
import com.codebrew.mrsteam2.utils.visible
import kotlinx.android.synthetic.main.item_booking_services.view.*

class SelectCarAdapter(
    private val context: Context, private val recyclerView: RecyclerView,
    private val items: ArrayList<MyCars>
) : RecyclerView.Adapter<SelectCarAdapter.ViewHolder>() {

    private val itemsCount = items.size

    override fun getItemCount(): Int {
      //  return Int.MAX_VALUE
        return itemsCount
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pos = position % itemsCount
        holder.onBind(pos, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_booking_services,
                parent, false
            )
        )
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            //itemView?.layoutParams?.width = (getScreenWidth(context as Activity) / 5)
            itemView?.setOnClickListener {
                //                (context as HomeActivity).recyclerView.smoothScrollToPosition(adapterPosition-2)
//                layoutmanager.smoothScrollToPosition(context.recyclerView,RecyclerView.State(), adapterPosition-2)
                recyclerView.smoothScrollToPosition(adapterPosition)
//                layoutmanager.smoothScrollToPosition(adapterPosition-2)
//            }
            }
        }

        fun onBind(position: Int, actualPosition: Int) {

/*
            if (items[position].image_url != null) {
                loadImage(
                    context as Activity,
                    itemView.imageView,
                    items[position].image_url?.thumbnail,
                    items[position].image_url?.original
                )
            } else {*/

            when (items[position].vehicle_type?.toInt()) {
                VehicleType.SMALL -> {
                    itemView.imageView.setImageResource(R.drawable.ic_addcar_hatchback)
                }
                VehicleType.SEDAN -> {
                    itemView.imageView.setImageResource(R.drawable.ic_addcar_sedan)
                }
                VehicleType.MEDIUM -> {
                    itemView.imageView.setImageResource(R.drawable.ic_addcar_mpv)
                }
                VehicleType.LARGE_TRUCK -> {
                    itemView.imageView.setImageResource(R.drawable.ic_addcar_suv)
                }
                else -> {
                    itemView.imageView.setImageResource(R.drawable.ic_addcar_hatchback)
                }
            }
            // }

        }

        fun setSelected(position: Int) {
            itemView.ivBack.setCardBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.mrSteamMain
                )
            )

           // itemView.imageView.setColorFilter(ContextCompat.getColor(itemView.context, R.color.add_car_back_off))
            itemView.ivSelected.visible()
            //itemView.imageView.setImageResource(mImagesSelected[position % itemsCount])
        }

        fun setUnSelected(position: Int) {
            itemView.ivBack.setCardBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.add_car_back_off
                )
            )
            itemView.imageView.setColorFilter(R.color.black)
            itemView.ivSelected.invisible()
        }
    }

    var prevPosition = 0
}