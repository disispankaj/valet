package com.codebrew.mrsteam2.ui.customer.booking.promocode

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R

class PromoCodeAdapter : RecyclerView.Adapter<PromoCodeAdapter.ViewHolder>() {
    private val l1 = ArrayList<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_available_coupon, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //holder.bind(l1[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        init {

        }

        fun bind(data: String?) {

        }
    }

    fun setData(list: ArrayList<String>) {
        l1.clear()
        l1.addAll(list)
        notifyDataSetChanged()
    }

}
