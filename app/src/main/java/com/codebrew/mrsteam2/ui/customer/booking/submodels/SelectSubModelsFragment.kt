package com.codebrew.mrsteam2.ui.customer.booking.submodels


import android.os.Build
import android.os.Bundle

import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager


import com.buraq24.customer.webservices.models.homeapi.Product
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.Service
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.PrefsManager
import com.codebrew.mrsteam2.utils.SERVICES
import com.codebrew.mrsteam2.utils.showSnack
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_select_vehicle_type.*


class SelectSubModelsFragment : Fragment() {

    private val products: ArrayList<Product?>? = ArrayList()

    private var adapter: SelectSubModelsAdapter? = null

    private lateinit var serviceRequest: ServiceRequestModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel
        serviceRequest.category_brand_product_id = -1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_select_vehicle_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvTitle.text = getString(R.string.select_sub_model)
        products?.clear()
        products?.addAll(Gson().fromJson(arguments?.getString("submodels", ""), object : TypeToken<List<Product>>() {}.type)
                ?: ArrayList())
        if (products?.isEmpty() == false) {
            val selectedPosition = getSelectedPosition(serviceRequest.category_brand_product_id
                    ?: 0)
            adapter = SelectSubModelsAdapter(products, selectedPosition)
            rvCompanies?.layoutManager = GridLayoutManager(activity, 3, GridLayoutManager.VERTICAL, false)
            rvCompanies?.adapter = adapter
        }
        setListeners()
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            if (products?.isEmpty() == true) {
                rootView.showSnack(R.string.no_vehicles_available)
                return@setOnClickListener
            }
            serviceRequest.category_brand_product_id = adapter?.getSelectedBrandProductId()
            serviceRequest.productName = adapter?.getSelectedBrandProductName()
            serviceRequest.final_charge = getFinalCharge()
            serviceRequest.images = ArrayList()
            serviceRequest.product_weight = 0
            serviceRequest.material_details = ""
            serviceRequest.details = ""
            /* val fragment = BookingTypeFragment()
             fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
     */
        }
    }

    private fun getSelectedPosition(categoryBrandId: Int): Int {
        for (i in products.orEmpty().indices) {
            if (products?.get(i)?.category_brand_product_id == categoryBrandId) {
                return i
            }
        }
        return 0
    }

    private fun getFinalCharge(): Double? {
        val service = Gson().fromJson<List<Service>>(PrefsManager.get().getString(SERVICES, ""), object : TypeToken<List<Service>>() {}.type)
        val product = adapter?.getSelectedProduct()
        val price = product?.price_per_quantity?.times(serviceRequest.product_quantity ?: 1)
                ?.plus(product.alpha_price?.toDouble() ?: 0.0)
                ?.plus((product.price_per_distance?.toDouble() ?: 0.0)
                        .times(serviceRequest.order_distance ?: 0f))
        var buraqPercentage = 0f
        service.forEach {
            if (it.category_id == serviceRequest.category_id) {
                buraqPercentage = it.buraq_percentage ?: 0f
            }
        }
        return price?.div(100)?.times(buraqPercentage)?.plus(price)

    }

}
