package com.codebrew.mrsteam2.ui.common.review

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RateReviewViewModel : ViewModel() {

    val rateRes by lazy { SingleLiveEvent<Resource<Wash>>() }

    fun rateReview(map: HashMap<String, String>) {

        rateRes.value = Resource.loading()
        RetrofitClient.getApi()
            .submitReview(map)
            .enqueue(object : Callback<ApiResponse<Wash>> {

                override fun onFailure(call: Call<ApiResponse<Wash>>, throwable: Throwable) {
                    rateRes.value = Resource.error(ApiUtils.failure(throwable))
                }

                override fun onResponse(call: Call<ApiResponse<Wash>>, response: Response<ApiResponse<Wash>>) {
                    if (response.isSuccessful) {
                        rateRes.value = Resource.success(response?.body()?.data)
                    } else {
                        rateRes.value = Resource.error(
                            ApiUtils.getError(
                                response.code(),
                                response.errorBody()?.string()
                            )
                        )
                    }
                }
            })
    }

}