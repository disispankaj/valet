package com.codebrew.mrsteam2.ui.customer.booking.promocode

data class Promo(

    val id: Int = 0,
    val value: Int = 0,
    val promo_type: String? = null,
    val dual_name: String? = null,
    val name: String? = null
)