package com.codebrew.mrsteam2.ui.common.main.home.recentwash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.home.HomeAdapter
import com.codebrew.mrsteam2.ui.driver.main.history.AllWashesViewModel
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.RECENT_WASH
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_home_recent_wash.*

class RecentWashActivity : AppCompatActivity() {
    private lateinit var viewModel: AllWashesViewModel
    private lateinit var progressDialog: ProgressDialog

    private val recentAdapter = HomeAdapter(null,RECENT_WASH)


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_recent_wash)


        adapterSetting()
        initialise()
        liveData()
        listener()
    }

    private fun adapterSetting() {
        rvRecentWash.layoutManager = LinearLayoutManager(this)
        rvRecentWash.adapter = recentAdapter
    }

    private fun listener() {
        tbrHomeRecentWash.setNavigationOnClickListener {
            onBackPressed()
        }

        swipeRefreshLayout.setOnRefreshListener {
            if (isConnectedToInternet(this, true))
                viewModel.allWashes(2)
        }
    }


    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[AllWashesViewModel::class.java]
        progressDialog = ProgressDialog(this)

        if (isConnectedToInternet(this, true))
            viewModel.allWashes(2)
    }


    private fun liveData() {
        viewModel.washesRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    recentAdapter.setDataRecent(resources.data?.recent_washes ?: ArrayList())
                }

                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })

    }
}