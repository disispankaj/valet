package com.codebrew.mrsteam2.ui.common.main.home.offers

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OfferViewModel : ViewModel() {

    val allOfferRes by lazy { SingleLiveEvent<Resource<ArrayList<Offers>>>() }

    fun allOffers() {
        allOfferRes.value = Resource.loading()


        RetrofitClient.getApi().allOffers()
                .enqueue(object : Callback<ApiResponse<ArrayList<Offers>>> {
                    override fun onFailure(call: Call<ApiResponse<ArrayList<Offers>>>, throwable: Throwable) {
                        allOfferRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<ArrayList<Offers>>>,
                            response: Response<ApiResponse<ArrayList<Offers>>>
                    ) {
                        if (response.isSuccessful) {
                            allOfferRes.value = Resource.success(response.body()?.data)
                        } else {
                            allOfferRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}