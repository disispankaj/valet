package com.codebrew.mrsteam2.ui.driver.servicerequest.requestAfterAccept


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.AcceptRequestViewModel
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.ui.driver.servicerequest.ServiceProgressActivity
import com.codebrew.mrsteam2.utils.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_service_progress.*


class StartRequestFragment : Fragment() {

    private lateinit var orderDetail: Wash
    private lateinit var progressDialog: ProgressDialog
    private var requestAcceptReject = false
    private var serviceProgressActivity: ServiceProgressActivity? = null


    private lateinit var viewModelAcceptReject: AcceptRequestViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_service_progress, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialise()
        setData()
        liveData()
        setListeners()
    }

    private fun initialise() {
        serviceProgressActivity = activity as ServiceProgressActivity
        serviceProgressActivity?.getMapAsync()

        viewModelAcceptReject = ViewModelProviders.of(this)[AcceptRequestViewModel::class.java]

        orderDetail = Gson().fromJson(arguments?.getString(ORDER_PROGRESS), Wash::class.java)
        progressDialog = ProgressDialog(activity)
    }

    private fun setData() {
        tvLocation.text = orderDetail.address
        tvCarNameV.text = orderDetail.my_car?.model
        tvCarServiceV.text = orderDetail.sub_service?.dual_title

        /*tvCarServicePrice.text = when {
            orderDetail.payment_type.toInt() == PaymentsType.CASH -> getString(R.string.cash)
            orderDetail.payment_type.toInt() == PaymentsType.CREDIT -> getString(R.string.credit_card)
            else -> getString(R.string.services_offer)
        }*/

        tvCarServicePriceV.text = orderDetail.sub_service?.price.toString()
        tvLocation.text = orderDetail.address

        tvName.text = orderDetail.user?.name
        loadImage(
            activity, ivUser, orderDetail.user?.image_url?.thumbnail
                ?: "", orderDetail.user?.image_url?.original ?: ""
        )

        tvDistanceStartRequest.text = "${orderDetail.distance} km"
        tvTimeStartRequest.text = "${orderDetail.time} min"

        tvAccept.text = when (orderDetail.status) {
            ServiceStatus.ACCEPT -> getString(R.string.move_to)
            ServiceStatus.JOURNEY_STARTED -> getString(R.string.start_washing)
            ServiceStatus.WASHING_STARTED -> getString(R.string.complete_job)
            else -> getString(R.string.move_to)
        }

        if (serviceProgressActivity?.googleMapHome != null)
            updateTrack()

    }

    private fun liveData() {
        viewModelAcceptReject.acceptRejectRequest.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    orderDetail = it.data ?: Wash()

                    if (orderDetail.status == ServiceStatus.COMPLETED) {
                      /*  startActivity(
                            Intent(activity, DetailActivity::class.java)
                                .putExtra(DETAIL_DATA, orderDetail)
                        )*/

                        if (orderDetail.is_reviewed == true)
                            startActivity(Intent(activity, DetailActivity::class.java)
                                .putExtra(DETAIL_DATA, orderDetail)
                            )
                        else {
                            startActivity(Intent(activity, RateReviewActivity::class.java)
                                .putExtra(DETAIL_DATA, orderDetail))
                        }



                        activity?.setResult(Activity.RESULT_OK)
                        activity?.finish()
                    } else
                        setData()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(it.error, activity)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModelAcceptReject.acceptRejectRequest.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    orderDetail = it.data ?: Wash()

                    if (orderDetail.status == ServiceStatus.COMPLETED) {
                        if (orderDetail.is_reviewed == true)
                            startActivity(Intent(activity, DetailActivity::class.java)
                                .putExtra(DETAIL_DATA, orderDetail)
                            )
                        else {
                            startActivity(Intent(activity, RateReviewActivity::class.java)
                                .putExtra(DETAIL_DATA, orderDetail))
                        }
                        activity?.setResult(Activity.RESULT_OK)
                        activity?.finish()
                    } else
                        setData()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(it.error, activity)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }


    fun updateTrack() {
        if (isVisible) {
            if (orderDetail.status == ServiceStatus.JOURNEY_STARTED) {
                serviceProgressActivity?.drawPolyLine(
                    orderDetail.user?.live_lat ?: orderDetail.user?.lat,
                    orderDetail.user?.live_long ?: orderDetail.user?.long,
                    orderDetail.lat, orderDetail.long
                )
            } else {
                serviceProgressActivity?.clearPolyline()
            }

            serviceProgressActivity?.showMarker(
                LatLng(
                    orderDetail.lat ?: 0.0, orderDetail.long
                        ?: 0.0
                )
            )
        }
    }

    private fun setListeners() {
        /* val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                 0.0f)
         val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
         val latlng = LatLng(lat.toDouble(), long.toDouble())*/

        tvAccept.setOnClickListener {
            if (isConnectedToInternet(activity, true)) {
                requestAcceptReject = true

                val hashMap = HashMap<String, Any>()

                hashMap["status"] = orderDetail.status?.plus(1) ?: 0

                val id = if (orderDetail.wash_request_id == null)
                    orderDetail.id ?: 0
                else
                    orderDetail.wash_request_id ?: 0

                viewModelAcceptReject.acceptRejectRequest(id, hashMap)
            }
        }
        ivCall.setOnClickListener {
            val phone = orderDetail.user?.full_phone.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }

    }

}
