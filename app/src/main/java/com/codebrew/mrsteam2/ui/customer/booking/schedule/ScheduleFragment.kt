package com.codebrew.mrsteam2.ui.customer.booking.schedule


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.paymenttype.PaymentTypeFragment
import com.codebrew.mrsteam2.utils.CategoryId
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.getFormatFromDate
import com.codebrew.mrsteam2.utils.getFormatFromDateUtc
import kotlinx.android.synthetic.main.fragment_schedule.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class ScheduleFragment : Fragment(), View.OnClickListener {

    private var serviceRequest: ServiceRequestModel? = null

    private var selectedDate = Calendar.getInstance(Locale.getDefault())

    var scheduledTime = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel
        selectedDate = Calendar.getInstance(Locale.getDefault())
        setDateTimeViews()
        setListeners()
    }

    private fun setListeners() {
        tvDate.setOnClickListener(this)
        tvTime.setOnClickListener(this)
        tvNext.setOnClickListener(this)

        spnr_Time?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                var selectedItem = parent.selectedItem
                val df = SimpleDateFormat("yyyy/MM/dd")
                val date = df.format(Calendar.getInstance().time)
                println("selectedItem " + selectedItem)
                if (selectedItem != null) {
                    var lastHour = selectedItem?.toString().split("- ")[1]
                    val currentTime = getFormatFromDate(selectedDate.time, "yyyy/MM/dd hh:mm a").toString()
                    if (calculateTimeDifference(currentTime, date + " " + lastHour) < 1) {
                        Toast.makeText(
                            activity,
                            getString(R.string.schedule_time_selection_validation_msg),
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        scheduledTime = lastHour
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tvDate -> {
                showDatePicker()
            }
            R.id.tvTime -> {
                showTimePicker()
            }
            R.id.tvNext -> {
                if (spnr_Time.selectedItem != null) {

                  var selectedD =   getFormatFromDate(selectedDate.time, "yyyy-MM-dd")

                    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm a")
                    var dateTime = simpleDateFormat.parse(selectedD+" "+scheduledTime)//"08:00 AM")


                    serviceRequest?.orderTime = dateTime
                    serviceRequest?.order_timings = getFormatFromDateUtc(dateTime, "yyyy-MM-dd HH:mm:ss")
                    serviceRequest?.order_timings_text = getFormatFromDate(dateTime, "EEE, MMM d h:mm a")
                    fragmentManager?.beginTransaction()?.replace(R.id.container, PaymentTypeFragment())
                        ?.addToBackStack("backstack")?.commit()
                } else {
                    Toast.makeText(
                        activity,
                        getString(R.string.select_scheduling_time),
                        Toast.LENGTH_LONG
                    ).show()
                }

                /*val tempCal = Calendar.getInstance()
                tempCal.add(Calendar.HOUR_OF_DAY, 1)
                if (selectedDate.before(tempCal)) {
                    Toast.makeText(
                        activity,
                        getString(R.string.schedule_time_selection_validation_msg),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    selectedDate.add(Calendar.MINUTE, 5)
                    serviceRequest?.orderTime = selectedDate.time
                    serviceRequest?.order_timings = getFormatFromDateUtc(selectedDate.time, "yyyy-MM-dd HH:mm:ss")
                    serviceRequest?.order_timings_text = getFormatFromDate(selectedDate.time, "EEE, MMM d h:mm a")
                    fragmentManager?.beginTransaction()?.replace(R.id.container, PaymentTypeFragment())
                        ?.addToBackStack("backstack")?.commit()
                }*/
            }
        }
    }

    private fun showDatePicker() {
        val datePicker = DatePickerDialog(
            activity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                selectedDate.set(Calendar.YEAR, year)
                selectedDate.set(Calendar.MONTH, month)
                selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val tempCalendar = Calendar.getInstance()
                if (selectedDate.time.before(tempCalendar.time)) {
                    selectedDate = tempCalendar.clone() as Calendar
                }
                setDateTimeViews()
            }, selectedDate.get(Calendar.YEAR),
            selectedDate.get(Calendar.MONTH),
            selectedDate.get(Calendar.DAY_OF_MONTH)
        )

        datePicker.setCanceledOnTouchOutside(true)
        val tempCal = Calendar.getInstance()
        datePicker.datePicker.minDate = tempCal.timeInMillis
        if (serviceRequest?.category_id == CategoryId.MINERAL_WATER) {
            tempCal.add(Calendar.DAY_OF_MONTH, 6)
            tempCal.set(Calendar.HOUR_OF_DAY, 23)
            tempCal.set(Calendar.MINUTE, 59)
            tempCal.set(Calendar.SECOND, 0)
            datePicker.datePicker.maxDate = tempCal.timeInMillis
        }
        datePicker.show()
    }

    private fun showTimePicker() {
        val timePicker = TimePickerDialog(
            activity, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                val selectedTimeCalendar = selectedDate.clone() as Calendar
                selectedTimeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                selectedTimeCalendar.set(Calendar.MINUTE, minute)
                selectedTimeCalendar.set(Calendar.SECOND, 0)
                val tempCalendar = Calendar.getInstance()
                tempCalendar.add(Calendar.HOUR_OF_DAY, 1)
                tempCalendar.add(Calendar.MINUTE, 1)
                tempCalendar.set(Calendar.SECOND, 0)
                if (selectedTimeCalendar.time.before(tempCalendar.time)) {
                    Toast.makeText(
                        activity,
                        getString(R.string.schedule_time_selection_validation_msg),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    selectedDate = selectedTimeCalendar.clone() as Calendar
                    setDateTimeViews()
                }
            }, selectedDate.get(Calendar.HOUR_OF_DAY),
            selectedDate.get(Calendar.MINUTE),
            false
        )
        timePicker.setCanceledOnTouchOutside(true)
        timePicker.show()
    }


    private fun showListOfTimeScheduling() {
        var array = resources.getStringArray(R.array.spinnerItems)
        val currentTime = getFormatFromDate(selectedDate.time, "yyyy/MM/dd hh:mm a").toString()

        var spnrArray = arrayListOf<String>()

        val df = SimpleDateFormat("yyyy/MM/dd")
        val date = df.format(Calendar.getInstance().time)

        for (i in array.indices) {
            var lastHour = array[i].toString().split("- ")[1]
            if (calculateTimeDifference(currentTime, date + " " + lastHour) >= 1) {
                spnrArray.add(array[i])
            }
        }

        val adp = ArrayAdapter(
            activity,
            R.layout.spinner_text,
            spnrArray
        )

        spnr_Time.setAdapter(adp)
    }


    private fun setDateTimeViews() {
        tvDate.text = getFormatFromDate(selectedDate.time, "EEE, dd MMM")
        tvTime.text = getFormatFromDate(selectedDate.time, "h:mm a")

        showListOfTimeScheduling()
    }

    fun calculateTimeDifference(str_date1: String, str_date2: String): Int {
        val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd hh:mm a")
        var date1 = simpleDateFormat.parse(str_date1)//"08:00 AM")
        var date2 = simpleDateFormat.parse(str_date2)//"04:00 PM")

        val simpleDateFormat_scnd = SimpleDateFormat("dd-MM-yyyy")
        var date_f = simpleDateFormat_scnd.format(date1)
        var date_s = simpleDateFormat_scnd.format(date2)

        var fDate = date_f.split("-")[0].toInt()
        var sDate = date_s.split("-")[0].toInt()

        var difference: Long = 0
        if (fDate > sDate) {
            difference = date1.getTime() - date2.getTime()
        } else difference = date2.getTime() - date1.getTime()

        var days = (difference / (1000 * 60 * 60 * 24)).toInt()
        var hours = ((difference - 1000 * 60 * 60 * 24 * days) / (1000 * 60 * 60)).toInt()
        Log.i("======= Hours", " :: $hours")
        return hours
    }


}
