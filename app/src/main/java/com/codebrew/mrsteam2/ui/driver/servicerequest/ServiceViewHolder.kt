package com.codebrew.mrsteam2.ui.driver.servicerequest

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.network.responsemodels.Wash

class ServiceViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    init {
        itemView?.setOnClickListener {

        }
    }

    fun onBind(orderDetails: Wash) {

    }
}