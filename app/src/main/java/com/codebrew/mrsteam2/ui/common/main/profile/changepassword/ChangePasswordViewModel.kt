package com.codebrew.mrsteam2.ui.common.main.profile.changepassword


import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.ChangePasswordRequest
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ChangePasswordViewModel : ViewModel() {
    val changeRes by lazy { SingleLiveEvent<Resource<UserData?>>() }

    fun changePassword(changeReq: ChangePasswordRequest) {
        changeRes.value = Resource.loading()

        RetrofitClient.getApi().changePassword(changeReq)
                .enqueue(object : Callback<ApiResponse<UserData?>> {
                    override fun onFailure(call: Call<ApiResponse<UserData?>>, throwable: Throwable) {
                        changeRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<UserData?>>,
                            response: Response<ApiResponse<UserData?>>
                    ) {
                        if (response.isSuccessful) {
                            changeRes.value = Resource.success(response.body()?.data)
                        } else {
                            changeRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}