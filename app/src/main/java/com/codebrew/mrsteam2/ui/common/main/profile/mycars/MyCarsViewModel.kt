package com.codebrew.mrsteam2.ui.common.main.profile.mycars

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyCarsViewModel : ViewModel() {
    val myCarRes by lazy { SingleLiveEvent<Resource<ArrayList<MyCars>>>() }
    fun myCars() {
        myCarRes.value = Resource.loading()


        RetrofitClient.getApi().myCars()
            .enqueue(object : Callback<ApiResponse<ArrayList<MyCars>>> {
                override fun onFailure(call: Call<ApiResponse<ArrayList<MyCars>>>, throwable: Throwable) {
                    myCarRes.value = Resource.error(ApiUtils.failure(throwable))
                }

                override fun onResponse(
                    call: Call<ApiResponse<ArrayList<MyCars>>>,
                    response: Response<ApiResponse<ArrayList<MyCars>>>
                ) {
                    if (response.isSuccessful) {
                        myCarRes.value = Resource.success(response.body()?.data)
                    } else {
                        myCarRes.value = Resource.error(
                            ApiUtils.getError(
                                response.code(),
                                response.errorBody()?.string()
                            )
                        )
                    }
                }

            })
    }
}