package com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.NumberPicker
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.AddCarRequest
import com.codebrew.mrsteam2.network.requestmodels.AddCarRvAdapter
import com.codebrew.mrsteam2.network.responsemodels.ImageModel
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.profile.editprofile.ImageUploadViewModel
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_add_cars.*
import okhttp3.MediaType
import okhttp3.RequestBody
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class AddCarActivity : AppCompatActivity() {


    private lateinit var vehicleAdapter: AddCarAdapter

    private lateinit var colorAdapter: AddCarAdapter

    private var fileToUpload: File? = null
    private lateinit var viewModelUpload: ImageUploadViewModel
    private lateinit var viewModel: AddCarViewModel

    private lateinit var progressDialog: ProgressDialog

    var colorIndex = 0

    var vehicleIndex = 0

    private var itemsColor = ArrayList<AddCarRvAdapter>()

    private val itemsCars = ArrayList<AddCarRvAdapter>()

    private var carEdit: MyCars? = null
    private var imageModel: ImageModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_cars)
        listener()
        adapterSetting()
        initialise()
        liveData()
    }


    private fun listener() {
        btnSave.setOnClickListener {
            validation()
        }
        tbrAddCars.setNavigationOnClickListener {
            onBackPressed()
        }

        etCarYear.setOnClickListener {
            showYearDialog()
        }
        ivCarImage.setOnClickListener {
            getStorageWithPermissionCheck()
        }
    }


    /*Year Dialog*/
    private fun showYearDialog() {
        val year = Calendar.getInstance().get(Calendar.YEAR)

        val d = Dialog(this)
        d.setTitle(getString(R.string.str_year))
        d.setContentView(R.layout.dialog_year)
        val btnSet = d.findViewById(R.id.btnSet) as TextView
        val btnCancel = d.findViewById(R.id.btnCancel) as TextView

        val npYear = d.findViewById(R.id.npYear) as NumberPicker

        npYear.maxValue = year
        npYear.minValue = year - 50
        npYear.wrapSelectorWheel = false
        npYear.value = year
        npYear.descendantFocusability = NumberPicker.FOCUS_BLOCK_DESCENDANTS

        btnSet.setOnClickListener {
            etCarYear.setText(npYear.value.toString())
            d.dismiss()
        }
        btnCancel.setOnClickListener { d.dismiss() }
        d.show()
    }

    private fun adapterSetting() {

        itemsColor = ArrayList()
        var item1 = AddCarRvAdapter(ColorType.GREEN, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.ORANGE, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.GREY, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.RED, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.BLUE, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.WHITE, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.BLACK, false)
        itemsColor.add(item1)
        item1 = AddCarRvAdapter(ColorType.BROWN, false)
        itemsColor.add(item1)

        rvColor.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        colorAdapter = AddCarAdapter(itemsColor, this, true)
        rvColor.adapter = colorAdapter


        var item2 = AddCarRvAdapter(VehicleType.SMALL, false)
        itemsCars.add(item2)
        item2 = AddCarRvAdapter(VehicleType.SEDAN, false)
        itemsCars.add(item2)
        item2 = AddCarRvAdapter(VehicleType.MEDIUM, false)
        itemsCars.add(item2)
        item2 = AddCarRvAdapter(VehicleType.LARGE_TRUCK, false)
        itemsCars.add(item2)

        rvVehicleType.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        vehicleAdapter = AddCarAdapter(itemsCars, this, false)
        rvVehicleType.adapter = vehicleAdapter
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[AddCarViewModel::class.java]
        viewModelUpload = ViewModelProviders.of(this)[ImageUploadViewModel::class.java]
        progressDialog = ProgressDialog(this)
        imageModel = ImageModel()

        if (intent.hasExtra(EXTRA_CAR)) {
            tvTitle.text = getString(R.string.update_car)
            carEdit = intent.getSerializableExtra(EXTRA_CAR) as MyCars

            etCarModel.setText(carEdit?.model)
            etCarYear.setText((carEdit?.year ?: 0).toString())
            etPlateNumber.setText(carEdit?.plate_number)

            /* colorIndex = carEdit?.color?.toInt() ?: 0
             itemsColor[colorIndex - 1].isSelected = true
             colorAdapter.notifyDataSetChanged()*/

            vehicleIndex = carEdit?.vehicle_type?.toInt() ?: 0
            itemsCars[vehicleIndex - 1].isSelected = true
            vehicleAdapter.notifyDataSetChanged()


            loadImage(this, ivCarImage, carEdit?.image_url?.original, carEdit?.image_url?.thumbnail)
            imageModel?.original = carEdit?.image
            imageModel?.thumbnail = carEdit?.thumbnail_image
        }
    }


    private fun addCarApiHit() {
        if (isConnectedToInternet(this, true)) {
            val addCarReq = AddCarRequest(
                model = etCarModel.text.toString(),
                plate_number = etPlateNumber.text.toString(),
                vehicle_type = vehicleIndex,
                /*  color = colorIndex,*/
                year = etCarYear.text.toString(),
                image = imageModel
            )

            if (carEdit != null) {
                viewModel.editCar(carEdit?.id ?: 0, addCarReq)

            } else
                viewModel.addCar(addCarReq)
        }
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(
            ivCarImage.context,
            R.string.permission_required_to_select_image,
            request
        )
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(
            ivCarImage.context,
            R.string.permission_required_to_select_image
        )
    }


    private fun liveData() {
        viewModel.addCarRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModel.editCar.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    setResult(Activity.RESULT_OK)
                    finish()
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModelUpload.uploadFile.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    /*  loadImage(
                          this, ivCarImage, resources.data?.thumbnail
                              ?: "", resources.data?.original ?: ""
                      )*/

                    imageModel?.thumbnail = resources.data?.thumbnail
                    imageModel?.original = resources.data?.original


                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

    private fun validation() {
        tilCarModel.error = null
        tilCarYear.error = null
        tilPlateNumber.error = null

        return when {
            etCarModel.text.toString().isEmpty() -> {
                invalidStringInput(etCarModel, tilCarModel, getString(R.string.error_empty_car_model))
            }
            etCarYear.text.toString().isEmpty() -> {
                invalidStringInput(etCarYear, tilCarYear, getString(R.string.error_empty_car_year))
            }
            etPlateNumber.text.toString().isEmpty() -> {
                invalidStringInput(etPlateNumber, tilPlateNumber, getString(R.string.error_empty_year))
            }
            imageModel?.original == null -> {
                ivCarImage.showSnack(getString(R.string.please_add_car_image))
            }

            /*  colorIndex == 0 -> {
                  rvColor.showSnack(getString(R.string.str_color))
              }*/
            vehicleIndex == 0 -> {
                rvColor.showSnack(getString(R.string.str_vehicle_type))
            }
            else -> {
                addCarApiHit()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    fileToUpload = let { ImageUtils.getFile(it) }
                    addImage()
                }
                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    fileToUpload = data?.data?.let {
                        ImageUtils.getImagePathFromGallery(this, it)
                    }
                    addImage()
                }
            }
        }
    }

    private fun addImage() {

        if (getFileSizeLess5MB(fileToUpload)) {
            if (isConnectedToInternet(this, true)) {
                val map = HashMap<String, RequestBody>()
                map["type"] =
                    RequestBody.create(MediaType.parse("text/plain"), DocType.IMAGE)
                if (fileToUpload != null && fileToUpload?.exists() == true) {
                    val body: RequestBody =
                        RequestBody.create(MediaType.parse("image/jpeg"), fileToUpload)

                    map["image\"; fileName=\"" + fileToUpload?.name] = body
                }

                loadImage(this, ivCarImage, fileToUpload?.absolutePath, fileToUpload?.absolutePath)

                viewModelUpload.uploadFile(map)
            }
        } else {
            fileToUpload = null
            ivCarImage.showSnack(getString(R.string.file_size_more))
        }
    }

}


