package com.codebrew.mrsteam2.ui.common.main.profile.changepassword

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ChangePasswordRequest
import com.codebrew.mrsteam2.network.requestmodels.ResetPasswordRequest
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_change_password.*


class ChangePasswordActivity : AppCompatActivity() {
    private lateinit var viewModel: ChangePasswordViewModel
    private lateinit var resetViewModel: ResetPasswordViewModel
    private lateinit var progressDialog: ProgressDialog
    private var oTP: Int = 0
    private var userID: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        oTP = intent.getIntExtra(OTP, 0)
        userID = intent.getIntExtra(USER_ID, 0)

        if (oTP != 0) {
            tilCurrentPassword.gone()
            tilOTP.visible()
            tvTitle.text = getString(R.string.str_reset_password)
        }
        listeners()
        initialise()
        liveData()
    }


    private fun listeners() {
        tbrChangePassword.setNavigationOnClickListener {
            onBackPressed()
        }
        tvSave.setOnClickListener {
            validation()
        }
    }

    private fun validation() {
        tilOTP.error = null
        tilCurrentPassword.error = null
        tilNewPassword.error = null
        tilConfirmNewPassword.error = null

        if (oTP != 0) {
            when {
                etOTP.text.toString().isEmpty() -> {
                    invalidStringInput(etOTP, tilOTP, getString(R.string.error_empty_OTP))
                }
                etNewPassword.text.toString().isEmpty() -> {
                    invalidStringInput(etNewPassword, tilNewPassword, getString(R.string.error_empty_password))
                }
                etConfirmNewPassword.text.toString().isEmpty() -> {
                    invalidStringInput(
                            etConfirmNewPassword,
                            tilConfirmNewPassword,
                            getString(R.string.error_empty_password)
                    )
                }
                (!PASSWORD_FORMAT.matcher(etNewPassword.text.toString()).matches()) -> {
                    invalidString(etNewPassword, getString(R.string.str_password_error))
                }
                etNewPassword.text.toString() != etConfirmNewPassword.text.toString() -> {
                    invalidStringInput(
                            etConfirmNewPassword,
                            tilConfirmNewPassword,
                            getString(R.string.error_password_matching)
                    )
                }
                else -> {
                    resetPasswordApiHit()
                }
            }
        } else {
            when {
                etCurrentPassword.text.toString().isEmpty() -> {
                    invalidStringInput(etCurrentPassword, tilCurrentPassword, getString(R.string.error_empty_password))
                }
                etNewPassword.text.toString().isEmpty() -> {
                    invalidStringInput(etNewPassword, tilNewPassword, getString(R.string.error_empty_password))
                }
                etConfirmNewPassword.text.toString().isEmpty() -> {
                    invalidStringInput(
                            etConfirmNewPassword,
                            tilConfirmNewPassword,
                            getString(R.string.error_empty_password)
                    )
                }
                etCurrentPassword.text.toString() == etNewPassword.text.toString() -> {
                    invalidStringInput(etNewPassword, tilNewPassword, getString(R.string.error_old_new_password))
                }
                (!PASSWORD_FORMAT.matcher(etNewPassword.text.toString()).matches()) -> {
                    invalidString(etNewPassword, getString(R.string.str_password_error))
                }
                etNewPassword.text.toString() != etConfirmNewPassword.text.toString() -> {
                    invalidStringInput(
                            etConfirmNewPassword,
                            tilConfirmNewPassword,
                            getString(R.string.error_password_matching)
                    )
                }
                else -> changePasswordApiHit()
            }
        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[ChangePasswordViewModel::class.java]
        resetViewModel = ViewModelProviders.of(this)[ResetPasswordViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }

    private fun changePasswordApiHit() {
        val changeReq = ChangePasswordRequest(
                old_password = etCurrentPassword.text.toString(),
                password = etNewPassword.text.toString(),
                password_confirmation = etConfirmNewPassword.text.toString()
        )
        if (isConnectedToInternet(this, true))
            viewModel.changePassword(changeReq)
    }

    private fun resetPasswordApiHit() {
        val resetReq = ResetPasswordRequest(
                otp = oTP,
                user_id = userID,
                password = etNewPassword.text.toString()
        )
        if (isConnectedToInternet(this, true))
            resetViewModel.resetPassword(resetReq)
    }

    private fun liveData() {
        viewModel.changeRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    setResult(Activity.RESULT_OK)
                    finish()
                    longToast(getString(R.string.password_changed))
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        resetViewModel.resetRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    longToast(resources.data?.msg.toString())
                    startActivity(Intent(this, LoginSignupActivity::class.java))
                    finish()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }
}