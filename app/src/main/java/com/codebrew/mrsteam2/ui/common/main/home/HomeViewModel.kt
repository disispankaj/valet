package com.codebrew.mrsteam2.ui.common.main.home

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Home
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {
    val homeRes by lazy { SingleLiveEvent<Resource<Home>>() }

    fun home() {
        homeRes.value = Resource.loading()

        RetrofitClient.getApi().home()
                .enqueue(object : Callback<ApiResponse<Home>> {
                    override fun onFailure(call: Call<ApiResponse<Home>>, throwable: Throwable) {
                        homeRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<Home>>,
                            response: Response<ApiResponse<Home>>
                    ) {
                        if (response.isSuccessful) {
                            homeRes.value = Resource.success(response.body()?.data)
                        } else {
                            homeRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}