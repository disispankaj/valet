package com.codebrew.mrsteam2.ui.customer.booking.confirmbooking


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.bookmakepayment.MakePaymentFragment
import com.codebrew.mrsteam2.ui.customer.booking.dropofflocation.DropOffLocationFragment
import com.codebrew.mrsteam2.ui.customer.booking.service.SubServiceAdapter
import com.codebrew.mrsteam2.ui.customer.booking.service.SubServiceAdapter1
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.getFormatFromDate
import com.codebrew.mrsteam2.utils.gone
import kotlinx.android.synthetic.main.fragment_confirm_booking.*
import java.util.*

/**
 * Fragment to get the details of the order for the service: Gas, Mineral Water and Water Tanker
 * after getting the location and addresses from [DropOffLocationFragment]
 * */
class ConfirmBookingFragment : Fragment() {

    private lateinit var serviceRequest: ServiceRequestModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_booking, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()
        setData()

    }


    private fun setData() {

        tvCarV.text = serviceRequest.carName
        tvLocationV.text = serviceRequest.dropoff_address

        if (serviceRequest.future == ServiceRequestModel.BOOK_NOW) {
            tvSchedule.gone()
            clSchedule.gone()
        } else {
            tvDateV.text = getFormatFromDate(serviceRequest.orderTime ?: Date(), "dd,MMM yy")
            tvTime.text = getFormatFromDate(serviceRequest.orderTime ?: Date(), "hh:mm a")
        }

        tvServiceName.text = serviceRequest.serviceName
        rvServiceDetails.layoutManager = LinearLayoutManager(activity)
        val subServiceAdapter = SubServiceAdapter(serviceRequest.subserviceId, this, false, false)
        rvServiceDetails.adapter = subServiceAdapter
    }


    private fun setListeners() {

        tbToolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        btnNext.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(R.id.container, MakePaymentFragment())
                    ?.addToBackStack("backstack")?.commit()
        }

    }
}
