package com.codebrew.mrsteam2.ui.common.loginsignup.signup


import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.SignupRequest
import com.codebrew.mrsteam2.network.responsemodels.SignUp
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignUpViewModel : ViewModel() {
    val signupRes by lazy { SingleLiveEvent<Resource<SignUp>>() }

    fun signUp(signupReq: SignupRequest) {

        signupRes.value = Resource.loading()
        RetrofitClient.getApi().signUp(signupReq)
                .enqueue(object : Callback<ApiResponse<SignUp>> {
                    override fun onFailure(call: Call<ApiResponse<SignUp>>, throwable: Throwable) {
                        signupRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<SignUp>>,
                            response: Response<ApiResponse<SignUp>>
                    ) {
                        if (response.isSuccessful) {
                            signupRes.value = Resource.success(response.body()?.data)
                        } else {
                            signupRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }


}