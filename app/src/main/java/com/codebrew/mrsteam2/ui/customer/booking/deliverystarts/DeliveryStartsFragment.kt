package com.codebrew.mrsteam2.ui.customer.booking.deliverystarts


import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.LinearInterpolator
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.PolylineModel
import com.codebrew.mrsteam2.network.responsemodels.TrackingModel
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.AcceptRequestViewModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.invoice.InvoiceFragment
import com.codebrew.mrsteam2.ui.customer.booking.selectcar.SelectCarFragment
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.utils.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.fragment_delivery_starts.*
import org.json.JSONObject
import java.util.*


class DeliveryStartsFragment : BaseFragment() {


    private var line: Polyline? = null

    private var mMap: GoogleMap? = null

    private var order: Wash? = null

    private var pathArray = ArrayList<String>()

    private var list = ArrayList<LatLng>()

    private var sourceLatLong: LatLng? = null

    private var destLong: LatLng? = null

    /* Checks if all events are idle for the time */
    private var checkIdleTimer = Timer()

    private var isPaused = false

    private var isHeavyLoadsVehicle = false

    private var mapType: Int? = null

    var valueAnimatorMove: ValueAnimator? = null

    private lateinit var orderDetail: Wash

    private var bookingActivity: BookingActivity? = null

    private var progressDialog: ProgressDialog? = null

    private lateinit var viewModelAcceptReject: AcceptRequestViewModel

    private val ratingDrawables = listOf(
            R.drawable.notification,
            R.drawable.notification,
            R.drawable.notification,
            R.drawable.notification,
            R.drawable.notification
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_delivery_starts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //presenter.attachView(this)
        //presenter.onGoingOrderApi()


        initialise()
        setupMap()
        setListeners()
        setData()
        liveData()
    }

    private fun initialise() {
        bookingActivity = activity as BookingActivity
        bookingActivity?.getMapAsync()

        progressDialog = ProgressDialog(activity)

        orderDetail = Gson().fromJson(arguments?.getString(ORDER_PROGRESS), Wash::class.java)
        viewModelAcceptReject = ViewModelProviders.of(this)[AcceptRequestViewModel::class.java]
    }


    override fun onResume() {
        super.onResume()
        isPaused = false
        if (AppSocket.get().isConnected) {
            restartCheckIdleTimer(0)
        }
        AppSocket.get().on(AppSocket.Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().on(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().on(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    private fun setData() {
        if (orderDetail.status == ServiceStatus.COMPLETED) {
//            startActivity(Intent(activity, DetailActivity::class.java)
//                    .putExtra(DETAIL_DATA, activity))

            if (orderDetail.is_reviewed == true)
                startActivity(Intent(activity, DetailActivity::class.java)
                    .putExtra(DETAIL_DATA, orderDetail))
            else {
                startActivity(Intent(activity, RateReviewActivity::class.java)
                    .putExtra(DETAIL_DATA, orderDetail))
            }

            activity?.setResult(Activity.RESULT_OK)
            activity?.finish()
        } else {
            when (orderDetail.status) {
                ServiceStatus.ACCEPT -> {
                    tvDriverStatus.text = getString(R.string.accept)
                    tvCancel.visible()
                }
                ServiceStatus.JOURNEY_STARTED -> {
                    tvDriverStatus.text = getString(R.string.on_the_way)
                    tvCancel.visible()
                }
                ServiceStatus.WASHING_STARTED -> {
                    tvDriverStatus.text = getString(R.string.start_washing)
                    tvCancel.gone()
                }
                else -> {
                    tvDriverStatus.text = getString(R.string.on_the_way)
                    tvCancel.visible()
                }
            }

        }

        tvDriverName.text = orderDetail.user?.name
        loadImage(activity, ivDriverImage, orderDetail.user?.image_url?.thumbnail
                ?: "", orderDetail.user?.image_url?.original ?: "")

        if (bookingActivity?.googleMapHome != null)
            updateTrack()

    }

    private fun rejectApiCall(confirm: Boolean) {
        if (isConnectedToInternet(activity, true)) {
            val hashMap = HashMap<String, Any>()

            hashMap["status"] = ServiceStatus.CANCLE_BY_USER
            if (confirm)
                hashMap["is_confirmed"] = true

            val id = if (orderDetail.wash_request_id == null)
                orderDetail.id ?: 0
            else
                orderDetail.wash_request_id ?: 0

            viewModelAcceptReject.acceptRejectRequest(id, hashMap)
        }
    }

    private fun liveData() {
        viewModelAcceptReject.acceptRejectRequest.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog?.setLoading(false)

                    if (it.data?.need_confirmation == true) {
                        confirmRequestCancel()
                    } else {
                        activity?.finish()
                        Toast.makeText(activity, "Request cancelled", Toast.LENGTH_LONG).show()
                    }
                }

                Status.ERROR -> {
                    progressDialog?.setLoading(false)
                    ApisRespHandler.handleError(it.error, activity)
                }

                Status.LOADING -> {
                    progressDialog?.setLoading(true)
                }
            }
        })
    }

    private fun confirmRequestCancel() {
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.alert,
                R.string.cancel_dialog_message, R.string.yes,
                R.string.no, true,
                object : AlertDialogUtil.OnOkCancelDialogListener {
                    override fun onOkButtonClicked() {
                        rejectApiCall(true)
                    }

                    override fun onCancelButtonClicked() {
                    }
                }).show()
    }

    fun updateTrack() {
        if (isVisible) {
            if (orderDetail.status == ServiceStatus.JOURNEY_STARTED) {
                bookingActivity?.drawPolyLine(orderDetail.lat, orderDetail.long,
                        orderDetail.user?.live_lat ?: orderDetail.user?.lat,
                        orderDetail.user?.live_long ?: orderDetail.user?.long)
            } else {
                bookingActivity?.clearPolyline()
            }


            bookingActivity?.showMarker(LatLng(orderDetail.lat ?: 0.0, orderDetail.long ?: 0.0),
                    LatLng(orderDetail.user?.live_lat ?: orderDetail.user?.lat ?: 0.0,
                            orderDetail.user?.live_long ?: orderDetail.user?.long ?: 0.0))
        }
    }

    override fun onPause() {
        super.onPause()
        isPaused = true
        checkIdleTimer.cancel()
        AppSocket.get().off(AppSocket.Events.ORDER_EVENT, orderEventListener)
        AppSocket.get().off(Socket.EVENT_CONNECT, onSocketConnected)
        AppSocket.get().off(Socket.EVENT_DISCONNECT, onSocketDisconnected)
    }

    override fun onDestroyView() {
        dialog?.cancel()
        AppSocket.get().off(AppSocket.Events.ORDER_EVENT)
        AppSocket.get().off(AppSocket.Events.COMMON_EVENT)
        // presenter.detachView()
        super.onDestroyView()

    }

    override fun onNetworkConnected() {

    }

    override fun onNetworkDisconnected() {

    }

    private val onSocketConnected = Emitter.Listener {
        activity?.runOnUiThread {
            if (!isPaused) {
                restartCheckIdleTimer(0)
            }
        }
    }

    private val onSocketDisconnected = Emitter.Listener {
        checkIdleTimer.cancel()
    }

    private fun setupMap() {
        //mMap = (activity as BookingActivity).googleMapHome
        if (map != null) {
            mMap?.clear()
        }
        mapType = PrefsManager.get().getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)

        order = Gson().fromJson(arguments?.getString(ORDER_PROGRESS), Wash::class.java)
        bookingActivity?.ivMarker?.visibility = View.GONE
        sourceLatLong = LatLng(order?.driver?.latitude ?: 0.0, order?.driver?.longitude ?: 0.0)
        when (order?.category_id) {
            CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                isHeavyLoadsVehicle = false
                destLong = LatLng(order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude ?: 0.0)
            }
            CategoryId.FREIGHT, CategoryId.TOW -> {
                isHeavyLoadsVehicle = true
                if (order?.order_status == OrderStatus.CONFIRMED) {
                    destLong = LatLng(order?.pickup_latitude ?: 0.0, order?.pickup_longitude ?: 0.0)
                } else {
                    destLong = LatLng(
                            order?.dropoff_latitude ?: 0.0, order?.dropoff_longitude
                            ?: 0.0
                    )
                }
            }
        }
        showMarker(sourceLatLong, destLong)
        reFocusMapCamera()
    }

    @SuppressLint("WrongConstant")
    private fun setListeners() {
        fabMyLocation.setOnClickListener {
            reFocusMapCamera()
        }
        tvCancel.setOnClickListener {
            rejectApiCall(false)
            //showCancellationDialog()
        }

        cvToolbar.setNavigationOnClickListener {
            activity?.finish()
        }
        /*ivProfile.setOnClickListener {
            activity?.drawer_layout?.openDrawer(Gravity.START)
        }

        ivSupport.setOnClickListener {
            activity?.drawer_layout?.openDrawer(Gravity.END)
        }*/


        ivCall.setOnClickListener {
            val phone = orderDetail.user?.full_phone.toString()
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }

    // Socket Listener for orders events
    private val orderEventListener = Emitter.Listener { args ->
        Logger.e("orderEventListener", args[0].toString())
        restartCheckIdleTimer(20000)
        activity?.runOnUiThread {

            when (JSONObject(args[0].toString()).getString("type")) {
                OrderEventType.SERVICE_COMPLETE -> {
                    val orderJson = JSONObject(args[0].toString()).getJSONArray("order").get(0).toString()
                    val orderModel = Gson().fromJson(orderJson, Wash::class.java)
                    if (orderModel.order_id == order?.order_id && !isPaused) {
                        openInvoiceFragment(orderJson)
                    }
                }

                OrderEventType.CURRENT_ORDERS -> {
                    val track = Gson().fromJson(args[0].toString(), TrackingModel::class.java)
                            ?: null
                    if (track?.order_id == order?.order_id && !isPaused) {
                        sourceLatLong = LatLng(track?.latitude ?: 0.0, track?.longitude ?: 0.0)
//                        if (track?.polyline != null && track.polyline?.points?.isEmpty() == false) {
//                            drawPolylineFromDriver(track.polyline)
//                            if (sourceLatLong != null) {
//                                animateMarker(sourceLatLong, track.bearing?.toFloat())
//                            }
//                            if (isHeavyLoadsVehicle && track.order_status == OrderStatus.ONGOING) {
//                                destLong = LatLng(order?.dropoff_latitude
//                                        ?: 0.0, order?.dropoff_longitude ?: 0.0)
//                                destMarker?.position = LatLng(destLong?.latitude
//                                        ?: 0.0, destLong?.longitude ?: 0.0)
//                                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
//                            }
//                        } else {
                        // presenter.getRoadPoints(track)
//                        }
                        if (track?.order_status == OrderStatus.CONFIRMED) {
                            tvDriverStatus.text = getString(R.string.app_name)
                            tvTime.visibility = View.VISIBLE
                        } else if (track?.order_status == OrderStatus.ONGOING) {
                            if (track.my_turn == "1") {
                                tvDriverStatus.text = getString(R.string.app_name)
                                tvTime.visibility = View.VISIBLE
                            } else {
                                tvDriverStatus.text = getString(R.string.app_name)
                                tvTime.visibility = View.INVISIBLE
                            }
                        }
                    }
                }

                OrderStatus.DRIVER_CANCELLED -> {
                    openServiceFragment()
                    Toast.makeText(activity, getString(R.string.app_name), Toast.LENGTH_LONG)
                            .show()
                }
            }
        }
    }

    /*override fun snappedPoints(response: List<RoadItem>?, trackingModel: TrackingModel?) {
        if ((response?.size ?: 0) > 0) {
            sourceLatLong = LatLng(
                response?.get(0)?.location?.latitude ?: 0.0,
                response?.get(0)?.location?.longitude ?: 0.0
            )
            if (isHeavyLoadsVehicle && trackingModel?.order_status == OrderStatus.ONGOING) {
                destLong = LatLng(
                    order?.dropoff_latitude
                        ?: 0.0, order?.dropoff_longitude ?: 0.0
                )
                destMarker?.position = LatLng(
                    destLong?.latitude
                        ?: 0.0, destLong?.longitude ?: 0.0
                )
                destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_location_mrkr)))
            }
            presenter.drawPolyLine(
                sourceLat = trackingModel?.latitude
                    ?: 0.0, sourceLong = trackingModel?.longitude ?: 0.0,
                destLat = destLong?.latitude ?: 0.0, destLong = destLong?.longitude
                    ?: 0.0, language = Locale.US.language
            )
            if (sourceLatLong != null) {
                animateMarker(sourceLatLong, trackingModel?.bearing?.toFloat())
//                            carMarker?.let { animateMarker(it, sourceLatLong!!, false) }
//                            carMarker?.let {
//                                rotateMarker(it, track.bearing?.toFloat() ?: 0f, startRotation)
//                            }
            }
        }
    }


    override fun onApiSuccess(response: List<Order>?) {
        if (response?.isNotEmpty() == true) {
            tvDriverName?.text = response[0].driver?.name
            if (response[0].driver?.rating_count ?: 0 > 0 && response[0].driver?.rating_avg?.toInt() ?: 0 != 0) {
                val rating = if ((response[0].driver?.rating_avg?.toInt() ?: 0) <= 5) {
                    response[0].driver?.rating_avg?.toInt()
                } else {
                    5
                }
                ivRating.setImageResource(ratingDrawables[rating?.minus(1) ?: 0])
                tvRating.text = " · " + response?.get(0)?.driver?.rating_count.toString()
            } else {
                ivRating.visibility = View.INVISIBLE
                tvRating.visibility = View.INVISIBLE
            }
            order = response[0]
            ivDriverImage.setRoundProfileUrl(response[0].driver?.profile_pic_url)
            if (order?.order_status == OrderStatus.CONFIRMED) {
                tvDriverStatus.text = getString(R.string.driver_accepted_request)
            } else if (order?.order_status == OrderStatus.ONGOING) {
                if (order?.my_turn == "1") {
                    tvDriverStatus.text = getString(R.string.driver_is_on_the_way)
                } else {
                    tvDriverStatus.text = getString(R.string.driver_completing_nearby_order)
                }
            }
        } else {
            fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = SelectCarFragment()
            (activity as HomeActivity).serviceRequestModel = ServiceRequestModel()
            (activity as HomeActivity).selectCarFragment = fragment
            fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
            Toast.makeText(activity, getString(R.string.order_cancelled), Toast.LENGTH_LONG).show()
        }
    }

    override fun showLoader(isLoading: Boolean) {

    }

    override fun apiFailure() {
        rootView.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        if (code == StatusCode.UNAUTHORIZED) {
            AppUtils.logout(activity as Activity)
        } else {
            rootView.showSnack(error.toString())
        }
    }

    override fun onCancelApiSuccess() {
        openServiceFragment()
    }

    override fun polyLine(jsonRootObject: JSONObject) {
        line?.remove()
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        var routes: JSONObject?
        routes = routeArray.getJSONObject(0)
        val overviewPolylines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolylines.getString("points")
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
            .addAll(list)
            .width(8f)
            .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) activity?.let {
                ContextCompat.getColor(
                    it,
                    R.color.text_dark
                )
            }
                ?: 0 else activity?.let { ContextCompat.getColor(it, R.color.white) } ?: 0)
            .geodesic(true))
        *//* To calculate the estimated distance*//*
        *//*val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String*//*
        val estimatedTime =
            (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
        tvTime.text = estimatedTime
    }*/

    private fun drawPolylineFromDriver(model: PolylineModel?) {
        line?.remove()
        val encodedString = model?.points ?: ""
        pathArray.add(encodedString)
        list = decodePoly(encodedString)
        val listSize = list.size
        sourceLatLong?.let { list.add(0, it) }
        destLong?.let { list.add(listSize + 1, it) }
        line = mMap?.addPolyline(PolylineOptions()
                .addAll(list)
                .width(8f)
                .color(if (mapType == GoogleMap.MAP_TYPE_NORMAL) activity?.let {
                    ContextCompat.getColor(
                            it,
                            R.color.colorAccent
                    )
                }
                        ?: 0 else activity?.let { ContextCompat.getColor(it, R.color.white) }
                        ?: 0)
                .geodesic(true))
//            val builder = LatLngBounds.Builder()
//            val arr = ArrayList<Marker?>()
//            arr.add(carMarker)
//            arr.add(destMarker)
//            for (marker in arr) {
//                builder.include(marker?.position)
//            }
//            val bounds = builder.build()
//            val cu = CameraUpdateFactory.newLatLngBounds(bounds,
//                    activity?.let { Utils.getScreenWidth(it) } ?: 0,
//                    activity?.let { Utils.getScreenWidth(it) }?.minus(Utils.dpToPx(24).toInt())
//                            ?: 0,
//                    Utils.dpToPx(56).toInt())
//            googleMapHome?.animateCamera(cu)

//        val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
        tvTime.text = model?.timeText

    }

    private fun reFocusMapCamera() {
        val builder = LatLngBounds.Builder()
        val arr = ArrayList<Marker?>()
        arr.add(carMarker)
        arr.add(destMarker)
        /*for (marker in arr) {
            builder.include(marker?.position)
        }
        val bounds = builder.build()
        val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                activity?.let { getScreenWidth(it) } ?: 0,
                activity?.let { getScreenWidth(it) }?.minus(dpToPx(24).toInt())
                        ?: 0, dpToPx(56).toInt()
        )
        mMap?.animateCamera(cu)*/
    }

    private fun decodePoly(encoded: String): ArrayList<LatLng> {

        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(
                    lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5
            )
            poly.add(p)
        }
        return poly
    }

    var carMarker: Marker? = null
    var destMarker: Marker? = null
    private fun showMarker(sourceLatLong: LatLng?, destLong: LatLng?) {
        /*add marker for both source and destination*/
        carMarker = mMap?.addMarker(this.sourceLatLong?.let { MarkerOptions().position(it) })
        when (order?.category_id) {
            CategoryId.GAS -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
            CategoryId.MINERAL_WATER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
            CategoryId.WATER_TANKER -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
            CategoryId.FREIGHT -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
            CategoryId.TOW -> carMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
        }
        carMarker?.isFlat = true
        carMarker?.setAnchor(0.5f, 0.5f)
        destMarker = mMap?.addMarker(this.destLong?.let { MarkerOptions().position(it) })
        if (isHeavyLoadsVehicle && order?.order_status == OrderStatus.CONFIRMED) {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
        } else {
            destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.notification)))
        }

        this.sourceLatLong?.latitude?.let {
            this.sourceLatLong?.longitude?.let { it1 ->
                this.destLong?.latitude?.let { it2 ->
                    this.destLong?.longitude?.let { it3 ->
                        /* presenter.drawPolyLine(
                             sourceLat = it, sourceLong = it1,
                             destLat = it2, destLong = it3, language = Locale.US.language
                         )*/
                    }
                }
            }
        }
    }

    fun animateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)
            val startRotation = carMarker?.rotation
            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorMove?.cancel()
            valueAnimatorMove = ValueAnimator.ofFloat(0f, 1f)
            valueAnimatorMove?.setDuration(10000) // duration 1 second
            valueAnimatorMove?.setInterpolator(LinearInterpolator())
            valueAnimatorMove?.addUpdateListener(object : ValueAnimator.AnimatorUpdateListener {
                override fun onAnimationUpdate(animation: ValueAnimator) {
                    try {
                        val v = animation.getAnimatedFraction()
                        val newPosition = latLngInterpolator.interpolate(
                                v, startPosition
                                ?: LatLng(0.0, 0.0), endPosition
                        )
                        carMarker?.setPosition(newPosition)
//                        carMarker?.setRotation(computeRotation(v, startRotation?:0f, currentBearing))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })
            valueAnimatorMove?.start()
            rotateMarker(latLng, bearing)
        }
    }

    var valueAnimatorRotate: ValueAnimator? = null
    private fun rotateMarker(latLng: LatLng?, bearing: Float?) {
        if (carMarker != null) {
            val startPosition = carMarker?.position
            val endPosition = LatLng(latLng?.latitude ?: 0.0, latLng?.longitude ?: 0.0)

            val latLngInterpolator = LatLngInterpolator.LinearFixed()
            valueAnimatorRotate?.cancel()
            val startRotation = carMarker?.rotation
            valueAnimatorRotate = ValueAnimator.ofFloat(0f, 1f)
//            valueAnimatorRotate?.startDelay = 200
            valueAnimatorRotate?.duration = 5000 // duration 1 second
            valueAnimatorRotate?.interpolator = LinearInterpolator()
            valueAnimatorRotate?.addUpdateListener { animation ->
                try {
                    val v = animation.animatedFraction
                    val newPosition = latLngInterpolator.interpolate(
                            v, startPosition
                            ?: LatLng(0.0, 0.0), endPosition
                    )
                    //                        carMarker?.setPosition(newPosition)
                    carMarker?.rotation = computeRotation(
                            v, startRotation ?: 0f, bearing
                            ?: 0f
                    )
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            valueAnimatorRotate?.start()

        }
    }

    private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
        val normalizeEnd = end - start // rotate start to 0
        val normalizedEndAbs = (normalizeEnd + 360) % 360

        val direction = (if (normalizedEndAbs > 180) -1 else 1).toFloat() // -1 = anticlockwise, 1 = clockwise
        val rotation: Float
        if (direction > 0) {
            rotation = normalizedEndAbs
        } else {
            rotation = normalizedEndAbs - 360
        }

        val result = fraction * rotation + start
        return (result + 360) % 360
    }

    private interface LatLngInterpolator {
        fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng
        class LinearFixed : LatLngInterpolator {
            public override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
                val lat = (b.latitude - a.latitude) * fraction + a.latitude
                var lngDelta = b.longitude - a.longitude
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360
                }
                val lng = lngDelta * fraction + a.longitude
                return LatLng(lat, lng)
            }
        }
    }

    private fun animateMarker(marker: Marker, toPosition: LatLng, hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap?.projection
        val startPoint = proj?.toScreenLocation(marker.position)
        val startLatLng = proj?.fromScreenLocation(startPoint)
        val duration: Long = 2000
        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * (startLatLng?.longitude ?: 0.0)
                val lat = t * toPosition.latitude + (1 - t) * (startLatLng?.latitude ?: 0.0)
                marker.position = LatLng(lat, lng)
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    marker.isVisible = !hideMarker
                }
            }
        })
    }

    private var startRotation = 0f

    private fun rotateMarker(marker: Marker, toRotation: Float, st: Float) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val duration = 1555L
        val startRotation = marker.rotation
        val interpolator = LinearInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val rot = t * toRotation + (1 - t) * startRotation
                marker.rotation = if (-rot > 180) rot / 2 else rot
                this@DeliveryStartsFragment.startRotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

    private var dialog: Dialog? = null
    private fun showCancellationDialog() {
        dialog = activity?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setCancelable(true)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setContentView(R.layout.dialog_cancel_reason)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        val etMessage = dialog?.findViewById(R.id.etMessage) as EditText
        val tvSubmit = dialog?.findViewById(R.id.tvSubmit) as TextView
        tvSubmit.setOnClickListener {
            if (etMessage.text.toString().trim().isNotEmpty()) {
                val map = HashMap<String, String>()
                map["order_id"] = order?.order_id.toString()
                map["cancel_reason"] = etMessage.text.toString().trim()
                if (isConnectedToInternet(activity, true)) {
                    //presenter.requestCancelApiCall(map)
                    dialog?.dismiss()
                }
            } else {
                activity?.let {
                    Toast.makeText(
                            it,
                            getString(R.string.cancellation_reason_validation_text),
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        dialog?.show()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private fun restartCheckIdleTimer(checkIdleTimeInterval: Long) {
        checkIdleTimer.cancel()
        checkIdleTimer = Timer()
        checkIdleTimer.schedule(object : TimerTask() {
            override fun run() {
                if (isVisible) {
                    if (isConnectedToInternet(activity, false)) {
                        checkOrderStatus()
                    }
                    restartCheckIdleTimer(20000L)
                }
            }
        }, checkIdleTimeInterval)
    }

    private fun checkOrderStatus() {
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", order?.order_token)
        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack {
            val response = Gson().fromJson<ApiResponse<Wash>>(it[0].toString(),
                    object : TypeToken<ApiResponse<Wash>>() {}.type)
            /* if (response?.statusCode == SUCCESS_CODE) {
                 val orderModel = response.result
                 val orderJson = JSONObject(it[0].toString()).getString("result")
                 activity?.runOnUiThread {
                     when (orderModel?.order_status) {

                         OrderStatus.SERVICE_COMPLETE -> {
                             openInvoiceFragment(orderJson)
                         }

                         OrderStatus.DRIVER_CANCELLED -> {
                             openServiceFragment()
                             Toast.makeText(
                                 activity,
                                 getString(R.string.ongoing_request_cancelled_by_driver),
                                 Toast.LENGTH_LONG
                             ).show()
                         }

                         OrderStatus.CUSTOMER_CANCEL -> {
                             openServiceFragment()
                             Toast.makeText(activity, getString(R.string.request_was_cancelled), Toast.LENGTH_LONG)
                                 .show()
                         }

                         else -> {
                             Logger.e("CUSTOMER_SINGLE_ORDER", "This status not handeled :" + orderModel?.order_status)

                         }
                     }
                 }
             } else if (response.statusCode == StatusCode.UNAUTHORIZED) {
                 AppUtils.logout(activity)
             }*/
        })
    }

    private fun openInvoiceFragment(orderJson: String?) {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openServiceFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = SelectCarFragment()
        (activity as? BookingActivity)?.selectCarFragment = fragment
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

}
