package com.codebrew.mrsteam2.ui.customer.booking.processingrequest


import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.transition.Explode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.AcceptRequestViewModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.deliverystarts.DeliveryStartsFragment
import com.codebrew.mrsteam2.ui.customer.booking.invoice.InvoiceFragment
import com.codebrew.mrsteam2.ui.customer.booking.selectcar.SelectCarFragment
import com.codebrew.mrsteam2.utils.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.Ack
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.fragment_processing_request.*
import org.json.JSONObject


class ProcessingRequestFragment : Fragment() {

    private var timeLimit = 170000L

    private lateinit var serviceRequest: ServiceRequestModel

    private var orderDetail: Wash? = null

    private var progressDialog: ProgressDialog? = null

    private var isPaused = false

    private var isTimerFinished = false

    private var handlerCheckOrder = Handler()

    private var isServicesAccepted = false

    private var timer: CountDownTimer? = null

    private lateinit var viewModelAcceptReject: AcceptRequestViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_processing_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setData()
        initialise()
        setListeners()
        liveData()

        Log.e("","")
    }

    private fun initialise() {
        progressDialog = ProgressDialog(activity)
        (activity as BookingActivity).getMapAsync()
        progressBar.max = timeLimit.toInt()
        if (arguments?.getBoolean("changeTimeOut", false) == true) {
            progressBar?.isIndeterminate = true
            isTimerFinished = true
//            checkOrderStatus(0)
        } else {
            startTimer()
        }
        progressBar.visibility = View.VISIBLE

        viewModelAcceptReject = ViewModelProviders.of(this)[AcceptRequestViewModel::class.java]
    }

    override fun onResume() {
        super.onResume()
        AppSocket.get().on(Socket.EVENT_CONNECT, onConnect)
        isPaused = false
        checkOrderStatus(0)
//        if (!isTimerFinished) {
        startEventSocket()
        context?.let {
            LocalBroadcastManager
                    .getInstance(it)
                    .registerReceiver(notificationBroadcast, IntentFilter(Broadcast.NOTIFICATION))
        }
//        }
    }

    override fun onPause() {
        super.onPause()
        AppSocket.get().off(Socket.EVENT_CONNECT, onConnect)
        isPaused = true
        stopSocketEvent()
    }

    private val onConnect = Emitter.Listener {
        //        if (isTimerFinished) {
        checkOrderStatus(0)
//        }
    }

    private val notificationBroadcast = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.getIntExtra("order_id", 0) == orderDetail?.order_id?.toInt() && !isServicesAccepted) {
                checkOrderStatus(0)
            }
        }
    }

    private fun startEventSocket() {
        AppSocket.get().on(Events.ORDER_EVENT) {
            if (JSONObject(it[0].toString()).has("type")) {
                when (JSONObject(it[0].toString()).getString("type")) {
                    OrderEventType.SERVICE_ACCEPT -> {
                        isServicesAccepted = true
                        try {
                            val orderJson = JSONObject(it[0].toString()).getJSONArray("order").get(0).toString()
                            val orderModel = Gson().fromJson(orderJson, Wash::class.java)
                            if (orderModel.order_id == orderDetail?.order_id) {
                                timer?.cancel()
                                Logger.e(Events.ORDER_EVENT, it[0].toString())
                                Handler(Looper.getMainLooper()).post {
                                    if (orderModel.future == ServiceRequestModel.SCHEDULE) {
                                        showScheduledConfirmationDialog()
                                    } else {
                                        openBookingConfirmedFragment(orderJson)
                                    }
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    OrderEventType.SERVICE_REJECT -> {
                        isServicesAccepted = true
                        Handler(Looper.getMainLooper()).post {
                            if (arguments?.getBoolean("changeTimeOut", false) == false) {
                                openBookingFragment()
                            } else {
                                fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                val fragment = SelectCarFragment()
                                (activity as BookingActivity).serviceRequestModel = ServiceRequestModel()
                                (activity as BookingActivity).selectCarFragment = fragment
                                fragmentManager?.beginTransaction()?.add(R.id.container, fragment,
                                        SelectCarFragment::class.java.name)?.addToBackStack("backstack")?.commit()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun stopSocketEvent() {
        AppSocket.get().off(Events.ORDER_EVENT)
    }

    private fun setData() {
        orderDetail = Gson().fromJson(arguments?.getString(ORDER_PROGRESS), Wash::class.java)
        serviceRequest = (activity as BookingActivity).serviceRequestModel
        tvCarServiceV.text = orderDetail?.my_car?.model
        tvLocation.text = orderDetail?.address
        tvCarServicePriceV.text = orderDetail?.my_car?.plate_number
        timeLimit = ((orderDetail?.time_left_in_mins ?: 170000 - 10) * 1000).toLong()

        timer = object : CountDownTimer(timeLimit, 16) {
            override fun onFinish() {
                openBookingFragment()
                //  onAcceptRequest.onOrderTimeoutOrError()
            }

            override fun onTick(millisUntilFinished: Long) {
                progressBar?.progress = timeLimit.toInt() - millisUntilFinished.toInt()
            }
        }
    }

    private fun setListeners() {
        tvCancel.setOnClickListener {
            rejectApiCall(false)
        }
    }

    private fun rejectApiCall(confirm: Boolean) {
        if (isConnectedToInternet(activity, true)) {
            val hashMap = HashMap<String, Any>()

            hashMap["status"] = ServiceStatus.CANCLE_BY_USER
            if (confirm)
                hashMap["is_confirmed"] = true

            val id = if (orderDetail?.wash_request_id == null)
                orderDetail?.id ?: 0
            else
                orderDetail?.wash_request_id ?: 0

            viewModelAcceptReject.acceptRejectRequest(id, hashMap)
        }
    }

    private fun liveData() {
        viewModelAcceptReject.acceptRejectRequest.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog?.setLoading(false)

                    if (it.data?.need_confirmation == true) {
                        confirmRequestCancel()
                    } else {
                        Toast.makeText(activity, "Request cancelled", Toast.LENGTH_LONG).show()
                        activity?.finish()
                    }
                }

                Status.ERROR -> {
                    progressDialog?.setLoading(false)
                    ApisRespHandler.handleError(it.error, activity)
                }

                Status.LOADING -> {
                    progressDialog?.setLoading(true)
                }
            }
        })
    }

    private fun confirmRequestCancel() {
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.alert,
                R.string.logout_dialog_message, R.string.yes,
                R.string.no, true,
                object : AlertDialogUtil.OnOkCancelDialogListener {
                    override fun onOkButtonClicked() {
                        rejectApiCall(true)
                    }

                    override fun onCancelButtonClicked() {
                    }
                }).show()
    }


    /* private val timer = object : CountDownTimer(timeLimit, 16) {
         override fun onFinish() {
             isTimerFinished = true
             if (!isPaused) {
                 //checkOrderStatus(0)
                 openBookingFragment()
             }
         }

         override fun onTick(millisUntilFinished: Long) {
             progressBar.progress = timeLimit.toInt() - millisUntilFinished.toInt()
         }

     }*/

    private fun startTimer() {
        timer?.start()
    }

    private fun checkOrderStatus(delay: Long) {
        try {
            handlerCheckOrder.removeCallbacks(runnable)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        handlerCheckOrder.postDelayed(runnable, delay)
    }

    private val runnable = Runnable {
        if (isTimerFinished) {
            checkOrderStatus(5000)
        }
        val request = JSONObject()
        request.put("type", OrderEventType.CUSTOMER_SINGLE_ORDER)
        request.put("access_token", ACCESS_TOKEN)
        request.put("order_token", orderDetail?.order_token)

        AppSocket.get().emit(Events.COMMON_EVENT, request, Ack {
            val response = Gson().fromJson<ApiResponse<Wash>>(it[0].toString(),
                    object : TypeToken<ApiResponse<Wash>>() {}.type)
            if (response.status == SUCCESS_CODE) {
                val orderModel = response.data
                val orderJson = JSONObject(it[0]?.toString()).getString("result")
                Handler(Looper.getMainLooper()).post {
                    when (orderModel?.order_status) {

                        OrderStatus.ONGOING, OrderStatus.CONFIRMED -> {
                            openBookingConfirmedFragment(orderJson)
                        }

                        OrderStatus.SERVICE_COMPLETE -> {
                            openInvoiceFragment(orderJson)
                        }

                        OrderStatus.SEARCHING -> {
                            /*Keep searching */
                        }

                        OrderStatus.SCHEDULED -> {
                            showScheduledConfirmationDialog()
                        }
                        else -> {
                            if (arguments?.getBoolean("changeTimeOut", false) == false) {
                                openBookingFragment()
                            } else {
                                fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                val fragment = SelectCarFragment()
                                (activity as BookingActivity).serviceRequestModel = ServiceRequestModel()
                                (activity as BookingActivity).selectCarFragment = fragment
                                fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
                            }
                        }
                    }
                }
            } else if (response.status == 401) {
                logoutUser(activity as Activity)
            }
        })
    }

    private fun openBookingConfirmedFragment(orderJson: String?) {
        val fragment = DeliveryStartsFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        try {
            fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commitAllowingStateLoss()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun openInvoiceFragment(orderJson: String?) {
        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putString("order", orderJson)
        fragment.arguments = bundle
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

    private fun openBookingFragment() {
        if (activity != null) {
            Toast.makeText(activity, R.string.currently_no_driver_available, Toast.LENGTH_SHORT).show()
        }
        if (fragmentManager?.backStackEntryCount ?: 0 > 1)
            fragmentManager?.popBackStackImmediate()
        else
            activity?.finish()
    }

    /* override fun onApiSuccess() {
         timer.cancel()
         Toast.makeText(activity, R.string.request_cancelled_successfully, Toast.LENGTH_SHORT).show()
         fragmentManager?.popBackStackImmediate()
     }

     override fun showLoader(isLoading: Boolean) {
         progressDialog?.show(isLoading)
     }

     override fun apiFailure() {
         rootView.showSWWerror()
     }

     override fun handleApiError(code: Int?, error: String?) {
         if (code == StatusCode.UNAUTHORIZED) {
             AppUtils.logout(activity as Activity)
         } else {
             rootView.showSnack(error.toString())
         }
     }

     override fun handleOrderError(order: Order?) {
         when (order?.order_status) {
             OrderStatus.CUSTOMER_CANCEL, OrderStatus.DRIVER_CANCELLED, OrderStatus.DRIVER_SCHEDULED_CANCELLED, OrderStatus.SYS_SCHEDULED_CANCELLED, OrderStatus.SERVICE_TIMEOUT -> {
                 timer.cancel()
                 Toast.makeText(activity, R.string.request_cancelled_successfully, Toast.LENGTH_SHORT).show()
                 fragmentManager?.popBackStackImmediate()
             }

             OrderStatus.SERVICE_COMPLETE->{
                 openInvoiceFragment(Gson().toJson(orderDetail))
             }
         }
     }*/

    override fun onDestroyView() {
        super.onDestroyView()
        timer?.cancel()
        progressDialog?.setLoading(false)
        handlerCheckOrder.removeCallbacks(runnable)
        //presenter.detachView()

    }

    private fun showScheduledConfirmationDialog() {
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.congratulations,
                R.string.service_booked_successfully, R.string.ok, 0,
                false, object : AlertDialogUtil.OnOkCancelDialogListener {
            override fun onOkButtonClicked() {
                openHomeFragment()
            }

            override fun onCancelButtonClicked() {
                openHomeFragment()
            }
        }).show()
    }

    private fun openHomeFragment() {
        fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val fragment = SelectCarFragment()
        (activity as BookingActivity).serviceRequestModel = ServiceRequestModel()
        (activity as BookingActivity).selectCarFragment = fragment
        fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
    }

}
