package com.codebrew.mrsteam2.ui.common.loginsignup

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.common.loginsignup.login.LoginFragment
import com.codebrew.mrsteam2.utils.MrSteamApp
import kotlinx.android.synthetic.main.activity_signup_login.*

class LoginSignupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup_login)

        supportFragmentManager.beginTransaction()
                .add(R.id.layoutFragmentData,
                        LoginFragment(), LoginFragment::class.java.name).commit()

        ivBack.setOnClickListener {
            onBackPressed()
        }
    }
}