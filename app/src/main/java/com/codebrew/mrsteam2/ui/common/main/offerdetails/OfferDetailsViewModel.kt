package com.codebrew.mrsteam2.ui.common.main.offerdetails

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.AddOfferRequest
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OfferDetailsViewModel : ViewModel() {
    val addOfferRes by lazy { SingleLiveEvent<Resource<Offers>>() }

    fun addOffers(addReq: AddOfferRequest) {
        addOfferRes.value = Resource.loading()


        RetrofitClient.getApi().addOffers(addReq)
                .enqueue(object : Callback<ApiResponse<Offers>> {
                    override fun onFailure(call: Call<ApiResponse<Offers>>, throwable: Throwable) {
                        addOfferRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<Offers>>,
                            response: Response<ApiResponse<Offers>>
                    ) {
                        if (response.isSuccessful) {
                            addOfferRes.value = Resource.success(response.body()?.data)
                        } else {
                            addOfferRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}