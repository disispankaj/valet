package com.codebrew.mrsteam2.ui.common.loginsignup


import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.SignUp
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SocialViewModel : ViewModel() {
    val socialLogin by lazy { SingleLiveEvent<Resource<SignUp>>() }

    fun socialLogin(hashMap: HashMap<String, Any>) {

        socialLogin.value = Resource.loading()

        RetrofitClient.getApi()
                .socialLogin(hashMap)
                .enqueue(object : Callback<ApiResponse<SignUp>> {
                    override fun onFailure(call: Call<ApiResponse<SignUp>>, throwable: Throwable) {
                        socialLogin.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<SignUp>>, response: Response<ApiResponse<SignUp>>) {
                        if (response.isSuccessful) {
                            socialLogin.value = Resource.success(response.body()?.data)
                        } else {
                            socialLogin.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }


}