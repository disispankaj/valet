package com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar

import permissions.dispatcher.PermissionRequest
import java.lang.ref.WeakReference


private val REQUEST_GETSTORAGE: Int = 1

private val PERMISSION_GETSTORAGE: Array<String> = arrayOf("android.permission.WRITE_EXTERNAL_STORAGE")

fun AddCarActivity.getStorageWithPermissionCheck() {
    if (permissions.dispatcher.PermissionUtils.hasSelfPermissions(this, *PERMISSION_GETSTORAGE)) {
        getStorage()
    } else {
        if (permissions.dispatcher.PermissionUtils.shouldShowRequestPermissionRationale(this, *PERMISSION_GETSTORAGE)) {
            showLocationRationale(EnterOrderDetailsFragmentGetStoragePermissionRequest(this))
        } else {
            this.requestPermissions(PERMISSION_GETSTORAGE, REQUEST_GETSTORAGE)
        }
    }
}

fun AddCarActivity.onRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
    when (requestCode) {
        REQUEST_GETSTORAGE -> {
            if (permissions.dispatcher.PermissionUtils.verifyPermissions(*grantResults)) {
                getStorage()
            } else {
                if (!permissions.dispatcher.PermissionUtils.shouldShowRequestPermissionRationale(this, *PERMISSION_GETSTORAGE)) {
                    onNeverAskAgainRationale()
                }
            }
        }
    }
}

private class EnterOrderDetailsFragmentGetStoragePermissionRequest(target: AddCarActivity) :
        PermissionRequest {
    private val weakTarget: WeakReference<AddCarActivity> = WeakReference(target)

    override fun proceed() {
        val target = weakTarget.get() ?: return
        target.requestPermissions(PERMISSION_GETSTORAGE, REQUEST_GETSTORAGE)
    }

    override fun cancel() {
    }
}
