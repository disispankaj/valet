package com.codebrew.mrsteam2.ui.customer.booking.bookmakepayment


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.processingrequest.ProcessingRequestFragment
import com.codebrew.mrsteam2.ui.customer.booking.promocode.Promo
import com.codebrew.mrsteam2.utils.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_payment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MakePaymentFragment : Fragment() {

    private lateinit var serviceRequest: ServiceRequestModel

    private lateinit var viewModel: RequestViewModel

    private lateinit var progressDialog: ProgressDialog

    var netPrice = 0

    var finalPrice = 0

    var promoCodeId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(activity!!)
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialise()
        setListeners()
        setData()
        liveData()
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[RequestViewModel::class.java]
        progressDialog = ProgressDialog(activity)
    }


    private fun setData() {
        val price = serviceRequest.checkOffer

        tvCarWashPrice.text = "$${price?.price ?: 0}"
        tvGrandTotalV.text = "$${price?.price ?: 0}"

        netPrice = price?.price ?: 0

        btnPay.text = when (serviceRequest.paymentType) {
            PaymentsType.OFFER, PaymentsType.CASH -> getString(R.string.book_now)
            PaymentsType.CREDIT -> "${getString(R.string.pay)} $${price?.price ?: 0}"
            else -> getString(R.string.book_now)
        }

    }


    private fun setListeners() {

        tbToolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        btnPay.setOnClickListener {
            if (isConnectedToInternet(activity, true)) {

                val hashMap = HashMap<String, Any>()
                hashMap["my_car_id"] = serviceRequest.carId ?: 0
                hashMap["sub_service_id"] = serviceRequest.subService ?: 0
                hashMap["lat"] = serviceRequest.dropoff_latitude ?: 0
                hashMap["long"] = serviceRequest.dropoff_longitude ?: 0
                hashMap["address"] = serviceRequest.dropoff_address ?: ""
                hashMap["booking_type"] = serviceRequest.future ?: 0
                hashMap["price"] = netPrice
                hashMap["final_price"] = finalPrice
                hashMap["final_price"] = promoCodeId
                hashMap["token"] = serviceRequest.tokenConnekta?:""
                if (serviceRequest.future == ServiceRequestModel.SCHEDULE)
                    hashMap["required_at"] = serviceRequest.order_timings ?: ""
                hashMap["payment_type"] = serviceRequest.paymentType ?: 0
//2019-05-25 10:03:05
                viewModel.requests(hashMap)
            }
        }

        tvApplyPromo.setOnClickListener {
            showDialog()
        }

    }


    private fun liveData() {
        viewModel.request.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    val fragment = ProcessingRequestFragment()
                    val bundle = Bundle()
                    bundle.putString(ORDER_PROGRESS, Gson().toJson(resources.data))
                    fragment.arguments = bundle

                    fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)
                        ?.addToBackStack("backstack")?.commit()

                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }


    private fun showDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.cutsom_layout)
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        val etEnterCoupon = dialog.findViewById(R.id.etEnterCoupon) as EditText
        val noBtn = dialog.findViewById(R.id.tvApply) as TextView
        val btnCancel = dialog.findViewById(R.id.btnCancel) as Button
        btnCancel.setOnClickListener { dialog.dismiss() }

        noBtn.setOnClickListener {
            if (etEnterCoupon.text.toString().length > 0) {
                validatePromoCodes(netPrice.toString(), etEnterCoupon.text.toString(), dialog)
            } else
                Toast.makeText(activity, getString(R.string.add_coupan_first), Toast.LENGTH_SHORT).show()


        }
        dialog.show()
    }

    fun validatePromoCodes(netPrice: String, promoCode: String, dialog: Dialog) {
        val request by lazy { SingleLiveEvent<Resource<Wash>>() }
        request.value = Resource.loading()
        RetrofitClient.getApi().validatePromoCode(netPrice, promoCode)
            .enqueue(object : Callback<ApiResponse<Promo>> {
                override fun onFailure(call: Call<ApiResponse<Promo>>, throwable: Throwable) {
                    request.value = Resource.error(ApiUtils.failure(throwable))
                }

                override fun onResponse(
                    call: Call<ApiResponse<Promo>>,
                    response: Response<ApiResponse<Promo>>
                ) {
                    if (response.isSuccessful && response.code() == 200) {
                        Toast.makeText(activity, response.body()?.message, Toast.LENGTH_SHORT).show()
                        promoCodeId = response.body()?.data?.id!!
                        dialog.dismiss()
                        calculatePrice(response.body()?.data?.promo_type!!, response.body()?.data?.value!!)

                    } else {
                        request.value = Resource.error(
                            ApiUtils.getError(
                                response.code(),
                                response.errorBody()?.string()
                            )
                        )
                    }
                }

            })
    }

    fun calculatePrice(type: String, value: Int) {
        linear_promo.visibility = View.VISIBLE
        var price = value
        tvApplyPromo.text = getString(R.string.coupan_applied)+" ($$value ${getString(R.string.off)})"
        if (type.equals("percentage")) {
            price = netPrice * value / 100
            tvApplyPromo.text = getString(R.string.coupan_applied)+" ($value"+"% ${getString(R.string.off)})"
        }

        finalPrice = netPrice - price

        tvGrandTotalV_promo.text = "$" + finalPrice

        btnPay.text = when (serviceRequest.paymentType) {
            PaymentsType.OFFER, PaymentsType.CASH -> getString(R.string.book_now)
            PaymentsType.CREDIT -> "${getString(R.string.pay)} $${finalPrice}"
            else -> getString(R.string.book_now)
        }
        tvApplyPromo.isEnabled = false
    }

}
