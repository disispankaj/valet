package com.codebrew.mrsteam2.ui.common.main.common

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.ui.common.main.offerdetails.OfferDetailsActivity
import com.codebrew.mrsteam2.utils.OFFERS_DATA
import com.codebrew.mrsteam2.utils.OFFER_STATUS
import kotlinx.android.synthetic.main.item_rv_home_offers.view.*

class OffersAdapter(private val allOffers: Boolean) : RecyclerView.Adapter<OffersAdapter.ViewHolder>() {
    private val l1 = ArrayList<Offers>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_my_offers, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return l1.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(l1[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        init {

            itemView.layoutBack.setOnClickListener {
                val res = l1[adapterPosition]
                val i1 = Intent(itemView.context, OfferDetailsActivity::class.java)
                i1.putExtra(OFFERS_DATA, res)
                i1.putExtra(OFFER_STATUS, allOffers)
                item.context.startActivity(i1)
            }

        }

        fun bind(data: Offers) = with(itemView) {
            when (adapterPosition % 4) {
                0 -> layoutBack.setBackgroundResource(R.drawable.offer_basic_background)
                1 -> layoutBack.setBackgroundResource(R.drawable.offer_economic_background)
                2 -> layoutBack.setBackgroundResource(R.drawable.offer_vip_background)
                else -> layoutBack.setBackgroundResource(R.drawable.offer_premium_background)
            }

            tvCategory.text = data.offer?.dual_name
            tvPrice.text = "$ ${data.offer?.price}"
            tvOfferValid.text = "Offer valid for next ${data.offer?.valid_days} days"

            if (allOffers) {

                tvCategory.text = data.dual_name
                tvPrice.text = "$ ${data.price}"
                tvOfferValid.text = "Offer valid for next ${data.valid_days} days"

                val featuresAdapter = OfferFeaturesAdapter(allOffers)
                rvFeatures.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rvFeatures.adapter = featuresAdapter
                featuresAdapter.setData(data.active_sub_services ?: ArrayList())

            } else {

                tvCategory.text = data.offer?.dual_name
                tvPrice.text = "$ ${data.offer?.price}"
                tvOfferValid.text = "Offer valid for next ${data.offer?.valid_days} days"

                val featuresAdapter = OfferFeaturesAdapter(allOffers)
                rvFeatures.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                rvFeatures.adapter = featuresAdapter
                featuresAdapter.setData(data.active_details ?: ArrayList())
            }
        }
    }

    fun setData(list: ArrayList<Offers>) {
        l1.clear()
        l1.addAll(list)
        notifyDataSetChanged()
    }


}

