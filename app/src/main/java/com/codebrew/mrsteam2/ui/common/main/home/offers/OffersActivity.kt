package com.codebrew.mrsteam2.ui.common.main.home.offers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.common.OffersAdapter
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_home_offers.*

class OffersActivity : AppCompatActivity() {
    private lateinit var viewModel: OfferViewModel
    private lateinit var progressDialog: ProgressDialog
    private val offersAdapter = OffersAdapter(true)

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_offers)

        adapterSetting()
        initialise()
        listener()
        offerApiHit()
        liveData()
    }

    private fun listener() {
        tbrHomeOffers.setNavigationOnClickListener {
            onBackPressed()
        }

        swipeRefreshLayout.setOnRefreshListener {
            offerApiHit()
        }
    }

    private fun adapterSetting() {
        rvOffers.layoutManager = LinearLayoutManager(this@OffersActivity, RecyclerView.VERTICAL, false)
        rvOffers.adapter = offersAdapter
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[OfferViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }


    private fun offerApiHit() {
        if (isConnectedToInternet(this, true))
            viewModel.allOffers()
    }

    private fun liveData() {
        viewModel.allOfferRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    offersAdapter.setData(resources.data ?: ArrayList())
                }

                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })


    }

}