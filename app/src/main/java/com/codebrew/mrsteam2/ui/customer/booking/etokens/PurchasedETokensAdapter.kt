package com.codebrew.mrsteam2.ui.customer.booking.etokens

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.buraq24.customer.webservices.models.etokens.History
import com.codebrew.mrsteam2.R
import kotlinx.android.synthetic.main.item_token_purchased.view.*

class PurchasedETokensAdapter(private val context: Context?, private val purchasedList: ArrayList<History>?) : RecyclerView.Adapter<PurchasedETokensAdapter.ViewHolder>() {

    var prevSelectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_token_purchased, parent, false))
    }

    override fun getItemCount(): Int {
        return purchasedList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(purchasedList?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView?.setOnClickListener {
                purchasedList?.get(prevSelectedPosition)?.isSelected = false
                purchasedList?.get(adapterPosition)?.isSelected = true
                prevSelectedPosition = adapterPosition
                notifyDataSetChanged()
            }
        }

        fun onBind(tokens: History?) = with(itemView) {
            val nextLine = "\n"
            context?.let { Glide.with(it).load(tokens?.brand?.image_url).into(imageView) }
            textView.text = """${tokens?.quantity_left.toString()} """ + if (tokens?.quantity_left == 1) {
                """${context.getString(R.string.e_token)} $nextLine${context.getString(R.string.left)}"""
            } else {
                """${context.getString(R.string.e_tokens)} $nextLine${context.getString(R.string.left)}"""
            }
            rootView.isSelected = purchasedList?.get(adapterPosition)?.isSelected ?: false
        }
    }

}