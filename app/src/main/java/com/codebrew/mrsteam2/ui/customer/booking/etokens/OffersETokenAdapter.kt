package com.codebrew.mrsteam2.ui.customer.booking.etokens

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.etokens.Brand
import kotlinx.android.synthetic.main.item_tokens_offers.view.*


class OffersETokenAdapter(private val context: Context?, private val offersList: ArrayList<Brand>?) : RecyclerView.Adapter<OffersETokenAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tokens_offers, parent, false))
    }

    override fun getItemCount(): Int {
        return offersList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(offersList?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(brand: Brand?) = with(itemView) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(6))
            context?.let { Glide.with(it).load(brand?.image_url).apply(requestOptions).into(ivBrandImage) }
            tvBrandName.text = brand?.category_brand_name
            tvOffersCount.text = """${brand?.etokens_count.toString()} ${context.getString(R.string.e_tokens)} ${context.getString(R.string.available)}"""
            val tokensAdapter = OffersListAdapter(context, brand?.etokens)
            rvOffers?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvOffers.adapter = tokensAdapter
        }
    }

}