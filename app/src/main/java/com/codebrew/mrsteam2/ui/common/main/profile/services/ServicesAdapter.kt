package com.codebrew.mrsteam2.ui.common.main.profile.services

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Services
import com.codebrew.mrsteam2.ui.customer.booking.service.SubServiceAdapter1
import kotlinx.android.synthetic.main.item_profile_rv_services.view.*

class ServicesAdapter : RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {
    private val l1 = ArrayList<Services>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_profile_rv_services, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return l1.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(l1[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        init {
        }

        fun bind(data: Services?) = with(itemView) {

            tvServiceType.text = data?.dual_name
            tvServiceType.setOnClickListener {
                rellayout.setBackgroundColor(ContextCompat.getColor(context,R.color.mrSteamMain))
            }

            val subAdapter = SubServiceAdapter1(data?.active_sub_services, null, false)
            rvSubServices.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            rvSubServices.adapter = subAdapter

        }
    }

    fun setData(list: ArrayList<Services>) {
        l1.clear()
        l1.addAll(list)
        notifyDataSetChanged()
    }

}