package com.codebrew.mrsteam2.ui.common.loginsignup.verifynumber

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.VerifyNumberRequest
import com.codebrew.mrsteam2.network.responsemodels.ResendOtp
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VerifyNumberViewModel : ViewModel() {
    val verifyNumRes by lazy { SingleLiveEvent<Resource<UserData>>() }
    val resendOtpRes by lazy { SingleLiveEvent<Resource<ResendOtp>>() }

    fun verifyNumber(verifyNumReq: VerifyNumberRequest) {
        verifyNumRes.value = Resource.loading()
        RetrofitClient.getApi().verifyNumber(verifyNumReq)
                .enqueue(object : Callback<ApiResponse<UserData>> {
                    override fun onFailure(call: Call<ApiResponse<UserData>>, throwable: Throwable) {
                        verifyNumRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<UserData>>,
                            response: Response<ApiResponse<UserData>>) {
                        if (response.isSuccessful) {
                            verifyNumRes.value = Resource.success(response.body()?.data)
                        } else {
                            verifyNumRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }

    fun resendOtp() {
        resendOtpRes.value = Resource.loading()
        RetrofitClient.getApi().resendOtp("9888585806", "+91")
                .enqueue(object : Callback<ApiResponse<ResendOtp>> {
                    override fun onFailure(call: Call<ApiResponse<ResendOtp>>, throwable: Throwable) {
                        resendOtpRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<ResendOtp>>,
                            response: Response<ApiResponse<ResendOtp>>
                    ) {
                        if (response.isSuccessful) {
                            resendOtpRes.value = Resource.success(response.body()?.data)
                        } else {
                            resendOtpRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}