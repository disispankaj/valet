package com.codebrew.mrsteam2.ui.common.main.offerdetails

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.AddOfferRequest
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.profile.myoffers.MyOffersActivity
import com.codebrew.mrsteam2.ui.customer.paymentModule.CardActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_offers_details.*

class OfferDetailsActivity : AppCompatActivity() {
    private lateinit var viewModel: OfferDetailsViewModel
    private lateinit var progressDialog: ProgressDialog

    private var data: Offers? = null
    private var allOffersStatus: Boolean? = null

    private val featuresAdapter = OfferDetailsAdapter(true)
    private val vehicleAdapter = OfferDetailsAdapter(false)


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offers_details)


        data = intent.getSerializableExtra(OFFERS_DATA) as Offers
        allOffersStatus = intent.getBooleanExtra(OFFER_STATUS, false)


        if (!allOffersStatus!!) {
            myOffersdataSetup()
        } else dataSetup()


        listener()
        setAdapter()
        initialise()
        liveData()
    }


    private fun dataSetup() {
        tvOfferType.text = data?.dual_name
        tvOfferPrice.text = "$ " + data?.price.toString()
        tvDescription.text = data?.dual_description
        tvOfferValid.text = "Offer valid for next ${data?.valid_days} days only"

        when ((data?.id)?.rem(4)) {
            0 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_premium_solid))
            }
            1 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_basic_solid))
            }
            2 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_economic_solid))
            }
            3 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_premium_solid))
            }

        }
        if (data?.is_purchased!!) {
            btnPay.text = getString(R.string.str_purchased)
            btnPay.isClickable = false
            btnPay.isFocusable = false
            btnPay.isEnabled = false
        } else {
            if (allOffersStatus!!) {
                btnPay.text = "Pay  $" + data?.price.toString()
            } else {
                btnPay.text = getString(R.string.str_purchase)
                btnPay.isClickable = false
                btnPay.isFocusable = false
            }
        }

    }

    private fun myOffersdataSetup() {
        tvOfferType.text = data?.offer?.dual_name
        tvOfferPrice.text = "$ " + data?.offer?.price.toString()
        tvDescription.text = data?.offer?.dual_description
        tvOfferValid.text = "Offer valid for next ${data?.offer?.valid_days} days only"

        when ((data?.offer?.id)?.rem(4)) {
            0 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_premium_solid))
            }
            1 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_basic_solid))
            }
            2 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_economic_solid))
            }
            3 -> {
                tvOfferPrice.setTextColor(ContextCompat.getColor(this, R.color.offer_premium_solid))
            }

        }

        btnPay.visibility = View.INVISIBLE
    }

    private fun listener() {
        tbrOfferDetails.setNavigationOnClickListener {
            onBackPressed()
        }
        btnPay.setOnClickListener {
            startActivityForResult(Intent(this, CardActivity::class.java), 120)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 120 && data != null) {
            println("CONNEKTA TOKEN " + data.getStringExtra("token"))
            if (data.getStringExtra("token") != null && !data.getStringExtra("token").equals("")) {
                addOffersApi(data.getStringExtra("token"))
            } else Toast.makeText(this, getString(R.string.error_with_card_payment_conekta), Toast.LENGTH_SHORT).show()
        }
    }


    private fun setAdapter() {
        rvFeature.layoutManager = LinearLayoutManager(this@OfferDetailsActivity, RecyclerView.VERTICAL, false)
        rvFeature.adapter = featuresAdapter

        if (data?.active_sub_services == null) {
            featuresAdapter.setDataFeatures(data?.active_details ?: ArrayList())
        } else {
            featuresAdapter.setDataFeatures(data?.active_sub_services ?: ArrayList())
        }
        // rvVehicleType.layoutManager = LinearLayoutManager(this@OfferDetailsActivity, RecyclerView.HORIZONTAL, false)
        //  rvVehicleType.adapter = vehicleAdapter
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[OfferDetailsViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }

    private fun addOffersApi(token: String) {
        val addReq = AddOfferRequest(
            offer_id = data?.id,
            token = token
        )
        viewModel.addOffers(addReq)
    }

    private fun liveData() {
        viewModel.addOfferRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    longToast(getString(R.string.str_offer_activated))
                    startActivity(Intent(this, MyOffersActivity::class.java))
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })


    }
}