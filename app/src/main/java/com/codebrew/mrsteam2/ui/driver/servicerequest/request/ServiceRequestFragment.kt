package com.codebrew.mrsteam2.ui.driver.servicerequest.request


import android.app.Activity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.AcceptRequestViewModel
import com.codebrew.mrsteam2.ui.driver.servicerequest.ServiceProgressActivity
import com.codebrew.mrsteam2.ui.driver.servicerequest.requestAfterAccept.StartRequestFragment
import com.codebrew.mrsteam2.utils.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_service_request.*


class ServiceRequestFragment : Fragment() {
    private var timeLimit = 170000L
    private var timer: CountDownTimer? = null
    private lateinit var orderDetail: Wash
    private lateinit var onAcceptRequest: OnAcceptRequest
    private lateinit var progressDialog: ProgressDialog
    private var handler = Handler()
    private var requestAcceptReject = false

    private lateinit var viewModelAcceptReject: AcceptRequestViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_service_request, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as ServiceProgressActivity).getMapAsync()
        initialise()
        setData()

        liveData()

        progressBar.max = timeLimit.toInt()
        timer = object : CountDownTimer(timeLimit, 16) {
            override fun onFinish() {
                if (activity?.supportFragmentManager?.backStackEntryCount ?: 0 > 1)
                    activity?.supportFragmentManager?.beginTransaction()?.remove(this@ServiceRequestFragment)
                        ?.commitAllowingStateLoss()
                else
                    activity?.finish()
                //  onAcceptRequest.onOrderTimeoutOrError()
            }

            override fun onTick(millisUntilFinished: Long) {
                progressBar?.progress = timeLimit.toInt() - millisUntilFinished.toInt()
            }
        }
        // tvTotal.text = getFinalCharge(orderDetail, 0) + " " + getString(R.string.currency)
        progressBar.visibility = View.VISIBLE
        startTimer()
        setListeners()
    }

    private fun initialise() {
        viewModelAcceptReject = ViewModelProviders.of(this)[AcceptRequestViewModel::class.java]
        orderDetail = Gson().fromJson(arguments?.getString(ORDER_PROGRESS), Wash::class.java)
        progressDialog = ProgressDialog(activity)
    }

    private fun setData() {
        timeLimit = ((orderDetail.time_left_in_mins ?: 170000 - 10) * 1000).toLong()
        tvLocation.text = orderDetail.address
        tvCarNameV.text = orderDetail.my_car?.model
        tvCarServiceV.text = orderDetail.sub_service?.dual_title

        tvCarServicePriceV.text = orderDetail.sub_service?.price.toString()
        tvLocation.text = orderDetail.address

        tvName.text = orderDetail.user?.name
        loadImage(
            activity, ivUser, orderDetail.user?.image_url?.thumbnail
                ?: "", orderDetail.user?.image_url?.original ?: ""
        )

        tvDistanceStartRequest.text = "${orderDetail.distance} km"
        tvTimeStartRequest.text = "${orderDetail.time} min"

    }

    private fun liveData() {
        viewModelAcceptReject.acceptRejectRequest.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    progressBar.gone()
                    timer?.cancel()

                    if (requestAcceptReject) {
                        val fragment = StartRequestFragment()
                        val bundle = Bundle()

                        //orderDetail.status = it.data?.status ?: 4
                        orderDetail = it.data ?: Wash()
                        bundle.putString(ORDER_PROGRESS, Gson().toJson(orderDetail))
                        fragment.arguments = bundle
                        //if(supportFragmentManager?.isStateSaved == true) return

                        activity?.supportFragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                        activity?.supportFragmentManager?.beginTransaction()?.add(
                            R.id.container,
                            fragment, orderDetail.id.toString()
                        )?.commitAllowingStateLoss()
                    } else
                        activity?.finish()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    //ApisRespHandler.handleError(it.error, activity)
                    errorMessage(activity, it.data?.message)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }


    private fun errorMessage(activity: Activity?, message: String?) {
        try {
            val alertDialog = AlertDialog.Builder(activity as Activity)
            alertDialog.setCancelable(false)
            alertDialog.setTitle(activity.getString(R.string.alert))
            alertDialog.setMessage(message)
            alertDialog.setPositiveButton(activity.getString(R.string.ok)) { _, _ ->
                activity.finish()
            }
            alertDialog.show()

        } catch (ignored: Exception) {
        }

    }

    private val buttonsDisableRunnable = Runnable {
        /*tvAccept.isEnabled = true
        tvCancel.isEnabled = true*/
    }


    private fun setListeners() {

        tvAccept.setOnClickListener {
            if (isConnectedToInternet(activity, true)) {
                requestAcceptReject = true

                val hashMap = HashMap<String, Any>()
                hashMap["status"] = ServiceStatus.ACCEPT

                viewModelAcceptReject.acceptRejectRequest(orderDetail.wash_request_id ?: 0, hashMap)

            }
        }
        tvCancel.setOnClickListener {
            if (isConnectedToInternet(activity, true)) {
                requestAcceptReject = false

                val hashMap = HashMap<String, Any>()
                hashMap["status"] = ServiceStatus.REJECT_BY_WASHER

                viewModelAcceptReject.acceptRejectRequest(orderDetail.wash_request_id ?: 0, hashMap)

            }
        }
    }

    private fun startTimer() {
        timer?.start()
    }

    interface OnAcceptRequest {
        fun requestAccepted(order: Wash?)
        fun requestRejected()
        fun refreshOrders(order: Wash?)
        fun onOrderTimeoutOrError()
    }

}
