package com.codebrew.mrsteam2.ui.common.main.home.upcomingwash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.home.HomeAdapter
import com.codebrew.mrsteam2.ui.driver.main.history.AllWashesViewModel
import com.codebrew.mrsteam2.utils.AppRequestCode
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.UPCOMING_WASH
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_home_upcoming_wash.*

class UpcomingWashActivity : AppCompatActivity() {

    private lateinit var viewModel: AllWashesViewModel
    private lateinit var progressDialog: ProgressDialog
    private val upcomingAdapter = HomeAdapter(null,UPCOMING_WASH)

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_upcoming_wash)

        adapterSetting()
        initialise()
        liveData()
        listener()
    }

    private fun listener() {
        tbrHomeUpcomingWash.setNavigationOnClickListener {
            onBackPressed()
        }

        swipeRefreshLayout.setOnRefreshListener {
            if (isConnectedToInternet(this, true))
                viewModel.allWashes(1)
        }
    }

    private fun adapterSetting() {
        rvUpcomingWash.layoutManager = LinearLayoutManager(this)
        rvUpcomingWash.adapter = upcomingAdapter
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[AllWashesViewModel::class.java]
        progressDialog = ProgressDialog(this)

        if (isConnectedToInternet(this, true))
            viewModel.allWashes(1)
    }


    private fun liveData() {
        viewModel.washesRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    upcomingAdapter.setDataUpcoming(resources.data?.upcoming_washes ?: ArrayList())
                }

                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppRequestCode.HOME_UPDATE) {
                setResult(Activity.RESULT_OK)
                if (isConnectedToInternet(this, true))
                    viewModel.allWashes(1)
            }
        }
    }
}