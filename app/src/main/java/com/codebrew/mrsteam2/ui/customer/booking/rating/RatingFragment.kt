package com.codebrew.mrsteam2.ui.customer.booking.rating


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.ORDER
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import com.codebrew.mrsteam2.utils.setRoundProfileUrl
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_rating.*

class RatingFragment : Fragment() {

    private var prevSelectedRating: TextView? = null

    private var rating = 0

    private var order: Wash? = null

    private var progressDialog: ProgressDialog? = null

    private val ratingUnselectedDrawables = listOf(
            R.drawable.notification, R.drawable.notification, R.drawable.notification,
            R.drawable.notification, R.drawable.notification
    )

    private val ratingselectedDrawables = listOf(
            R.drawable.notification,
            R.drawable.notification, R.drawable.notification, R.drawable.notification, R.drawable.notification
    )


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //presenter.attachView(this)
        progressDialog = ProgressDialog(activity)
        setData()
        setListeners()
    }

    private fun setData() {
        order = Gson().fromJson(arguments?.getString(ORDER), Wash::class.java)
        ivDriverImage.setRoundProfileUrl(order?.driver?.profile_pic_url)
        tvDriverName.text = order?.driver?.name
    }

    private fun setListeners() {
        tvRate1.setOnClickListener(ratingsClickListener)
        tvRate2.setOnClickListener(ratingsClickListener)
        tvRate3.setOnClickListener(ratingsClickListener)
        tvRate4.setOnClickListener(ratingsClickListener)
        tvRate5.setOnClickListener(ratingsClickListener)
        tvSubmit.setOnClickListener { checkValidations() }
    }

    private val ratingsClickListener = View.OnClickListener {
        prevSelectedRating?.setCompoundDrawablesWithIntrinsicBounds(
                0,
                ratingUnselectedDrawables[prevSelectedRating?.tag.toString().toInt() - 1],
                0,
                0
        )
        rating = it.tag.toString().toInt()
        (it as TextView).setCompoundDrawablesWithIntrinsicBounds(0, ratingselectedDrawables[rating - 1], 0, 0)
        prevSelectedRating = it
        tvRate1.isSelected = rating == 1
        tvRate2.isSelected = rating == 2
        tvRate3.isSelected = rating == 3
        tvRate4.isSelected = rating == 4
        tvRate5.isSelected = rating == 5
    }

    private fun checkValidations() {
        //presenter.checkValidations(rating, tvAddComment.text.toString().trim())
    }

    /*override fun onValidationsResult(isSuccess: Boolean?, message: Int?) {
        if (isSuccess == true) {
            rateApiCall()
        } else {
            //rootView.showSnack(message ?: 0)
        }
    }*/

    private fun rateApiCall() {
        if (isConnectedToInternet(activity, true)) {
            //presenter.rateOrder(rating, tvAddComment.text.toString().trim(), order?.order_id)
        }


        /*override fun onApiSuccess() {
            if (order?.payment?.payment_type == PaymentType.E_TOKEN) {
                activity?.finish()
            } else {
                fragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                val fragment = SelectCarFragment()
                (activity as? BookingActivity)?.serviceRequestModel = ServiceRequestModel()
                (activity as? BookingActivity)?.selectCarFragment = fragment
                fragmentManager?.beginTransaction()?.add(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
            }
        }

        override fun showLoader(isLoading: Boolean) {
            progressDialog?.show(isLoading)
        }

        override fun apiFailure() {
            rootView?.showSWWerror()
        }

        override fun handleApiError(code: Int?, error: String?) {
            if (code == StatusCode.UNAUTHORIZED) {
                AppUtils.logout(activity)
            } else {
                rootView?.showSnack(error ?: "")
            }
            */
    }

}
