package com.codebrew.mrsteam2.ui.common.main.profile.myoffers

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Offers
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyOffersViewModel : ViewModel() {
    val myOfferRes by lazy { SingleLiveEvent<Resource<ArrayList<Offers>>>() }

    fun myOffers() {
        myOfferRes.value = Resource.loading()


        RetrofitClient.getApi().myOffers()
                .enqueue(object : Callback<ApiResponse<ArrayList<Offers>>> {
                    override fun onFailure(call: Call<ApiResponse<ArrayList<Offers>>>, throwable: Throwable) {
                        myOfferRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<ArrayList<Offers>>>,
                            response: Response<ApiResponse<ArrayList<Offers>>>
                    ) {
                        if (response.isSuccessful) {
                            myOfferRes.value = Resource.success(response.body()?.data)
                        } else {
                            myOfferRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}