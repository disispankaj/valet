package com.codebrew.mrsteam2.ui.common.main.profile.services

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.customer.booking.service.ServicesViewModel
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_services.*

class ServicesActivity : AppCompatActivity() {

    private val serviceAdapter = ServicesAdapter()
    private lateinit var viewModel: ServicesViewModel
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_services)

        listener()
        initialise()
        liveData()
        setAdapter()
    }

    private fun listener() {
        tbrServices.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setAdapter() {
        rvServices.layoutManager = LinearLayoutManager(this@ServicesActivity, RecyclerView.VERTICAL, false)
        rvServices.adapter = serviceAdapter

//        rvServices.setOnClickListener {
//            rvServices.setBackgroundResource(R.color.mrSteamMain)
//        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[ServicesViewModel::class.java]
        progressDialog = ProgressDialog(this)

        if (isConnectedToInternet(this, true))
            viewModel.services()
    }

    private fun liveData() {
        viewModel.services.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    serviceAdapter.setData(resources.data ?: ArrayList())

                }
                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })
    }

}