package com.codebrew.mrsteam2.ui.customer.ratereview

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.ui.common.review.RateReviewViewModel
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_rate_review.*

class RateReviewActivity : AppCompatActivity() {

    private lateinit var viewModel: RateReviewViewModel
    private lateinit var progressDialog: ProgressDialog
    private var washDetail: Wash? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_review)
        initialise()
        listener()
        getData()
        liveData()
    }

    private fun getData() {
        if (intent?.hasExtra(DETAIL_DATA) == true) {
            washDetail = intent.getSerializableExtra(DETAIL_DATA) as Wash
        }

        tvName.text = washDetail?.user?.name
        loadImage(this, ivDP, washDetail?.user?.image_url?.thumbnail, washDetail?.user?.image_url?.original)

    }


    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[RateReviewViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }

    private fun listener() {
        tbrRateReview.setNavigationOnClickListener {
            onBackPressed()
        }

        btnSubmit.setOnClickListener {
            if (validateOk())
                if (isConnectedToInternet(this, true)) {
             //       hitRateApi()

                    Toast.makeText(this,"Thank you",Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, MainActivity::class.java))
                }
        }
    }

    private fun validateOk(): Boolean {
        return when {

            ratingBarRateReview.progress == 0 -> {
                etReview.showSnack(getString(R.string.error_empty_rating))
                false
            }
            etReview.text.toString().trim().isEmpty() -> {
                invalidString(etReview, getString(R.string.error_empty_review))
                false
            }
            else ->
                return true
        }
    }

    private fun hitRateApi() {

        val map = HashMap<String, String>()
        map.put("rating_to", washDetail?.user?.id.toString())
        map.put("wash_request_id", washDetail?.id.toString())
        map.put("rating", ratingBarRateReview.progress.toString())
        map.put("comment", etReview.text.toString().trim())
        viewModel.rateReview(map)
    }

    private fun liveData() {

        viewModel.rateRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    startActivity(
                        Intent(this, DetailActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra(DETAIL_DATA, resources.data)
                    )
                    finish()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }

        })
    }
}