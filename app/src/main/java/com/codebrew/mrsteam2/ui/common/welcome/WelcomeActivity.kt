package com.codebrew.mrsteam2.ui.common.welcome

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import androidx.appcompat.app.AppCompatActivity
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.utils.LANG
import com.codebrew.mrsteam2.utils.PrefsManager
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        if (PrefsManager.get().getString(LANG, "").equals("")) {
            PrefsManager.get().save(LANG, getString(R.string.str_lang_en))
        }

        var language= PrefsManager.get().getString(LANG, getString(R.string.str_lang_en))?:"en"
        if(language.equals("es")){
            btnLanguage.setText(R.string.spanish_spanish)
        }


        listeners()
    }

    private fun listeners() {

        btnLanguage.setOnClickListener {
            popupWindowStart()
        }
    }

    private fun popupWindowStart() {
        val listLang = ArrayList<String>()
        listLang.add(getString(R.string.english))
        listLang.add(getString(R.string.spanish_spanish))
        val popup = ListPopupWindow(this)
        popup.setAdapter(ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listLang))
        popup.anchorView = btnLanguage
        popup.isModal = true
        popup.setOnItemClickListener { _, _, position, _ ->
            btnLanguage.text = listLang[position]
            if (position == 0) {
                PrefsManager.get().save(LANG, getString(R.string.str_lang_en))

                iv.setOnClickListener {
                    goAhead(it)
                }

            } else {
                PrefsManager.get().save(LANG, getString(R.string.str_lang_es))

                iv.setOnClickListener {
                    goAhead(it)
                }
            }
            popup.dismiss()
        }

        popup.show()


    }

    fun goAhead(view: View) {
        val intent = Intent(this, LoginSignupActivity::class.java)
        startActivity(intent)
    }

}
