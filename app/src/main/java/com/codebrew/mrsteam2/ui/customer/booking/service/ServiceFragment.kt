package com.codebrew.mrsteam2.ui.customer.booking.service


import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.Active_sub_service
import com.codebrew.mrsteam2.network.responsemodels.Services
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.bookingtype.BookingTypeFragment
import com.codebrew.mrsteam2.ui.customer.booking.dropofflocation.DropOffLocationFragment
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.fragment_service.*

/**
 * Fragment to get the details of the order for the service: Gas, Mineral Water and Water Tanker
 * after getting the location and addresses from [DropOffLocationFragment]
 * */
class ServiceFragment : Fragment(), ServiceAdapter.ItemSelectedListener {

    private var bookingActivity: BookingActivity? = null

    private lateinit var serviceRequest: ServiceRequestModel

    private lateinit var serviceAdapter: ServiceAdapter

    private lateinit var subServiceAdapter: SubServiceAdapter

    private lateinit var viewModel: ServicesViewModel

    private var selectedPosition = 0 // selected brand position

    var subCategoryId = -1  // selected product position

    private var categoriesList: ArrayList<Services>? = ArrayList()

    private var subCategoriesList: ArrayList<Active_sub_service>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /* Enter and exit animations for this fragment */
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialise()
        liveData()
        setListeners()

        /*If user already done this step*/
        if (categoriesList?.isEmpty() == true && serviceRequest.servicePosition != -1) {
            selectedPosition = serviceRequest.servicePosition ?: -1
            categoriesList?.clear()
            categoriesList?.addAll(serviceRequest.services ?: emptyList())

            subCategoriesList?.clear()
            subCategoriesList?.addAll(categoriesList?.get(selectedPosition)?.active_sub_services
                    ?: emptyList())
        }
        setAdapter()

        if (categoriesList?.isEmpty() == true && isConnectedToInternet(activity, true))
            viewModel.services()
        /* else {
             subCategoriesList?.clear()
             subCategoriesList?.addAll(serviceRequest.subserviceId ?: emptyList())
             subServiceAdapter.notifyDataSetChanged()
         }*/
    }

    private fun setAdapter() {
        rvService.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        serviceAdapter = ServiceAdapter(activity, categoriesList, selectedPosition)
        rvService.adapter = serviceAdapter
        serviceAdapter.setItemSelectedListener(this)

        rvServiceDetails.layoutManager = LinearLayoutManager(activity)
        subServiceAdapter = SubServiceAdapter(subCategoriesList, this, true,true)
        rvServiceDetails.adapter = subServiceAdapter
    }

    private fun initialise() {
        bookingActivity = activity as BookingActivity
        viewModel = ViewModelProviders.of(this)[ServicesViewModel::class.java]

    }

    private fun liveData() {
        viewModel.services.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    bookingActivity?.rlLoader?.gone()

                    categoriesList?.clear()
                    categoriesList?.addAll(resources.data ?: emptyList())
                    serviceAdapter.notifyDataSetChanged()

                    if (categoriesList?.isNotEmpty() == true) {
                        subCategoriesList?.clear()
                        subCategoriesList?.addAll(categoriesList?.get(0)?.active_sub_services
                                ?: emptyList())
                        subServiceAdapter.notifyDataSetChanged()
                    }
                }
                Status.ERROR -> {
                    bookingActivity?.rlLoader?.gone()
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    bookingActivity?.rlLoader?.visible()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDestroyView() {
        super.onDestroyView()

    }

    /* gets the selected position of the category brand*/
    private fun getSelectedPosition(categoryBrandId: Int): Int {

        return 0
    }

    private fun setListeners() {

        btnNext.setOnClickListener {
            btnNext.hideKeyboard()
            if (checkValidations()) {
                serviceRequest.services = ArrayList()
                serviceRequest.services?.addAll(categoriesList ?: ArrayList())
                serviceRequest.servicePosition = selectedPosition
                serviceRequest.serviceId = categoriesList?.get(selectedPosition)?.id
                serviceRequest.serviceName = categoriesList?.get(selectedPosition)?.dual_name ?: ""
                serviceRequest.subService = subCategoryId
                serviceRequest.subserviceId = ArrayList()
                serviceRequest.subserviceId?.addAll(subCategoriesList ?: ArrayList())

                fragmentManager?.beginTransaction()?.replace(R.id.container, BookingTypeFragment())
                        ?.addToBackStack("backstack")?.commit()
            }
        }
    }

    /* Validates the entered data by user*/
    private fun checkValidations(): Boolean {
        return when {
            categoriesList?.isEmpty() == true -> {
                rvService.showSnack(R.string.select_service)
                false
            }
            subCategoriesList?.isEmpty() == true || subCategoryId == -1 -> {
                rvServiceDetails.showSnack(R.string.select_sub_service)
                false
            }
            else -> true
        }
    }

    /* Callback for select brand */
    override fun onItemSelected(position: Int) {
        setCategoriesData(position)
    }

    /* Sets the selected category's data*/
    private fun setCategoriesData(position: Int) {
        selectedPosition = position

        subCategoriesList?.clear()

        for (i in categoriesList?.get(position)?.active_sub_services!!.indices){
            var dat = categoriesList?.get(position)?.active_sub_services!![i]
            dat.isSelected = false
            subCategoriesList!!.add(dat)
        }
        subServiceAdapter.notifyDataSetChanged()
    }

    /* Calculates all the charges related to the selected service*/
    private fun getFinalCharge(): Double? {
        return 0.0
    }
}
