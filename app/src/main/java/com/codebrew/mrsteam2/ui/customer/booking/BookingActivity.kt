package com.codebrew.mrsteam2.ui.customer.booking

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Location
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.MrSteamApi
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.PolylineModel
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.network.responsemodels.homeapi.ServiceDetails
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.customer.booking.deliverystarts.DeliveryStartsFragment
import com.codebrew.mrsteam2.ui.customer.booking.processingrequest.ProcessingRequestFragment
import com.codebrew.mrsteam2.ui.customer.booking.selectcar.SelectCarFragment
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.ui.driver.servicerequest.WashDetailsViewModel
import com.codebrew.mrsteam2.utils.*
import com.codebrew.mrsteam2.utils.location.LocationProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*

/**
 * This activity is the home screen for the user after successful login.
 * Shows the google maps.
 * Acts  as a container for whole booking process of all kinds of services using fragments.
 * Provides side navigation bar to navigate through other screens of the application.
 * Keeps server updated to the current location of the user.
 * */
class BookingActivity : AppCompatActivity(), OnMapReadyCallback {

    private var location: Location? = null

    private lateinit var progressDialog: ProgressDialog

    private lateinit var viewModel: WashDetailsViewModel

    private val ratingDrawables = listOf(
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification
    )

    var srcMarker: Marker? = null

    var destMarker: Marker? = null

    var mapFragment: SupportMapFragment? = null

    var selectCarFragment: SelectCarFragment? = SelectCarFragment()

    private var deliverStartFragment: DeliveryStartsFragment? = null

    var serviceDetails: ServiceDetails? = null

    var serviceRequestModel = ServiceRequestModel()

    var googleMapHome: GoogleMap? = null

    lateinit var locationProvider: LocationProvider

    private var line: Polyline? = null
    private var estimatedTime: String = ""
    private var estimatedDistance: String = ""
    private var list = ArrayList<LatLng>()
    private var carMarker: Marker? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)

        initialise()
        // drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        // setHeaderView()
        liveData()
    }

    private fun initialise() {
        getMapAsync()
        viewModel = ViewModelProviders.of(this)[WashDetailsViewModel::class.java]
        //AppSocket.get().init(this) // Initialize socket

        progressDialog = ProgressDialog(this)
        //AppSocket.get().init(this) // Initialize socket
        FirebaseApp.initializeApp(this) // Initialise firebase app
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            try {
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStackImmediate(
                        "backstack",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finishAffinity()
            }
            checkServiceInProgress(intent)

            mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        })
    }


    private fun checkServiceInProgress(intent: Intent) {
        if (intent.hasExtra(ORDER_PROGRESS)) {
            val request = Gson().fromJson(intent.getStringExtra(ORDER_PROGRESS), Wash::class.java)
            moveToFragment(request)
        } else if (intent.hasExtra(DETAIL_DATA)) {
            val data = intent.getSerializableExtra(DETAIL_DATA) as Wash
            if (isConnectedToInternet(this, true))
                viewModel.washDetails(data.id ?: 0)
        } else {
            supportFragmentManager.beginTransaction().add(
                R.id.container, selectCarFragment
                    ?: SelectCarFragment()
            )
                .addToBackStack("backstack").commitAllowingStateLoss()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        checkServiceInProgress(intent ?: Intent())
    }

    /*If request is in pending*/
    private fun moveToFragment(request: Wash?) {
        if (request?.status == ServiceStatus.COMPLETED) {

            if (request.is_reviewed == true)
                startActivity(
                    Intent(this, DetailActivity::class.java)
                        .putExtra(DETAIL_DATA, request)
                )
            else {
                startActivity(
                    Intent(this, RateReviewActivity::class.java)
                        .putExtra(DETAIL_DATA, request)
                )
            }
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            val fragment = when (request?.status) {
                ServiceStatus.REQUEST -> ProcessingRequestFragment()
                ServiceStatus.ACCEPT -> DeliveryStartsFragment()
                ServiceStatus.JOURNEY_STARTED -> DeliveryStartsFragment()
                ServiceStatus.WASHING_STARTED -> DeliveryStartsFragment()
                else -> SelectCarFragment()
            }
            val bundle = Bundle()

            if (fragment is DeliveryStartsFragment)
                deliverStartFragment = fragment

            bundle.putString(ORDER_PROGRESS, Gson().toJson(request))
            fragment.arguments = bundle
            //if(supportFragmentManager?.isStateSaved == true) return
            //activity?.supportFragmentManager?.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)

            if (supportFragmentManager.isStateSaved) return
            supportFragmentManager.popBackStack("backstack", FragmentManager.POP_BACK_STACK_INCLUSIVE)
            supportFragmentManager.beginTransaction().replace(R.id.container, fragment)
                .addToBackStack("backstack").commit()
        }
    }

    private fun liveData() {
        viewModel.washDetails.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    val request = it.data
                    moveToFragment(request)
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(it.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll() // Clears all the notifications in the system notification tray if exists
        updateDataApiCall(
            (location?.latitude ?: 0.0).toString(), (location?.longitude
                ?: 0.0).toString()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /* Callback providing google maps object showing map is ready to use after getMapAsync*/
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        googleMapHome = googleMap
        googleMapHome?.clear()
        googleMapHome?.setOnMarkerClickListener { true }
        //val mapType = SharedPrefs.with(this).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        googleMapHome?.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMapHome?.uiSettings?.isTiltGesturesEnabled =
            false // Disables the gesture to view 3D view of the google maps
        googleMapHome?.uiSettings?.isMyLocationButtonEnabled =
            false // Hides the default current locator button of google maps
        locationProvider = LocationProvider.LocationUpdatesBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener { location ->
            /* Gets the last known location of the user*/
            location?.let {
                this@BookingActivity.location = location
                googleMapHome?.isMyLocationEnabled = false
                updateDataApiCall((location.latitude).toString(), (location.longitude).toString())
                googleMapHome?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(location.latitude, location.longitude), 14f
                    )
                )
                locationProvider.getAddressFromLatLng(
                    location.latitude,
                    location.longitude,
                    object : LocationProvider.OnAddressListener {
                        override fun getAddress(address: String, result: List<Address>) {
                            /* gets the address from the current user's location*/
                            val currentLocation = CurrentLocationModel(location.latitude, location.longitude, address)
                            PrefsManager.get().save(PrefsConstant.CURRENT_LOCATION, currentLocation)
                            selectCarFragment?.onMapReady(googleMap)
                            deliverStartFragment?.updateTrack()
                        }
                    })
            }
        })
    }

    /* Api call to update the user's latest data like location and fcm_id */
    private fun updateDataApiCall(lat: String, lng: String) {
        if (!isConnectedToInternet(this, true)) {
            return
        }
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                //Logger.e("getInstanceId failed", it.exception?.message ?: "")
            } else {
                // Get new Instance ID token
                val token = it.result?.token
                //PrefsManager.get().save(FCM_TOKEN, token)
                /* val map = HashMap<String, String>()
                 map["timezone"] = TimeZone.getDefault().id
                 map["latitude"] = lat
                 map["longitude"] = lng
                 map["fcm_id"] = token
                 map["language_id"] = getLanguageId(LocaleManager.getLanguage(this)).toString()
                 presenter.updateDataApi(map)
                 Logger.e("getFirebaseInstanceId", token)*/
            }
        }
    }

    /* Get google maps instance*/
    fun getMapAsync() {
        mapFragment?.getMapAsync(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    /**
     * Show markers for both source and destination LatLng and draw polyline path
     * between source and destination
     *
     * @param source source LatLng provided by the user.
     * @param destination destination LatLng provided by the user.
     * */
    fun showMarker(source: LatLng?, destination: LatLng?) {
        /*add marker for both source and destination*/
        srcMarker = googleMapHome?.addMarker(source?.let { MarkerOptions().position(it) })
        srcMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)))
        srcMarker?.setAnchor(0.5f, 0.5f)
        destMarker = googleMapHome?.addMarker(destination?.let { MarkerOptions().position(it) })
        destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_addcar_hatchback)))
        destMarker?.setAnchor(0.5f, 0.5f)
        // presenter.drawPolyLine(source, destination, language = LocaleManager.getLanguage(this))
    }

    private fun decodePoly(encoded: String): ArrayList<LatLng> {
        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }
        return poly
    }

    override fun onBackPressed() {
        when {
            supportFragmentManager.fragments.find { it is ProcessingRequestFragment } != null -> {
                // Do nothing
            }
            supportFragmentManager.backStackEntryCount == 1 -> {
                finish()
            }
            else -> super.onBackPressed()
        }
    }

    var callPolyline: Call<ResponseBody>? = null
    var isPolylineApiCalling = false
    fun drawPolyLine(sourceLat: Double?, sourceLong: Double?, destLat: Double?, destLong: Double?) {
        if (isPolylineApiCalling) return
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(MrSteamApi::class.java)
//        callPolyline?.cancel()
        isPolylineApiCalling = true
        callPolyline = api.getPolYLine(
            "$sourceLat,$sourceLong",
            "$destLat,$destLong", Locale.US.language
        )
        callPolyline?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isPolylineApiCalling = false
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }/* else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }*/
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                isPolylineApiCalling = false
//                getView()?.apiFailure()
            }
        })
    }

    fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        val routes = routeArray.getJSONObject(0)
        line?.remove()
        val overviewPolyLines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolyLines.getString("points")
        val legsJsonObject = (((routes.get("legs") as JSONArray).get(0) as JSONObject))
        estimatedDistance = (legsJsonObject.get("distance") as JSONObject).get("text") as String
        estimatedTime = (legsJsonObject.get("duration") as JSONObject).get("text") as String
        sendBroadcast(Intent("updateETA").putExtra("time", estimatedTime).putExtra("distance", estimatedDistance))
        line?.remove()
        list.clear()
        list.addAll(decodePoly(encodedString))


        line = googleMapHome?.addPolyline(
            PolylineOptions()
                .addAll(list)
                .width(10f)
                .color(ContextCompat.getColor(this, R.color.mrSteamMain))
                .geodesic(true)
        )
        if (list.size >= 2) {
            val polylineModel = PolylineModel(
                encodedString,
                estimatedDistance,
                (legsJsonObject.get("distance") as JSONObject).getInt("value"),
                estimatedTime,
                (legsJsonObject.get("duration") as JSONObject).getInt("value")
            )

            //updateMarkerOfDriver(list[0], polylineModel)
        }
    }

    fun clearPolyline() {
        line?.remove()
    }
}
