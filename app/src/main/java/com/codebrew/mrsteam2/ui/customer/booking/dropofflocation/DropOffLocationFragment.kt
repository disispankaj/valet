package com.codebrew.mrsteam2.ui.customer.booking.dropofflocation


import android.location.Address
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.bookingtype.BookingTypeFragment
import com.codebrew.mrsteam2.ui.customer.booking.vehicles.SelectVehicleTypeFragment
import com.codebrew.mrsteam2.utils.*
import com.codebrew.mrsteam2.utils.location.LocationProvider
import com.google.android.gms.location.LocationAvailability
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceBufferResponse
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.RuntimeRemoteException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.android.synthetic.main.fragment_drop_off_location.*


/**
 * Fragment to get the user's pickup and drop off address after selecting one of the service
 * from SelectCarFragment.
 */
class DropOffLocationFragment : Fragment(), View.OnClickListener {


    private var mGeoDataClient: GeoDataClient? = null

    private lateinit var adapterPickUp: PlaceAutocompleteAdapter

    private lateinit var adapterDropOff: PlaceAutocompleteAdapter

    private var pLat = 0.0

    private var pLng = 0.0

    private var pAddress = ""

    private var pTempAddress = ""

    private var dLat = 0.0

    private var dLng = 0.0

    private var dAddress = ""

    private var dTempAddress = ""

    private var map: GoogleMap? = null

    private var currentLocation: Location? = null

    private var isCurrentLocation: Boolean = false

    private var serviceRequest: ServiceRequestModel? = null

    private var isPickupSelected = true

    private var isPickupAddressRequired = false

    private var cameraMoveReason = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mGeoDataClient = Places.getGeoDataClient(activity as BookingActivity)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /* Enter and exit animation of this fragment */
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_drop_off_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //map = (activity as BookingActivity).googleMapHome
        map?.clear()
        (activity as BookingActivity).ivMarker.visibility = View.VISIBLE
        serviceRequest?.category_brand_id = -1

        val mapType = PrefsManager.get().getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        tvMapNormal.isSelected = mapType == GoogleMap.MAP_TYPE_NORMAL
        tvSatellite.isSelected = mapType == GoogleMap.MAP_TYPE_HYBRID

        serviceRequest = (activity as BookingActivity).serviceRequestModel
        pLat = serviceRequest?.pickup_latitude ?: 0.0
        pLng = serviceRequest?.pickup_longitude ?: 0.0
        dLat = serviceRequest?.dropoff_latitude ?: 0.0
        dLng = serviceRequest?.dropoff_longitude ?: 0.0
        isCurrentLocation = serviceRequest?.isCurrentLocation ?: false
        if (isCurrentLocation) {
            fabMyLocation.setImageResource(R.drawable.notification)
        } else {
            fabMyLocation.setImageResource(R.drawable.notification)
        }
        pAddress = serviceRequest?.pickup_address ?: ""
        dAddress = serviceRequest?.dropoff_address ?: ""
        val latLngBounds = LatLngBounds.builder()
        latLngBounds.include(LatLng(pLat + 1, pLng + 1))
        when (serviceRequest?.category_id) {
            CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                /* We needs of drop off address in case of these services*/
                isPickupSelected = false
                isPickupAddressRequired = false
                acPickupAddress?.visibility = View.GONE
                latLngBounds.include(LatLng(pLat + 1, pLng + 1))
                latLngBounds.include(LatLng(pLat - 1, pLng - 1))

            }
            CategoryId.FREIGHT, CategoryId.TOW -> {
                /* We needs both pickup and drop off address in case of these services*/
                isPickupSelected = true
                isPickupAddressRequired = true
                acPickupAddress?.isFocusable = false
                acPickupAddress?.isFocusableInTouchMode = false
                acDropOffAddress?.isFocusable = false
                acDropOffAddress?.isFocusableInTouchMode = false
                (activity as BookingActivity).ivMarker?.setImageResource(R.drawable.notification)
                if (pLat != 0.0 && pLng != 0.0) {
                    val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(pLat, pLng), ZOOM_LEVEL)
                    map?.animateCamera(cameraUpdate)
                }
                latLngBounds.include(LatLng(dLat + 1, dLng + 1))
                latLngBounds.include(LatLng(dLat - 1, dLng - 1))
                acPickupAddress.visibility = View.VISIBLE
                adapterPickUp = PlaceAutocompleteAdapter(activity, mGeoDataClient, latLngBounds.build(), null)
                acPickupAddress?.setAdapter(adapterPickUp)
                acPickupAddress?.onItemClickListener = onPickupItemClickListener
                acPickupAddress?.setOnClickListener {
                    isPickupSelected = true
                    acPickupAddress?.isFocusable = true
                    acPickupAddress?.isFocusableInTouchMode = true
                    acDropOffAddress?.isFocusable = false
                    acDropOffAddress?.isFocusableInTouchMode = false

                    acPickupAddress.requestFocus()
                    (activity as BookingActivity).ivMarker?.setImageResource(R.drawable.notification)
                    if (pLat != 0.0 && pLng != 0.0) {
                        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(pLat, pLng), ZOOM_LEVEL)
                        map?.animateCamera(cameraUpdate)
                    }
                }
                acDropOffAddress.setOnClickListener {
                    isPickupSelected = false
                    acPickupAddress.isFocusable = false
                    acPickupAddress?.isFocusableInTouchMode = false
                    acDropOffAddress.isFocusable = true
                    acDropOffAddress?.isFocusableInTouchMode = true
                    acDropOffAddress.requestFocus()
                    (activity as BookingActivity).ivMarker?.setImageResource(R.drawable.notification)
                    if (dLat != 0.0 && dLng != 0.0) {
                        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(LatLng(dLat, dLng), ZOOM_LEVEL)
                        map?.animateCamera(cameraUpdate)
                    }
                }
                acPickupAddress.setText(pAddress)
                pTempAddress = acPickupAddress.text.toString().trim()
            }
        }
        adapterDropOff = PlaceAutocompleteAdapter(activity, mGeoDataClient, latLngBounds.build(), null)
        acDropOffAddress.setAdapter(adapterDropOff)
        acDropOffAddress.onItemClickListener = onDropOffItemClickListener

        acDropOffAddress.setText(dAddress)
        setListeners()
        dTempAddress = acDropOffAddress.text.toString().trim()
    }

    private fun setListeners() {
        this.map?.setOnCameraMoveStartedListener(onCameraMoveStarted)
        this.map?.setOnCameraIdleListener(onCameraMoveIdle)
        ivNext.setOnClickListener(this)
        tvBack.setOnClickListener(this)
        fabMyLocation.setOnClickListener(this)
        tvMapNormal.setOnClickListener(this)
        tvSatellite.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.tvBack -> {
                activity?.onBackPressed()
            }

            R.id.ivNext -> {
                ivNext.hideKeyboard()
                if ((pLat == 0.0 && pLng == 0.0 || acPickupAddress.text.isEmpty()
                                || pTempAddress != acPickupAddress.text.toString().trim())
                        && isPickupAddressRequired
                ) {
                    rootView.showSnack(R.string.pickup_address_validation_message) // Invalid pickup address
                } else if (dLat == 0.0 && dLng == 0.0 || acDropOffAddress.text.isEmpty()
                        || dTempAddress != acDropOffAddress.text.toString().trim()
                ) {
                    rootView.showSnack(R.string.dropoff_address_validation_message) // Invalid drop off address
                } else {
                    if (isPickupAddressRequired) {
                        (activity as BookingActivity).showMarker(LatLng(pLat, pLng), LatLng(dLat, dLng))
                        (activity as BookingActivity).ivMarker.visibility = View.GONE
                    } else {
                        (activity as BookingActivity).ivMarker.visibility = View.VISIBLE
                    }
                    rootView.hideKeyboard()
                    serviceRequest?.pickup_latitude = pLat
                    serviceRequest?.pickup_longitude = pLng
                    serviceRequest?.pickup_address = pAddress
                    serviceRequest?.dropoff_latitude = dLat
                    serviceRequest?.dropoff_longitude = dLng
                    serviceRequest?.dropoff_address = dAddress
                    serviceRequest?.isCurrentLocation = isCurrentLocation
                    when (serviceRequest?.category_id) {
                        CategoryId.GAS, CategoryId.MINERAL_WATER, CategoryId.WATER_TANKER -> {
                            fragmentManager?.beginTransaction()?.replace(R.id.container, BookingTypeFragment())
                                    ?.addToBackStack("backstack")?.commit()
                        }
                        CategoryId.FREIGHT, CategoryId.TOW -> {
                            fragmentManager?.beginTransaction()?.replace(R.id.container, SelectVehicleTypeFragment())
                                    ?.addToBackStack("backstack")?.commit()
                        }

                    }
                }
            }

            R.id.fabMyLocation -> {
                // Animates camera to the current location
                LocationProvider.CurrentLocationBuilder(activity).build()
                        .getLastKnownLocation(OnSuccessListener {
                            currentLocation = it
                            if (it != null) {
                                focusOnLocation(it.latitude, it.longitude)
                            } else {
                                startLocationUpdates()
                            }
                        })
            }

            R.id.tvMapNormal -> {
                /* Change the map type to normal view */
                tvMapNormal.isSelected = true
                tvSatellite.isSelected = false
                map?.mapType = GoogleMap.MAP_TYPE_NORMAL
                PrefsManager.get().save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
            }

            R.id.tvSatellite -> {
                /* Change the map type to Hybrid view */
                tvMapNormal.isSelected = false
                tvSatellite.isSelected = true
                map?.mapType = GoogleMap.MAP_TYPE_HYBRID
                PrefsManager.get().save(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_HYBRID)
            }
        }
    }

    /* Animates map to the location provided */
    private fun focusOnLocation(latitude: Double?, longitude: Double?) {
        val target = LatLng(latitude ?: 0.0, longitude ?: 0.0)
        val builder = CameraPosition.Builder()
        builder.zoom(ZOOM_LEVEL)
        builder.target(target)
        clearAddressOnCameraMove()
        ((activity as BookingActivity).googleMapHome)?.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()))
        cameraMoveReason = GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE
    }

    /* Start getting location from the device */
    private fun startLocationUpdates() {
        val locationProvider = LocationProvider.LocationUpdatesBuilder(activity).apply {
            interval = 1000
            fastestInterval = 1000
        }.build()
        locationProvider.startLocationUpdates(object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                focusOnLocation(p0?.lastLocation?.latitude, p0?.lastLocation?.longitude)
                locationProvider.stopLocationUpdates(this)
                (activity as BookingActivity).getMapAsync()
            }

            override fun onLocationAvailability(p0: LocationAvailability?) {
                super.onLocationAvailability(p0)
            }
        })
    }

    /* Callback indicates the map's camera movement has started because of animate camera animation */
    private val onCameraMoveStarted = GoogleMap.OnCameraMoveStartedListener {
        cameraMoveReason = it
        if (it == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            clearAddressOnCameraMove()
        }
        isCurrentLocation = false
    }

    /* Callback indicates the map's camera animation is now stopped */
    private val onCameraMoveIdle = GoogleMap.OnCameraIdleListener {
        if (isPickupSelected && cameraMoveReason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            pLat = map?.cameraPosition?.target?.latitude ?: 0.0
            pLng = map?.cameraPosition?.target?.longitude ?: 0.0
            pAddress = ""
            (activity as BookingActivity).locationProvider.getAddressFromLatLng(pLat, pLng, pickupAddressListener)
        } else if (!isPickupSelected && cameraMoveReason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            dLat = map?.cameraPosition?.target?.latitude ?: 0.0
            dLng = map?.cameraPosition?.target?.longitude ?: 0.0
            dAddress = ""
            (activity as BookingActivity).locationProvider.getAddressFromLatLng(dLat, dLng, dropOffAddressListener)
        }
        isCurrentLocation = if (MapUtils.getDistanceBetweenTwoPoints(
                        LatLng(
                                map?.cameraPosition?.target?.latitude
                                        ?: 0.0, map?.cameraPosition?.target?.longitude
                                ?: 0.0
                        ), LatLng(
                        currentLocation?.latitude
                                ?: 0.0, currentLocation?.longitude ?: 0.0
                )
                ) < 1
        ) {
            fabMyLocation.setImageResource(R.drawable.notification)
            true
        } else {
            fabMyLocation.setImageResource(R.drawable.notification)
            false
        }
    }

    /* clears the address from the selected address autocomplete view */
    private fun clearAddressOnCameraMove() {
        if (isPickupSelected) {
            pLat = 0.0
            pLng = 0.0
            pAddress = ""
            acPickupAddress.setText(pAddress)
            acPickupAddress.hideKeyboard()
        } else {
            dLat = 0.0
            dLng = 0.0
            dAddress = ""
            acDropOffAddress.setText(dAddress)
            acDropOffAddress.hideKeyboard()
        }
    }

    /* Callbacks after getting pickup address from the selected location of the user */
    private val pickupAddressListener = object : LocationProvider.OnAddressListener {
        override fun getAddress(address: String, result: List<Address>) {
            this@DropOffLocationFragment.pAddress = address
            acPickupAddress?.isFocusable = false
            acPickupAddress?.isFocusableInTouchMode = false
            acPickupAddress?.setText(address)
            acPickupAddress?.requestFocus()
            acPickupAddress?.isFocusable = true
            acPickupAddress?.isFocusableInTouchMode = true

            pTempAddress = address
        }
    }

    /* Callbacks after getting drop off address from the selected location of the user */
    private val dropOffAddressListener = object : LocationProvider.OnAddressListener {
        override fun getAddress(address: String, result: List<Address>) {
            this@DropOffLocationFragment.dAddress = address
            acDropOffAddress?.isFocusable = false
            acDropOffAddress?.isFocusableInTouchMode = false
            acDropOffAddress?.setText(address)
            acDropOffAddress?.requestFocus()
            acDropOffAddress?.isFocusable = true
            acDropOffAddress?.isFocusableInTouchMode = true
            dTempAddress = address
        }
    }

    private val onPickupItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
        val item = adapterPickUp?.getItem(position)
        val placeId = item?.placeId
        val placeResult = mGeoDataClient?.getPlaceById(placeId)
        placeResult?.addOnCompleteListener(mUpdatePickupAddressCallback)
        pTempAddress = acPickupAddress.text.toString().trim()
    }

    private val mUpdatePickupAddressCallback = OnCompleteListener<PlaceBufferResponse> { task ->
        try {
            val places = task.result
            // Get the Place object from the buffer.
            val place = places?.get(0)
            val array = ArrayList<Double>()
            array.add(place?.latLng?.longitude ?: 0.0)
            array.add(place?.latLng?.latitude ?: 0.0)
            pLat = place?.latLng?.latitude ?: 0.0
            pLng = place?.latLng?.longitude ?: 0.0
            pAddress = place?.address.toString()
            val cameraUpdate = CameraUpdateFactory.newLatLng(LatLng(pLat, pLng))
            map?.animateCamera(cameraUpdate)
            places?.release()
        } catch (e: RuntimeRemoteException) {
            // Request did not complete successfully
            Log.e("EnterTransportAddress", "Place query did not complete.", e)
            return@OnCompleteListener
        }
    }

    private val onDropOffItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
        val item = adapterDropOff?.getItem(position)
        val placeId = item?.placeId
        val placeResult = mGeoDataClient?.getPlaceById(placeId)
        placeResult?.addOnCompleteListener(mUpdateDropOffAddressCallback)
        dTempAddress = acDropOffAddress.text.toString().trim()
    }

    private val mUpdateDropOffAddressCallback = OnCompleteListener<PlaceBufferResponse> { task ->
        try {
            val places = task.result
            // Get the Place object from the buffer.
            val place = places?.get(0)
            val array = ArrayList<Double>()
            array.add(place?.latLng?.longitude ?: 0.0)
            array.add(place?.latLng?.latitude ?: 0.0)
            dLat = place?.latLng?.latitude ?: 0.0
            dLng = place?.latLng?.longitude ?: 0.0
            dAddress = place?.address.toString()
            val cameraUpdate = CameraUpdateFactory.newLatLng(LatLng(dLat, dLng))
            map?.animateCamera(cameraUpdate)
            places?.release()
        } catch (e: RuntimeRemoteException) {
            // Request did not complete successfully
            Log.e("EnterTransportAddress", "Place query did not complete.", e)
            return@OnCompleteListener
        }
    }

    override fun onDestroyView() {
        tvMapNormal.hideKeyboard()
        super.onDestroyView()
        map?.setOnCameraMoveStartedListener(null)
        map?.setOnCameraIdleListener(null)
    }

}
