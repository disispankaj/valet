package com.codebrew.mrsteam2.ui.common.main.offerdetails

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.ActiveDetails
import kotlinx.android.synthetic.main.item_offer_details_feature.view.*

class OfferDetailsAdapter(private val features: Boolean) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val listFeatures = ArrayList<ActiveDetails>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_offer_details_feature, null)
        return ViewHolderFeatures(v1)

    }

    override fun getItemCount(): Int {
        return listFeatures.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderFeatures).bind(listFeatures[position])
    }


    inner class ViewHolderFeatures(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(data: ActiveDetails?) {
            itemView.tvFeature.text = data?.sub_service?.dual_title ?: data?.dual_title
        }
    }

    fun setDataFeatures(list: ArrayList<ActiveDetails>) {
        listFeatures.clear()
        listFeatures.addAll(list)
        notifyDataSetChanged()
    }


}
