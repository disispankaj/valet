package com.codebrew.mrsteam2.ui.common.main.profile.editprofile

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.ImageModel
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ImageUploadViewModel : ViewModel() {
    val uploadFile by lazy { SingleLiveEvent<Resource<ImageModel>>() }

    fun uploadFile(hashMap: HashMap<String, RequestBody>) {
        uploadFile.value = Resource.loading()

        RetrofitClient.getApi()
                .profileImage(hashMap)
                .enqueue(object : Callback<ApiResponse<ImageModel>> {
                    override fun onFailure(call: Call<ApiResponse<ImageModel>>, throwable: Throwable) {
                        uploadFile.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<ImageModel>>, response: Response<ApiResponse<ImageModel>>) {
                        if (response.isSuccessful) {
                            uploadFile.value = Resource.success(response.body()?.data)
                        } else {
                            uploadFile.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}