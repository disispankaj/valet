package com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.requestmodels.AddCarRequest
import com.codebrew.mrsteam2.network.responsemodels.AddCar
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCarViewModel : ViewModel() {

    val addCarRes by lazy { SingleLiveEvent<Resource<AddCar>>() }

    fun addCar(addCarReq: AddCarRequest) {
        addCarRes.value = Resource.loading()
        RetrofitClient.getApi().addCar(addCarReq)
                .enqueue(object : Callback<ApiResponse<AddCar>> {
                    override fun onFailure(call: Call<ApiResponse<AddCar>>, throwable: Throwable) {
                        addCarRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<AddCar>>, response: Response<ApiResponse<AddCar>>) {
                        if (response.isSuccessful) {
                            addCarRes.value = Resource.success(response.body()?.data)
                        } else {
                            addCarRes.value = Resource.error(ApiUtils.getError(
                                    response.code(),
                                    response.errorBody()?.string()))
                        }
                    }

                })
    }


    val editCar by lazy { SingleLiveEvent<Resource<AddCar>>() }

    fun editCar(cardId: Int, addCarReq: AddCarRequest) {
        editCar.value = Resource.loading()
        RetrofitClient.getApi().editCar(cardId, addCarReq)
                .enqueue(object : Callback<ApiResponse<AddCar>> {
                    override fun onFailure(call: Call<ApiResponse<AddCar>>, throwable: Throwable) {
                        editCar.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<AddCar>>, response: Response<ApiResponse<AddCar>>) {
                        if (response.isSuccessful) {
                            editCar.value = Resource.success(response.body()?.data)
                        } else {
                            editCar.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }


    val deleteCar by lazy { SingleLiveEvent<Resource<AddCar>>() }

    fun deleteCar(cardId: Int) {
        deleteCar.value = Resource.loading()
        RetrofitClient.getApi().deleteCar(cardId)
                .enqueue(object : Callback<ApiResponse<AddCar>> {
                    override fun onFailure(call: Call<ApiResponse<AddCar>>, throwable: Throwable) {
                        deleteCar.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<AddCar>>, response: Response<ApiResponse<AddCar>>) {
                        if (response.isSuccessful) {
                            deleteCar.value = Resource.success(response.body()?.data)
                        } else {
                            deleteCar.value = Resource.error(ApiUtils.getError(
                                    response.code(),
                                    response.errorBody()?.string()))
                        }
                    }

                })
    }
}