package com.codebrew.mrsteam2.ui.customer.booking.vehicles


import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.buraq24.customer.webservices.models.homeapi.Category
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.submodels.SelectSubModelsFragment
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.showSnack
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_select_vehicle_type.*


class SelectVehicleTypeFragment : Fragment() {

    private var categoriesList: ArrayList<Category>? = ArrayList()

    private lateinit var serviceRequest: ServiceRequestModel

    private var adapter: SelectVehicleTypeAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel
        serviceRequest.category_brand_id = -1
        serviceRequest.category_brand_product_id = -1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_vehicle_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriesList?.clear()
        categoriesList?.addAll((activity as BookingActivity).serviceDetails?.categories as? ArrayList
                ?: ArrayList())
        if (categoriesList?.isEmpty() == false) {
            val selectedPosition = getSelectedPosition(serviceRequest.category_brand_id ?: 0)
            rvCompanies?.layoutManager = GridLayoutManager(activity, 3, GridLayoutManager.VERTICAL, false)
            adapter = SelectVehicleTypeAdapter(categoriesList, selectedPosition)
            rvCompanies?.adapter = adapter
        }

        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (categoriesList?.isNotEmpty() == true) {
            categoriesList?.get(adapter?.getSelectedPosition() ?: 0)?.isSelected = false
        }
    }

    private fun setListeners() {
        tvNext.setOnClickListener {
            if (categoriesList?.isEmpty() == true) {
                rootView.showSnack(R.string.no_vehicles_available)
                return@setOnClickListener
            }
            serviceRequest.category_brand_id = adapter?.getSelectedCategoryBrandId()
            serviceRequest.brandName = adapter?.getSelectedCategoryBrandName()
            val fragment = SelectSubModelsFragment()
            val bundle = Bundle()
            bundle.putString("submodels", Gson().toJson(adapter?.getSelectedCategoryProducts()))
            fragment.arguments = bundle
            fragmentManager?.beginTransaction()?.replace(R.id.container, fragment)?.addToBackStack("backstack")?.commit()
        }
    }

    private fun getSelectedPosition(categoryBrandId: Int): Int {
        for (i in categoriesList.orEmpty().indices) {
            if (categoriesList?.get(i)?.category_brand_id == categoryBrandId) {
                return i
            }
        }
        return 0
    }
}
