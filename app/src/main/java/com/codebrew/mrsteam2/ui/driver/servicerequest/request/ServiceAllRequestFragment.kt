package com.codebrew.mrsteam2.ui.driver.servicerequest.request

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.utils.OrderStatus
import kotlinx.android.synthetic.main.fragment_after_accept.*
import kotlinx.android.synthetic.main.fragment_service_after_start.*
import kotlinx.android.synthetic.main.fragment_service_request.*

class ServiceAllRequestFragment : Fragment(), View.OnClickListener {
    private var orderDetail: Wash? = null
    private var onRequestStatusChange: OnRequestStatusChange? = null
    private var time = ""
    private var distance = ""
    private var timer: CountDownTimer? = null
    private var rating = 5.0
    private var selectedTextView: TextView? = null
    private var progressDialog: ProgressDialog? = null
    private var isMinimized = false


    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            time = intent?.getStringExtra("time") ?: ""
            distance = intent?.getStringExtra("distance") ?: ""
            setDistanceAndTime()
        }
    }
            ;

    private fun setDistanceAndTime() {
        when {
            orderDetail?.order_status == OrderStatus.CONFIRMED || orderDetail?.order_status == OrderStatus.DRIVER_APPROVED -> {
                tvDistanceAccept?.text = distance
                tvTimeAccept?.text = time
            }
            orderDetail?.order_status == OrderStatus.ONGOING -> {
                tvDistanceStart?.text = distance
                tvTimeStart?.text = time
            }
            orderDetail?.order_status == null -> {
                tvDistanceStartRequest?.text = distance
                tvTimeStartRequest?.text = time
            }
        }
    }

    companion object {
        var dialog: Dialog? = null
        var dialogDropoffConfirmation: AlertDialog? = null
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_all_request, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(view)
        dialog = activity?.let { Dialog(it) }
    }

    /*  override fun onChangeTurnSuccess() {
          orderDetail?.order_status = OrderStatus.ONGOING
          HomeActivity.listRequests.forEach {
              it.my_turn = "0"
          }
          val dataOrder = HomeActivity.listRequests.find { orderDetail?.order_id == it.order_id }
          dataOrder?.order_status = OrderStatus.ONGOING
          if (dataOrder != null) {
              orderDetail = dataOrder
          }
          orderDetail?.my_turn = "1"
          onRequestStatusChange?.changeTurnSuccess(orderDetail)
          val gmmIntentUri = Uri.parse("google.navigation:q=${orderDetail?.dropoff_latitude},${orderDetail?.dropoff_longitude}")
          val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
          mapIntent.setPackage("com.google.android.apps.maps")
          val activityExists = activity?.packageManager?.let { packageManager ->
              mapIntent.resolveActivityInfo(packageManager, 0) != null
          }
          if (activityExists == true) {
              startActivity(mapIntent)
          } else {
              view?.showSnack(getString(R.string.google_maps_not_installed))
          }
          //showFragmentAsPerStatus(orderDetail)
      }

      override fun onConfirmAcceptApiSuccess2(order: Order?) {
          onRequestStatusChange?.requestRejected(orderDetail?.order_id)
      }*/

    fun setRequestChangeListener(onRequestStatusChange: OnRequestStatusChange) {
        this.onRequestStatusChange = onRequestStatusChange
    }

    private fun setEndData() {
        /* if (orderDetail?.my_turn == "0") {
             tvDropAtAddressStart.text = getString(R.string.continue_ride)
             ivDirections.visibility = View.GONE
         } else {
             tvDropAtAddressStart.text = getString(R.string.drop_at_address)
             ivDirections.visibility = View.VISIBLE
         }
         val services = resources.getStringArray(R.array.services_list)
         tvProductNameStart.text = if (orderDetail?.category_id == 1) {
             services[1] + " . " + orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
         } else if (orderDetail?.category_id == 3) {
             services[3] + "." + orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
         } else if (orderDetail?.category_id == 4) {
             services[4] + "." + orderDetail?.brand?.name
         } else {
             orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
         }
         tvNameStart.text = orderDetail?.user?.name
         if ((orderDetail?.user?.rating_count ?: 0) > 0) {
             ivRatingStart.visibility = View.VISIBLE
             textViewStart.visibility = View.VISIBLE
             ivRatingStart.setImageResource(getRatingSymbol(orderDetail?.user?.rating_avg?.toInt()
                     ?: 0))
             textViewStart.text = orderDetail?.user?.rating_count?.toString() ?: "0"
         } else {
             ivRatingStart.visibility = View.INVISIBLE
             textViewStart.visibility = View.INVISIBLE
         }
         if (orderDetail?.payment_type == PaymentType.E_TOKEN) {
             val paymentType = getString(getPaymentStringId(orderDetail?.payment?.paymentType
                     ?: "0"))
             tvTotalStart.text = "$paymentType · ${orderDetail?.payment?.productQuantity}"
         } else {
             tvTotalStart.text = getFinalCharge(orderDetail, SharedPrefs.get().getInt(PREF_DISTANCE, 1)) + " " + getString(R.string.currency)
         }
         tvDistanceStart.text = if (arguments?.containsKey("time") == true) arguments?.getString("time")
                 ?: "" else ""
         tvTimeStart?.text = if (arguments?.containsKey("distance") == true) arguments?.getString("distance")
                 ?: "" else ""
         Glide.with(imageViewStart.context).setDefaultRequestOptions(RequestOptions()
                 .circleCrop().placeholder(R.drawable.ic_bg_menu)).load(orderDetail?.user?.profile_pic_url)
                 .into(imageViewStart)*/

    }

    private fun setStartData() {
        /*val services = resources.getStringArray(R.array.services_list)
        tvProductNameAccept.text = when {
            orderDetail?.category_id == 1 -> services[1] + " . " + orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
            orderDetail?.category_id == 3 -> services[3] + " . " + orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
            orderDetail?.category_id == 4 -> services[4] + " . " + orderDetail?.brand?.name
            else -> orderDetail?.brand?.brand_name + " . " + orderDetail?.brand?.name + " × " + orderDetail?.payment?.productQuantity
        }
        tvNameAccept.text = orderDetail?.user?.name
        if ((orderDetail?.user?.rating_count ?: 0) > 0) {
            ivRatingAccept.visibility = View.VISIBLE
            textViewAccept.visibility = View.VISIBLE
            ivRatingAccept.setImageResource(getRatingSymbol(orderDetail?.user?.rating_avg?.toInt()
                    ?: 0))
            textViewAccept.text = orderDetail?.user?.rating_count?.toString() ?: "0"
        } else {
            ivRatingAccept.visibility = View.GONE
            textViewAccept.visibility = View.GONE
        }
        if (orderDetail?.payment_type == PaymentType.E_TOKEN) {
            val paymentType = getString(getPaymentStringId(orderDetail?.payment?.paymentType
                    ?: "0"))
            tvTotalAccept.text = "$paymentType · ${orderDetail?.payment?.productQuantity}"
        } else {
            tvTotalAccept.text = getFinalCharge(orderDetail, SharedPrefs.get().getInt(PREF_DISTANCE, 1)) + " " + getString(R.string.currency)
        }

        Glide.with(imageViewAccept.context).setDefaultRequestOptions(RequestOptions()
                .circleCrop().placeholder(R.drawable.ic_bg_menu)).load(orderDetail?.user?.profile_pic_url)
                .into(imageViewAccept)*/
    }


    private fun init(view: View) {
        /*  presenter.attachView(this)
          progressDialog = DialogIndeterminate(view.context)
          orderDetail = Gson().fromJson(arguments?.getString("order"), Order::class.java)
          activity?.registerReceiver(receiver, IntentFilter("updateETA"))
          showFragmentAsPerStatus(orderDetail)*/
    }

    fun showFragmentAsPerStatus(orderDetail: Wash?) {
        /* when (orderDetail?.order_status) {

             OrderStatus.CONFIRMED -> {
                 onRequestStatusChange?.requestConfirmed(orderDetail)
                 flipperViews.displayedChild = 2
                 setStartListeners()
                 setPolyLine(orderDetail)
                 setStartData()
             }

             OrderStatus.ONGOING -> {
                 onRequestStatusChange?.requestStart(orderDetail)
                 flipperViews.displayedChild = 3
                 setEndListeners()
                 setEndData()
             }

             OrderStatus.SERVICE_COMPLETE -> {
                 flipperViews.displayedChild = 4
                 setInvoiceListeners()
                 setInvoiceData()
             }

             OrderStatus.SERVICE_INVOICED -> {
                 flipperViews.displayedChild = 5
                 setRateListeners()
                 setRatingDefault()
                 setRatingData()
             }

             OrderStatus.DRIVER_ARROVED -> {
                 onRequestStatusChange?.requestConfirmed(orderDetail)
                 flipperViews.displayedChild = 2
                 setStartListeners()
                 setPolyLine(orderDetail)
                 setStartData()
             }
         }*/
    }

    private fun setPolyLine(orderDetail: Wash) {
        /* val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                 0.0f)
         val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
         val destLatLong =
                 when (this.orderDetail?.brand?.category_id) {
                     CategoryId.GAS, CategoryId.WATER_TANKER, CategoryId.MINERAL_WATER -> {
                         LatLng(orderDetail.dropoff_latitude
                                 ?: 0.0, orderDetail.dropoff_longitude
                                 ?: 0.0)
                     }
                     else -> {
                         LatLng(this.orderDetail?.pickup_latitude
                                 ?: 0.0, this.orderDetail?.pickup_longitude
                                 ?: 0.0)
                     }
                 }

         presenter.drawPolyLine(lat.toDouble(), long.toDouble(), destLatLong.latitude, destLatLong.longitude)*/
    }

    override fun onClick(v: View?) {
        //setRatingDefault()
        /* when (v?.id) {
             R.id.tvRate1 -> {
                 rating = 1.00
                 selectedTextView = tvRate1
                 setSelectedViewRating(R.drawable.ic_1)
             }
             R.id.tvRate2 -> {
                 rating = 2.00
                 selectedTextView = tvRate2
                 setSelectedViewRating(R.drawable.ic_2)

             }
             R.id.tvRate3 -> {
                 rating = 3.00
                 selectedTextView = tvRate3
                 setSelectedViewRating(R.drawable.ic_3)

             }
             R.id.tvRate4 -> {
                 rating = 4.00
                 selectedTextView = tvRate4
                 setSelectedViewRating(R.drawable.ic_4)

             }
             R.id.tvRate5 -> {
                 rating = 5.00
                 selectedTextView = tvRate5
                 setSelectedViewRating(R.drawable.ic_5)

             }
         }*/
    }
/*
    private fun setSelectedViewRating(@DrawableRes drawable: Int) {
        selectedTextView?.setCompoundDrawablesWithIntrinsicBounds(0, drawable, 0, 0)
        selectedTextView?.setTextColor(ContextCompat.getColor(tvNameRate.context, R.color.colorRate))

    }

    private fun setRatingDefault() {
        tvRate1.setTextColor(ContextCompat.getColor(tvRate1.context, R.color.white))
        tvRate2.setTextColor(ContextCompat.getColor(tvRate2.context, R.color.white))
        tvRate3.setTextColor(ContextCompat.getColor(tvRate3.context, R.color.white))
        tvRate4.setTextColor(ContextCompat.getColor(tvRate4.context, R.color.white))
        tvRate5.setTextColor(ContextCompat.getColor(tvRate5.context, R.color.white))
        tvRate1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_1off, 0, 0)
        tvRate2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_2off, 0, 0)
        tvRate3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_3off, 0, 0)
        tvRate4.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_4off, 0, 0)
        tvRate5.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_5off, 0, 0)
    }

    private fun setRateListeners() {
        etComments.setText("")
        tvRate1.setOnClickListener(this)
        tvRate2.setOnClickListener(this)
        tvRate3.setOnClickListener(this)
        tvRate4.setOnClickListener(this)
        tvRate5.setOnClickListener(this)
        tvSubmitRate.setOnClickListener {
            val comments = etComments.text.toString().trim()
            if (selectedTextView != null) {
                if (CheckNetworkConnection.isOnline(activity)) {
                    presenter.rateOrder(rating, comments, orderDetail?.order_id?.toLong() ?: 0)
                }
            } else {
                Toast.makeText(activity, getString(R.string.select_rating), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onRateApiSuccess() {
        onRequestStatusChange?.onRated(orderDetail?.order_id)
    }


    private fun setInvoiceListeners() {
        tvNextInvoice.setOnClickListener {
            orderDetail?.order_status = OrderStatus.SERVICE_INVOICED
            showFragmentAsPerStatus(orderDetail)
        }
    }


    private fun setRatingData() {
        tvNameRate.text = orderDetail?.user?.name
        Glide.with(imageViewRate.context).setDefaultRequestOptions(RequestOptions().circleCrop().placeholder(R.drawable.ic_bg_menu)).load(orderDetail?.user?.profile_pic_url).into(imageViewRate)
    }

    private fun setInvoiceData() {
        val services = resources.getStringArray(R.array.services_list)
        val profile = SharedPrefs.with(activity).getObject(PROFILE, AppDetail::class.java)
        tvServiceNameInvoice.text = when (profile?.category_id) {
            CategoryId.GAS -> {
                tvQuantityInvoice.text = """${orderDetail?.brand?.name} × ${orderDetail?.payment?.productQuantity}"""
                getString(R.string.gas)
            }
            CategoryId.MINERAL_WATER -> {
                tvQuantityInvoice.text = """${orderDetail?.brand?.name} × ${orderDetail?.payment?.productQuantity}"""
                orderDetail?.brand?.brand_name
            }
            CategoryId.WATER_TANKER -> {
                tvQuantityInvoice.text = """${orderDetail?.brand?.name} × ${orderDetail?.payment?.productQuantity}"""
                getString(R.string.water_tanker)
            }
            CategoryId.FREIGHT -> {
                val loginData = SharedPrefs.with(activity).getObject(SERVICES, LoginModel::class.java)
                val dataCategory = loginData?.services?.find { it.category_id == profile.category_id }
                val dataSubCat = dataCategory?.brands?.find { it.category_brand_id == profile.category_brand_id }
                val brandName = dataSubCat?.name ?: ""
                tvQuantityInvoice.text = orderDetail?.brand?.name
                brandName
            }
            else -> {
                getString(R.string.default_category_name)
            }
        }
        tvPriceInvoice.text = "${getFormattedPrice(orderDetail?.payment?.initialCharge)} ${getString(com.buraq24.driver.R.string.currency)}" // base fair
        tvTexInvoice.text = "${getFormattedPrice(0.0)} ${getString(com.buraq24.driver.R.string.currency)}"
        tvBuraqPercentage.text = "${getFormattedPrice(orderDetail?.payment?.adminCharge)} ${getString(com.buraq24.driver.R.string.currency)}"
        tvGstInvoice.text = "${getFormattedPrice(0.0)} ${getString(com.buraq24.driver.R.string.currency)}"
        tvTotalInvoice.text = "${getFinalCharge(orderDetail, 1)} ${getString(com.buraq24.driver.R.string.currency)}"
    }

    private fun setStartListeners() {
        tvStart.setOnClickListener {

            val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                    0.0f)
            val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
            val latlng = LatLng(lat.toDouble(), long.toDouble())
            if (CheckNetworkConnection.isOnline(activity)) {
                if (orderDetail?.category_id == 4 || orderDetail?.category_id == 5) {
                    presenter.startRequest(latlng, orderDetail?.order_id ?: 0)
                } else {
                    presenter.startRequest(latlng, orderDetail?.order_id?.toLong() ?: 0)
                }
            }
        }
        tvCancelStart.setOnClickListener {
            val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                    0.0f)
            val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
            val latlng = LatLng(lat.toDouble(), long.toDouble())
            showCancellationDialog(latlng)
        }

        imageViewCallAccept.setOnClickListener {
            val phone = orderDetail?.user?.phone_number?.toString() ?: ""
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }

    }

    private fun setEndListeners() {
        val lat = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LAT,
                0.0f)
        val long = SharedPrefs.with(activity).getFloat(PREF_CURRENT_LNG, 0.0f)
        val latlng = LatLng(lat.toDouble(), long.toDouble())
        tvDropAtAddressStart.setOnClickListener {
            if (CheckNetworkConnection.isOnline(activity)) {
                when {
                    orderDetail?.organisation_coupon_user_id != 0 -> {
                        val intent = Intent(activity, BookingDetailsActivity::class.java)
                        intent.putExtra(ORDER, Gson().toJson(orderDetail))
                        startActivityForResult(intent, 1002)
                    }
                    orderDetail?.my_turn == "0" -> presenter.startOtherRide(orderDetail?.order_id?.toLong()
                            ?: 0L)
                    else -> {
                        dialogDropoffConfirmation = AlertDialogUtil.getInstance()?.createOkCancelDialog(activity,
                                R.string.dropoff_confirmation, R.string.dropoff_comfirmation_msg, R.string.yes, R.string.no, true, object : AlertDialogUtil.OnOkCancelDialogListener {
                            override fun onOkButtonClicked() {
                                presenter.dropAtAddressRequest(latlng, orderDetail?.order_id
                                        ?: 0)
                            }

                            override fun onCancelButtonClicked() {

                            }

                        })
                        try {
                            dialogDropoffConfirmation?.show()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }
            }
        }

        ivDirections.setOnClickListener {
            val gmmIntentUri = Uri.parse("google.navigation:q=${orderDetail?.dropoff_latitude},${orderDetail?.dropoff_longitude}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            val activityExists = activity?.packageManager?.let { packageManager ->
                mapIntent.resolveActivityInfo(packageManager, 0) != null
            }
            if (activityExists == true) {
                startActivity(mapIntent)
            } else {
                view?.showSnack(getString(R.string.google_maps_not_installed))
            }
        }

        imageViewCallStart.setOnClickListener {
            val phone = orderDetail?.user?.phone_number?.toString() ?: ""
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))
        }

        imageViewMin.setOnClickListener {
            if (!isMinimized) {
                llEstimatedDataStart.visibility = View.GONE
                tvDropAtAddressStart.visibility = View.GONE
                isMinimized = true
                imageViewMin.rotation = 180f
                onRequestStatusChange?.swipeStartStop(true)

            } else {
                llEstimatedDataStart.visibility = View.VISIBLE
                tvDropAtAddressStart.visibility = View.VISIBLE
                isMinimized = false
                imageViewMin.rotation = 0f
                onRequestStatusChange?.swipeStartStop(false)

            }
        }
    }

    private fun showCancellationDialog(latlng: LatLng) {
        dialog = context?.let { Dialog(it) }
        dialog?.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
            setContentView(R.layout.dialog_cancel_reason)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val etMessage = findViewById<EditText>(R.id.etMessage)
            val tvSubmit = findViewById<TextView>(R.id.tvSubmit)
            tvSubmit.setOnClickListener {
                if (etMessage.text.toString().trim().isNotEmpty()) {
                    val reason = etMessage.text.toString().trim()
                    if (CheckNetworkConnection.isOnline(activity)) {
                        if (CheckNetworkConnection.isOnline(activity)) {
                            presenter.rejectRequest(latlng, orderDetail?.order_id ?: 0, reason)
                        }
                        dismiss()
                    } else {
                        CheckNetworkConnection.showNetworkError(tvCancelStart)
                    }
                } else {
                    Toast.makeText(activity, getString(R.string.cancellation_reason_validation_text), Toast.LENGTH_SHORT).show()
                }
            }

            show()
            window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }


    override fun onEndApiSuccess() {
        orderDetail?.order_status = OrderStatus.SERVICE_COMPLETE
        showFragmentAsPerStatus(orderDetail)
    }

    override fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if ((routeArray?.length() ?: 0) > 0) {
            val routes = routeArray.getJSONObject(0)
            if ((routes.get("legs") as JSONArray).get(0) != null) {
                val estimatedDistance = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("distance")
                        as JSONObject).get("value") as Int
                SharedPrefs.with(activity).save(PREF_DISTANCE, estimatedDistance.div(1000))
                orderDetail?.order_distance = estimatedDistance.div(1000).toString()
                distance = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("text") as String
                time = (((routes.get("legs") as JSONArray).get(0) as JSONObject).get("duration") as JSONObject).get("text") as String
                setDistanceAndTime()
            }
        }
    }

    override fun onAcceptApiSuccess(order: Order?) {
        if (orderDetail?.future == "1") {
            onRejectApiSuccess()
        } else {
            val profileData = SharedPrefs.get().getObject(PROFILE, AppDetail::class.java)
            Logger.e("order_driver_id", order?.driver?.user_detail_id.toString())
            Logger.e("my_driver_id", order?.driver?.user_detail_id.toString())
            if (order?.driver_user_detail_id == profileData.user_detail_id) {
//                orderDetail?.order_status = OrderStatus.CONFIRMED
//                orderDetail?.payment = order?.payment
                orderDetail = order
                showFragmentAsPerStatus(orderDetail)
            } else {
                showToast(R.string.request_accepted_by_other_driver)
                onRequestStatusChange?.refreshOrders(order)
            }
        }
    }


    override fun onRejectApiSuccess() {
        onRequestStatusChange?.requestRejected(orderDetail?.order_id)
    }

    override fun onStartApiSuccess() {
        presenter.startOtherRide(orderDetail?.order_id ?: 0L)
    }

    override fun showLoader(isLoading: Boolean) {
        progressDialog?.show(isLoading)
    }

    override fun apiFailure() {
        view?.showSWWerror()
    }

    override fun handleApiError(code: Int?, error: String?) {
        view?.showSnack(error.toString())
        timer?.cancel()
        activity?.supportFragmentManager?.popBackStackImmediate()
    }

    override fun handleOrderError(order: Order?) {
        when (order?.order_status) {
            OrderStatus.CUSTOMER_CANCEL -> {
                showToast(R.string.order_already_cancelled_by_customer)
                onRequestStatusChange?.refreshOrders(order)

            }
            OrderStatus.DRIVER_CANCELLED -> {
                showToast(R.string.order_already_cancelled_by_driver)
                onRequestStatusChange?.refreshOrders(order)
            }
            OrderStatus.SERVICE_REJECT -> {
                showToast(R.string.order_already_rejected_by_driver)
                onRequestStatusChange?.refreshOrders(order)
            }
            OrderStatus.SERVICE_TIMEOUT, OrderStatus.DRIVER_SCHEDULED_TIMEOUT -> {
                showToast(R.string.request_timed_out)
                onRequestStatusChange?.refreshOrders(order)
            }
            else -> {
                val profileData = SharedPrefs.get().getObject(PROFILE, AppDetail::class.java)
                if (order?.driver_user_detail_id != profileData.user_detail_id) {
                    showToast(R.string.request_accepted_by_other_driver)
                } else {
                    showToast(R.string.invalid_order)
                }
                onRequestStatusChange?.refreshOrders(order)
            }
        }
    }

    private fun showToast(@StringRes msg: Int?) {
        Toast.makeText(activity, msg ?: 0, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        presenter.detachView()
        activity?.unregisterReceiver(receiver)
        progressDialog?.show(false)
        super.onDestroyView()
    }*/

    interface OnRequestStatusChange {
        fun requestStart(order: Wash?)
        fun requestConfirmed(orderDetail: Wash?)
        fun requestRejected(orderId: Long?)
        fun onServiceEnd(orderId: Long?)
        fun onRated(orderId: Long?)
        fun changeTurnSuccess(order: Wash?)
        fun refreshOrders(order: Wash?)
        fun swipeStartStop(isStop: Boolean)

    }
}