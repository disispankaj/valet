package com.codebrew.mrsteam2.ui.customer.booking.paymenttype

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.CheckOffer
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckOfferViewModel : ViewModel() {
    val checkOffer by lazy { SingleLiveEvent<Resource<CheckOffer>>() }
    fun checkOffer(hashMap: HashMap<String, Any>) {
        checkOffer.value = Resource.loading()


        RetrofitClient.getApi().checkOffer(hashMap)
                .enqueue(object : Callback<ApiResponse<CheckOffer>> {
                    override fun onFailure(call: Call<ApiResponse<CheckOffer>>, throwable: Throwable) {
                        checkOffer.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<CheckOffer>>,
                                            response: Response<ApiResponse<CheckOffer>>) {
                        if (response.isSuccessful) {
                            checkOffer.value = Resource.success(response.body()?.data)
                        } else {
                            checkOffer.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }
}