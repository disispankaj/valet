package com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.AddCarRvAdapter
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.item_rv_add_car_color.view.*
import kotlinx.android.synthetic.main.item_rv_add_car_vehicletype.view.*

class AddCarAdapter(
        private val adapterList: ArrayList<AddCarRvAdapter>,
        private val activity: AddCarActivity, private val color: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (color) {
            true -> {
                val v1 = View.inflate(parent.context, R.layout.item_rv_add_car_color, null)
                ViewHolderColor(v1)
            }
            false -> {
                val v1 = View.inflate(parent.context, R.layout.item_rv_add_car_vehicletype, null)
                ViewHolderVehicleType(v1)
            }
        }
    }

    override fun getItemCount(): Int {
        return adapterList.size

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (color) {
            true -> {
                (holder as ViewHolderColor).bind(adapterList[position])
            }
            false -> {
                (holder as ViewHolderVehicleType).bind(adapterList[position])
            }
        }

    }

    inner class ViewHolderColor(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(d1: AddCarRvAdapter) = with(itemView) {
            ivCarColor.setImageResource(context.getVehicleColor(d1.type ?: 0))
            ivCheckIcon.setBackgroundResource(R.drawable.ic_check_white_24dp)
            if(adapterPosition  == 5)
                ivCheckIcon.setBackgroundResource(R.drawable.ic_check_black_24dp)

            if (d1.isSelected)
                ivCheckIcon.visible()
            else
                ivCheckIcon.gone()

            ivCarColor.setOnClickListener {
                activity.colorIndex = adapterList[adapterPosition].type ?: 0
                for (item in adapterList) {
                    adapterList[adapterList.indexOf(item)].isSelected = (adapterList.indexOf(item) == adapterPosition)
                }
                notifyDataSetChanged()
            }


        }
    }

    inner class ViewHolderVehicleType(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(d1: AddCarRvAdapter) = with(itemView) {
            tvVehicleType.text = context.getVehicleType(d1.type.toString())
            when (d1.type) {
                VehicleType.SMALL -> {
                    ivVehicleType.setImageResource(R.drawable.ic_addcar_hatchback)
                }
                VehicleType.SEDAN -> {
                    ivVehicleType.setImageResource(R.drawable.ic_addcar_sedan)
                }
                VehicleType.MEDIUM -> {
                    ivVehicleType.setImageResource(R.drawable.ic_addcar_mpv)
                }
                VehicleType.LARGE_TRUCK -> {
                    ivVehicleType.setImageResource(R.drawable.ic_addcar_suv)
                }
            }

            if (d1.isSelected) {
                ivVehicleType.setBackgroundResource(R.drawable.back_vehicle_type_on)
                //ivVehicleType.setColorFilter(ContextCompat.getColor(context, R.color.white))
                tvVehicleType.setTextColor(ContextCompat.getColor(context, R.color.mrSteamMain))
            } else {
                ivVehicleType.setBackgroundResource(R.drawable.back_vehicle_type_off)
                //ivVehicleType.setColorFilter(R.color.add_car_back_off)
                tvVehicleType.setTextColor(ContextCompat.getColor(context, R.color.page_heading))
            }


            ivVehicleType.setOnClickListener {
                activity.vehicleIndex = adapterList[adapterPosition].type ?: 0
                for (item in adapterList) {
                    adapterList[adapterList.indexOf(item)].isSelected = (adapterList.indexOf(item) == adapterPosition)
                }
                notifyDataSetChanged()
            }

        }

    }


}




