package com.codebrew.mrsteam2.ui.customer.booking.paymenttype


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.confirmbooking.ConfirmBookingFragment
import com.codebrew.mrsteam2.ui.customer.booking.payments.PaymentsActivity
import com.codebrew.mrsteam2.ui.customer.paymentModule.CardActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.fragment_payment_type.*

class PaymentTypeFragment : Fragment(), View.OnClickListener {

    private val RQ_PAYMENT_TYPE = 400

    private var serviceRequest: ServiceRequestModel? = null

    private var selectedPaymentMethod = PaymentType.CASH

    private lateinit var viewModel: CheckOfferViewModel

    private var tokenId: Int? = null

    private var paymentType = 0

    private var bookingActivity: BookingActivity? = null

    var tokenConnekta: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_payment_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //presenter.attachView(this)

        initialise()
        setData()
        setListeners()
        liveData()
    }

    private fun initialise() {
        bookingActivity = activity as BookingActivity
        serviceRequest = bookingActivity?.serviceRequestModel
        viewModel = ViewModelProviders.of(this)[CheckOfferViewModel::class.java]

        paymentType = serviceRequest?.paymentType ?: 0
        when (paymentType) {
            PaymentsType.CASH -> rbCash.isChecked = true
            PaymentsType.CREDIT -> rbCredit.isChecked = true
            PaymentsType.OFFER -> rbOffer.isChecked = true
        }

        if (serviceRequest?.checkOffer == null && isConnectedToInternet(activity, true)) {
            val hashMap = HashMap<String, Any>()
            hashMap["sub_service_id"] = serviceRequest?.subService ?: 0
            viewModel.checkOffer(hashMap)
        } else {
            if (serviceRequest?.checkOffer?.is_having_offer == true)
                rbOffer.visible()
            else
                rbOffer.gone()
        }
    }


    private fun setData() {
        val serviceRequest = bookingActivity?.serviceRequestModel ?: ServiceRequestModel()

        selectedPaymentMethod = if (serviceRequest.payment_type?.isEmpty() == true) {
            PrefsManager.get().getString(PAYMENT_TYPE, PaymentType.CASH) ?: PaymentType.CASH
        } else {
            serviceRequest.payment_type ?: PaymentType.CASH
        }

        if (serviceRequest.future == ServiceRequestModel.BOOK_NOW) {
            btnNext.setText(R.string.next)
        } else {
            btnNext.text = "${getString(R.string.schedule)}\n${serviceRequest.order_timings_text}"
        }

    }


    private fun setListeners() {
        btnNext.setOnClickListener(this)
        rbCash.setOnClickListener(this)
        rbCredit.setOnClickListener(this)

        rgPaymentOption.setOnCheckedChangeListener { _, checkedId ->
            paymentType = when (checkedId) {
                R.id.rbOffer -> PaymentsType.OFFER
                R.id.rbCash -> PaymentsType.CASH
                R.id.rbCredit -> PaymentsType.CREDIT
                else -> 0
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnNext -> {
                if (paymentType == 0)
                    rbCash.showSnack(R.string.payment_option)
                else {

                    if (paymentType == PaymentsType.CREDIT && tokenConnekta.equals("")) {
                        rbCredit.showSnack(R.string.add_card_first)
                    } else {
                        serviceRequest?.paymentType = paymentType
                        serviceRequest?.tokenConnekta = tokenConnekta
                        fragmentManager?.beginTransaction()?.replace(R.id.container, ConfirmBookingFragment())
                            ?.addToBackStack("backstack")?.commit()
                    }


                }
            }

            R.id.rbCredit -> {
                startActivityForResult(Intent(activity, CardActivity::class.java), 120)
            }
        }
    }


    private fun liveData() {
        viewModel.checkOffer.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    bookingActivity?.rlLoader?.gone()

                    serviceRequest?.checkOffer = resources.data
                    if (resources.data?.is_having_offer == true)
                        rbOffer.visible()
                    else
                        rbOffer.gone()

                }
                Status.ERROR -> {
                    bookingActivity?.rlLoader?.gone()
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    bookingActivity?.rlLoader?.visible()
                }
            }
        })
    }

    private fun bookingApiCall() {
        if (isConnectedToInternet(activity, true)) {
            val requestModel = bookingActivity?.serviceRequestModel ?: ServiceRequestModel()
            if (requestModel.category_id != CategoryId.GAS && requestModel.category_id != CategoryId.FREIGHT
                && requestModel.category_id != CategoryId.WATER_TANKER && requestModel.category_id != CategoryId.MINERAL_WATER
            ) {
                val dialog = CustomDialogClass(activity, R.style.Custom_Dialog)
                val window = dialog.window
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                dialog.show()
            } else {
                if (requestModel.future == ServiceRequestModel.BOOK_NOW) {
                    requestModel.order_timings = getCurentDateStringUtc()
                }
//            requestModel.order_timings = getCurentDateStringUtc()
                requestModel.payment_type = selectedPaymentMethod
                requestModel.distance = 50000
                if (selectedPaymentMethod == PaymentType.E_TOKEN) {
                    requestModel.organisation_coupon_user_id = tokenId
                } else {
                    requestModel.organisation_coupon_user_id = null
                }
                //presenter.requestServiceApiCall(requestModel)
            }
        }
    }

    private fun selectPaymentMethod() {
        val intent = Intent(activity, PaymentsActivity::class.java)
        intent.putExtra(CATEGORY_ID, serviceRequest?.category_id)
        intent.putExtra(CATEGORY_BRAND_ID, serviceRequest?.category_brand_id)
        intent.putExtra(CATEGORY_PRODUCT_ID, serviceRequest?.category_brand_product_id)
        intent.putExtra(PAYMENT_TYPE, selectedPaymentMethod)
        startActivityForResult(intent, RQ_PAYMENT_TYPE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_PAYMENT_TYPE && resultCode == Activity.RESULT_OK && data != null) {
            selectedPaymentMethod = data.getStringExtra(PAYMENT_TYPE)
            if (selectedPaymentMethod == PaymentType.E_TOKEN) {
                tokenId = data.getIntExtra(TOKEN_ID, 0)
            }
        } else if (requestCode == 120  && data != null) {
            println("CONNEKTA TOKEN " + data.getStringExtra("token"))
            tokenConnekta = data.getStringExtra("token") ?: ""
        }
    }


}
