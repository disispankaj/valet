package com.codebrew.mrsteam2.ui.customer.booking.bookingtype


import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.ui.customer.booking.BookingActivity
import com.codebrew.mrsteam2.ui.customer.booking.dropofflocation.DropOffLocationFragment
import com.codebrew.mrsteam2.ui.customer.booking.paymenttype.PaymentTypeFragment
import com.codebrew.mrsteam2.ui.customer.booking.schedule.ScheduleFragment
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.hideKeyboard
import com.codebrew.mrsteam2.utils.showSnack
import kotlinx.android.synthetic.main.fragment_booking_type.*

/**
 * Fragment to get the details of the order for the service: Gas, Mineral Water and Water Tanker
 * after getting the location and addresses from [DropOffLocationFragment]
 * */
class BookingTypeFragment : Fragment() {


    private lateinit var serviceRequest: ServiceRequestModel

    private var selectedPosition = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(activity!!)
        super.onCreate(savedInstanceState)
        serviceRequest = (activity as BookingActivity).serviceRequestModel

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            /* Enter and exit animations for this fragment */
            enterTransition = Explode()
            exitTransition = Explode()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setListeners()

        selectedPosition = serviceRequest.future ?: -1

        if (selectedPosition == ServiceRequestModel.BOOK_NOW) {
            cbInstant.isChecked = true
            cbSchedule.isChecked = false
        } else if (selectedPosition == ServiceRequestModel.SCHEDULE) {
            cbInstant.isChecked = false
            cbSchedule.isChecked = true
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDestroyView() {
        super.onDestroyView()

    }

    /* gets the selected position of the category brand*/
    private fun getSelectedPosition(categoryBrandId: Int): Int {

        return 0
    }

    private fun setListeners() {
        cbInstant.setOnClickListener {
            selectedPosition = ServiceRequestModel.BOOK_NOW
            cbInstant.hideKeyboard()
            cbInstant.isChecked = true
            cbSchedule.isChecked = false
        }
        cbSchedule.setOnClickListener {
            selectedPosition = ServiceRequestModel.SCHEDULE
            cbSchedule.hideKeyboard()
            cbSchedule.isChecked = true
            cbInstant.isChecked = false

            serviceRequest.future = selectedPosition
            fragmentManager?.beginTransaction()?.replace(R.id.container, ScheduleFragment())
                    ?.addToBackStack("backstack")?.commit()
        }

        btnNext.setOnClickListener {
            if (selectedPosition == -1)
                btnNext.showSnack(R.string.select_booking_type)
            else {
                if (selectedPosition == ServiceRequestModel.BOOK_NOW)
                    fragmentManager?.beginTransaction()?.replace(R.id.container, PaymentTypeFragment())
                            ?.addToBackStack("backstack")?.commit()
                else if (selectedPosition == ServiceRequestModel.SCHEDULE)
                    fragmentManager?.beginTransaction()?.replace(R.id.container, ScheduleFragment())
                            ?.addToBackStack("backstack")?.commit()

                serviceRequest.future = selectedPosition
            }
        }

    }
}
