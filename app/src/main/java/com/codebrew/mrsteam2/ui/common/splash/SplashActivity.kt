package com.codebrew.mrsteam2.ui.common.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.ui.common.welcome.WelcomeActivity
import com.codebrew.mrsteam2.utils.getAccessToken


class SplashActivity : AppCompatActivity() {
    private var mDelayHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mDelayHandler = Handler()
        mDelayHandler?.postDelayed(mRunnable, 3000)
    }


    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            if (getAccessToken().isNotEmpty())
                startActivity(Intent(this, MainActivity::class.java))
            else
                startActivity(Intent(this, WelcomeActivity::class.java))
            finish()
        }
    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler?.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }
}
