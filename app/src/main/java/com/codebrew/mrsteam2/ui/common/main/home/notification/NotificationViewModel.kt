package com.codebrew.mrsteam2.ui.common.main.home.notification

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.notification.NotificationDataItem
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationViewModel : ViewModel() {

    val notificationRes by lazy { SingleLiveEvent<Resource<ArrayList<NotificationDataItem>>>() }

    fun getNotification() {

        notificationRes.value = Resource.loading()

        RetrofitClient.getApi()
            .notification()
            .enqueue(object : Callback<ApiResponse<ArrayList<NotificationDataItem>>> {
                override fun onFailure(call: Call<ApiResponse<ArrayList<NotificationDataItem>>>, t: Throwable) {
                    notificationRes.value = Resource.error(ApiUtils.failure(t))
                }

                override fun onResponse(
                    call: Call<ApiResponse<ArrayList<NotificationDataItem>>>,
                    response: Response<ApiResponse<ArrayList<NotificationDataItem>>>
                ) {
                    if (response.isSuccessful) {
                        notificationRes.value = Resource.success(response.body()?.data)
                    } else {
                        notificationRes.value = Resource.error(
                            ApiUtils.getError(
                                response.code(),
                                response.errorBody()?.string()
                            )
                        )
                    }
                }
            })
    }


    fun readNotification(id: String) {


        RetrofitClient.getApi()
            .readNotification(id)
            .enqueue(object : Callback<ApiResponse<Any>> {


                override fun onFailure(call: Call<ApiResponse<Any>>, t: Throwable) {
                    notificationRes.value = Resource.error(ApiUtils.failure(t))
                }

                override fun onResponse(
                    call: Call<ApiResponse<Any>>,
                    response: Response<ApiResponse<Any>>
                ) {
                    if (response.isSuccessful) {

                    } else {
                        notificationRes.value = Resource.error(
                            ApiUtils.getError(
                                response.code(),
                                response.errorBody()?.string()
                            )
                        )
                    }
                }
            })
    }

}