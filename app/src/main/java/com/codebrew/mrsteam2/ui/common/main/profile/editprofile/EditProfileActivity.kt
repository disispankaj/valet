package com.codebrew.mrsteam2.ui.common.main.profile.editprofile


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.EditProfileRequest
import com.codebrew.mrsteam2.network.responsemodels.ImageModel
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_edit_proflie.*
import okhttp3.MediaType
import okhttp3.RequestBody
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import java.io.File


class EditProfileActivity : AppCompatActivity() {
    private var imageStatus: Boolean = false
    private var genderIndex: Int = 1
    private var fileToUpload: File? = null
    private lateinit var viewModelUpload: ImageUploadViewModel
    private lateinit var viewModel: EditProfileViewModel
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_proflie)

        dataSetup()
        listeners()
        initialise()
        liveData()

    }

    private fun dataSetup() {
        val data = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)
        val str = "+${data?.country_code.toString()} ${data?.phone.toString()}"
      //  tvPhone.text = str
        etName.setText(data?.name, TextView.BufferType.EDITABLE)
        etEmail.setText(data?.email, TextView.BufferType.EDITABLE)
        tvPhone.setText(data?.phone.toString(), TextView.BufferType.EDITABLE)

        loadImage(this, ivProfilePicture, data?.image_url?.thumbnail
                ?: "", data?.image_url?.original ?: "")

        when (data?.gender) {
            UserGender.MALE -> radioBtnMale.isChecked = true
            UserGender.FEMALE -> radioBtnFemale.isChecked = true
            UserGender.OTHER -> radioBtnOthers.isChecked = true
        }
    }

    private fun listeners() {
        tbrEditProfile.setNavigationOnClickListener {
            onBackPressed()
        }
        ivProfilePicture.setOnClickListener {
            getStorageWithPermissionCheck()
        }
        radioBtnGroupGender.setOnCheckedChangeListener { _, checkedId ->
            genderIndex = when (checkedId) {
                R.id.radioBtnMale -> UserGender.MALE
                R.id.radioBtnFemale -> UserGender.FEMALE
                R.id.radioBtnOthers -> UserGender.OTHER
                else -> UserGender.MALE
            }
        }


        tvSave.setOnClickListener {
            imageStatus = true
            validation()
        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[EditProfileViewModel::class.java]
        viewModelUpload = ViewModelProviders.of(this)[ImageUploadViewModel::class.java]
        progressDialog = ProgressDialog(this)
    }

    private fun editProfileApiHit() {
        val user = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)

        val image = ImageModel()
        image.original = user?.original
        image.thumbnail = user?.thumbnail

        val editReq = EditProfileRequest(
                name = etName.text.toString(),
                country_code = user?.country_code,
                gender = genderIndex,
                phone = user?.phone.toString(),
                email = etEmail.text?.trim().toString(),
                image = image
        )
        if (isConnectedToInternet(this, true))
            viewModel.editProfile(editReq)


    }

    private fun liveData() {
        viewModel.editProfileRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    PrefsManager.get().save(SP_USER_DATA, resources.data)
                    progressDialog.setLoading(false)
                    setResult(Activity.RESULT_OK)
                    if (imageStatus) {
                        finish()
                    } else {
                        loadImage(this, ivProfilePicture, resources.data?.image_url?.thumbnail
                                ?: "", resources.data?.image_url?.original ?: "")
                    }
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModelUpload.uploadFile.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    loadImage(this, ivProfilePicture, resources.data?.thumbnail
                            ?: "", resources.data?.original ?: "")

                    val user = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)

                    imageStatus = false

                    val image = ImageModel()
                    image.original = resources.data?.original
                    image.thumbnail = resources.data?.thumbnail
                    val editReq = EditProfileRequest(
                            name = etName.text.toString(),
                            country_code = user?.country_code,
                            gender = genderIndex,
                            phone = user?.phone.toString(),
                            email = etEmail.text?.trim().toString(),
                            image = image
                    )
                    if (isConnectedToInternet(this, true))
                        viewModel.editProfile(editReq)

                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

    private fun validation() {
        tilName.error = null
        tilEmail.error = null
        return when {
            etName.text.toString().isEmpty() -> {
                invalidStringInput(etName, tilName, getString(R.string.error_empty_username))

            }
            /*!NAME_FORMAT.matcher(etName.text.toString()).matches() -> {
                invalidStringInput(etName, tilName, getString(R.string.str_username_error))

            }*/
            etEmail.text.toString().isNotEmpty() && !EMAIL_FORMAT.matcher(etEmail.text.toString()).matches() -> {
                invalidStringInput(etEmail, tilEmail, getString(R.string.str_email_error))
            }
            else -> editProfileApiHit()
        }
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun getStorage() {
        ImageUtils.displayImagePicker(this)
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showLocationRationale(request: PermissionRequest) {
        PermissionUtils.showRationalDialog(ivProfilePicture.context, R.string.permission_required_to_select_image, request)
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onNeverAskAgainRationale() {
        PermissionUtils.showAppSettingsDialog(ivProfilePicture.context,
                R.string.permission_required_to_select_image)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageUtils.REQ_CODE_CAMERA_PICTURE -> {
                    fileToUpload = let { ImageUtils.getFile(it) }
                    addImage()
                }
                ImageUtils.REQ_CODE_GALLERY_PICTURE -> {
                    fileToUpload = data?.data?.let {
                        ImageUtils.getImagePathFromGallery(this, it)
                    }
                    addImage()
                }
            }
        }
    }

    private fun addImage() {

        if (getFileSizeLess5MB(fileToUpload)) {
            if (isConnectedToInternet(this, true)) {
                val map = HashMap<String, RequestBody>()
                map["type"] =
                        RequestBody.create(MediaType.parse("text/plain"), DocType.IMAGE)
                if (fileToUpload != null && fileToUpload?.exists() == true) {
                    val body: RequestBody =
                            RequestBody.create(MediaType.parse("image/jpeg"), fileToUpload)

                    map["image\"; fileName=\"" + fileToUpload?.name] = body
                }

                viewModelUpload.uploadFile(map)
            }
        } else {
            fileToUpload = null
            ivProfilePicture.showSnack(getString(R.string.file_size_more))
        }
    }
}

