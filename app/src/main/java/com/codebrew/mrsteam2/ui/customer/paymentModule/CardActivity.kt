package com.codebrew.mrsteam2.ui.customer.paymentModule

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.braintreepayments.cardform.OnCardFormSubmitListener
import com.braintreepayments.cardform.utils.CardType
import com.braintreepayments.cardform.view.CardEditText
import com.braintreepayments.cardform.view.CardForm
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.utils.MrSteamApp
import io.conekta.conektasdk.Card
import io.conekta.conektasdk.Conekta
import io.conekta.conektasdk.Token
import kotlinx.android.synthetic.main.activity_card.*
import org.json.JSONObject


class CardActivity : AppCompatActivity(), OnCardFormSubmitListener,
    CardEditText.OnCardTypeChangedListener {
    private lateinit var progressDialog: ProgressDialog
    val SUPPORTED_CARD_TYPES = arrayOf(
        CardType.VISA,
        CardType.MASTERCARD,
        CardType.DISCOVER,
        CardType.AMEX,
        CardType.DINERS_CLUB,
        CardType.JCB,
        CardType.MAESTRO,
        CardType.UNIONPAY,
        CardType.HIPER,
        CardType.HIPERCARD)


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)

        tbrCarddetail.setNavigationOnClickListener {
            onBackPressed()
        }

        supported_card_types.setSupportedCardTypes(*SUPPORTED_CARD_TYPES)

        card_form.cardRequired(true)
            .maskCardNumber(true)
            .maskCvv(true)
            .expirationRequired(true)
            .cvvRequired(true)
            .postalCodeRequired(false)
            .mobileNumberRequired(false)
            .saveCardCheckBoxChecked(true)
            .saveCardCheckBoxVisible(false)
            .cardholderName(CardForm.FIELD_REQUIRED)
            .actionLabel(getString(R.string.bt_save_card_checkbox_name))
            .setup(this)
        card_form.setOnCardFormSubmitListener(this)
        card_form.setOnCardTypeChangedListener(this)

        // Warning: this is for development purposes only and should never be done outside of this example app.
        // Failure to set FLAG_SECURE exposes your app to screenshots allowing other apps to steal card information.
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
    }

    override fun onCardTypeChanged(cardType: CardType?) {
        if (cardType == CardType.EMPTY) {
            supported_card_types.setSupportedCardTypes(*SUPPORTED_CARD_TYPES)
        } else {
            supported_card_types.setSelected(cardType)
        }

    }

    override fun onCardFormSubmit() {
        if (card_form.isValid()) {

            progressDialog = ProgressDialog(this)
            progressDialog.setLoading(true)

            var cardNumber = card_form.cardNumber
            var cardHolderName = card_form.cardholderName
            var cardCvv = card_form.cvv
            var cardExpMonth = card_form.expirationMonth
            var cardExpYear = card_form.expirationYear

            //Toast.makeText(this, R.string.valid, Toast.LENGTH_SHORT).show()
            //Conekta.setPublicKey("key_KhmdzgdHzGCZccXhxdQsrsQ") //client_key
            Conekta.setPublicKey("key_CQken84DTCaJBkMqyBxESwA") //sunil_key
            Conekta.setApiVersion("0.3.0")
            Conekta.collectDevice(this)
            val card = Card(
                cardHolderName,
                cardNumber,
                cardCvv,
                cardExpMonth,
                cardExpYear
            )
            val token = Token(this)
            //Listen when token is returned
            token.onCreateTokenListener { data ->
                progressDialog.setLoading(false)
                try {
                    Log.d("The token::::", data.getString("id"))
                    var intent =  Intent()
                    intent.putExtra("token",data.getString("id"))
                    setResult(120, intent)
                    finish()
                } catch (err: Exception) {
                    Toast.makeText(this,"Error: $err",Toast.LENGTH_SHORT).show()
                }

            }

            //Request for create token
            token.create(card)

        } else {
            card_form.validate()
            Toast.makeText(this, R.string.valid, Toast.LENGTH_SHORT).show()
        }

    }
}
