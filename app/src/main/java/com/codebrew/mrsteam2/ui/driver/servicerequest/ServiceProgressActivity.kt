package com.codebrew.mrsteam2.ui.driver.servicerequest

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Location
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.MrSteamApi
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.ServiceRequestModel
import com.codebrew.mrsteam2.network.responsemodels.PolylineModel
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.network.responsemodels.homeapi.ServiceDetails
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.ui.driver.detail.DetailActivity
import com.codebrew.mrsteam2.ui.driver.servicerequest.request.ServiceRequestFragment
import com.codebrew.mrsteam2.ui.driver.servicerequest.requestAfterAccept.StartRequestFragment
import com.codebrew.mrsteam2.utils.*
import com.codebrew.mrsteam2.utils.location.LocationProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_home.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

/**
 * This activity is the home screen for the user after successful login.
 * Shows the google maps.
 * Acts  as a container for whole booking process of all kinds of services using fragments.
 * Provides side navigation bar to navigate through other screens of the application.
 * Keeps server updated to the current location of the user.
 * */
class ServiceProgressActivity : AppCompatActivity(), OnMapReadyCallback {

    private var location: Location? = null

    private lateinit var progressDialog: ProgressDialog

    private lateinit var viewModel: WashDetailsViewModel

    private val ratingDrawables = listOf(
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification,
        R.drawable.notification
    )

    var srcMarker: Marker? = null

    var destMarker: Marker? = null

    var mapFragment: SupportMapFragment? = null

    var serviceDetails: ServiceDetails? = null

    var serviceRequestModel = ServiceRequestModel()

    var googleMapHome: GoogleMap? = null

    lateinit var locationProvider: LocationProvider

    private var startRequestFragment: StartRequestFragment? = null


    private var line: Polyline? = null
    private var estimatedTime: String = ""
    private var estimatedDistance: String = ""
    private var list = ArrayList<LatLng>()
    private var carMarker: Marker? = null
    private lateinit var viewModelLocation: UpdateLocationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)

        initialise()
        // drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        // setHeaderView()
        liveData()
    }

    private fun initialise() {
        getMapAsync()
        ivMarker.gone()
        progressDialog = ProgressDialog(this)
        viewModel = ViewModelProviders.of(this)[WashDetailsViewModel::class.java]
        viewModelLocation = ViewModelProviders.of(this)[UpdateLocationViewModel::class.java]
        //AppSocket.get().init(this) // Initialize socket
        FirebaseApp.initializeApp(this) // Initialise firebase app
        locationProvider = LocationProvider.CurrentLocationBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener {
            try {
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStackImmediate(
                        "backstack",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finishAffinity()
            }
            mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

            checkServiceInProgress(intent)
        })
    }

    private fun checkServiceInProgress(intent: Intent) {
        if (intent.hasExtra(ORDER_PROGRESS)) {

            val request = Gson().fromJson(intent.getStringExtra(ORDER_PROGRESS), Wash::class.java)
            moveToFragment(request)
        } else if (intent.hasExtra(DETAIL_DATA)) {
            val data = intent.getSerializableExtra(DETAIL_DATA) as Wash
            if (isConnectedToInternet(this, true))
                viewModel.washDetails(data.id ?: 0)
        }
    }

    private fun moveToFragment(request: Wash?) {

        when {
            request?.status == ServiceStatus.COMPLETED -> {
                if (request.is_reviewed == true)
                    startActivity(Intent(this, DetailActivity::class.java)
                        .putExtra(DETAIL_DATA, request)
                    )
                else {
                    startActivity(Intent(this, RateReviewActivity::class.java)
                        .putExtra(DETAIL_DATA, request))
                }

                setResult(Activity.RESULT_OK)
                finish()
            }
            request?.status == ServiceStatus.CANCLE_BY_USER -> {
                Toast.makeText(this, "Request Cancelled By user", Toast.LENGTH_LONG).show()
                finish()
            }
            else -> {
                val fragment = when {
                    request?.status == ServiceStatus.REQUEST -> ServiceRequestFragment()
                    request?.status == ServiceStatus.ACCEPT -> StartRequestFragment()
                    request?.status == ServiceStatus.JOURNEY_STARTED -> StartRequestFragment()
                    request?.status == ServiceStatus.WASHING_STARTED -> StartRequestFragment()
                    else -> ServiceRequestFragment()
                }
                val bundle = Bundle()

                if (fragment is StartRequestFragment) {
                    startRequestFragment = fragment
                }

                bundle.putString(ORDER_PROGRESS, Gson().toJson(request))
                fragment.arguments = bundle
                //if(supportFragmentManager?.isStateSaved == true) return
                supportFragmentManager.beginTransaction().add(
                    R.id.container,
                    fragment, request?.id.toString()
                ).commitAllowingStateLoss()
            }
        }
    }

    private fun liveData() {
        viewModel.washDetails.observe(this, Observer {
            it ?: return@Observer
            when (it.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    val request = it.data
                    moveToFragment(request)
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(it.error, this)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }


    override fun onResume() {
        super.onResume()
        val nMgr = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nMgr.cancelAll() // Clears all the notifications in the system notification tray if exists

        if (location != null)
            updateDataApiCall(
                (location?.latitude ?: 0.0).toString(), (location?.longitude
                    ?: 0.0).toString()
            )
    }

    override fun onDestroy() {
        super.onDestroy()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        checkServiceInProgress(intent ?: Intent())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        locationProvider.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationProvider.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /* Callback providing google maps object showing map is ready to use after getMapAsync*/
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        googleMapHome = googleMap
        googleMapHome?.clear()
        googleMapHome?.setOnMarkerClickListener { true }
        //val mapType = SharedPrefs.with(this).getInt(PREF_MAP_VIEW, GoogleMap.MAP_TYPE_NORMAL)
        googleMapHome?.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMapHome?.uiSettings?.isTiltGesturesEnabled =
            false // Disables the gesture to view 3D view of the google maps
        googleMapHome?.uiSettings?.isMyLocationButtonEnabled =
            false // Hides the default current locator button of google maps
        locationProvider = LocationProvider.LocationUpdatesBuilder(this).build()
        locationProvider.getLastKnownLocation(OnSuccessListener { location ->
            /* Gets the last known location of the user*/
            location?.let {
                this.location = location
                googleMapHome?.isMyLocationEnabled = false
                updateDataApiCall((location.latitude).toString(), (location.longitude).toString())
                googleMapHome?.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(location.latitude, location.longitude), 14f
                    )
                )
                locationProvider.getAddressFromLatLng(
                    location.latitude,
                    location.longitude,
                    object : LocationProvider.OnAddressListener {
                        override fun getAddress(address: String, result: List<Address>) {
                            /* gets the address from the current user's location*/
                            val currentLocation = CurrentLocationModel(location.latitude, location.longitude, address)
                            PrefsManager.get().save(PrefsConstant.CURRENT_LOCATION, currentLocation)
                            startRequestFragment?.updateTrack()
                        }
                    })
            }
        })
    }

    /* Api call to update the user's latest data like location and fcm_id */
    private fun updateDataApiCall(lat: String, lng: String) {
        if (isConnectedToInternet(this, true)) {
            val hashMap = HashMap<String, Any>()
            hashMap["live_lat"] = lat
            hashMap["live_long"] = lng
            viewModelLocation.updateLocation(hashMap)
        }
    }


    /* *//* Api success for logout user api.*//*
    override fun logoutSuccess() {
        *//* Logout successful. Clears*//*
         //PrefsManager.get().removeAllSharedPrefsChangeListeners()
         PrefsManager.get().removeAll()
        finishAffinity()
      *//*  startActivity(Intent(this@BookingActivity, SignupActivity::class.java))
        AppSocket.get().disconnect()
        AppSocket.get().off()
        AppSocket.get().socket = null*//*
    }

    *//* Api success for update data api.*//*
    override fun onApiSuccess(login: String?) {
       *//* PrefsManager.get().save(PROFILE, login?.AppDetail)
        PrefsManager.get().save(SERVICES, Gson().toJson(login?.services))
        setHeaderView()*//*
    }

    *//* Api success for get Support options listing*//*
    override fun onSupportListApiSuccess(response: List<String>?) {
        *//*rvSupportServices.layoutManager = GridLayoutManager(this, 2,
                OrientationHelper.VERTICAL,
                false)
        rvSupportServices.adapter = SupportServicesAdapter(response)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spacing_home_support)
        rvSupportServices.addItemDecoration(GridSpacingItemDecoration(2, spacingInPixels, true))*//*
    }

    override fun showLoader(isLoading: Boolean) {
        progressDialog?.show(isLoading)
    }

    override fun apiFailure() {
        *//* Show error if needed *//*
    }

    override fun handleApiError(code: Int?, error: String?) {
        *//*if (code == StatusCode.UNAUTHORIZED) {
            *//**//* UserData's authorisation expired. Logout and ask user to login again.*//**//*
            AppUtils.logout(this)
        }*//*
    }
*/
    /* Get google maps instance*/
    fun getMapAsync() {
        mapFragment?.getMapAsync(this)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }

    /**
     * Show markers for both source and destination LatLng and draw polyline path
     * between source and destination
     *
     * @param source source LatLng provided by the user.
     * @param destination destination LatLng provided by the user.
     * */
    fun showMarker(source: LatLng?) {
        val destination = LatLng(location?.latitude ?: 0.0, location?.longitude ?: 0.0)


        /*add marker for both source and destination*/
        srcMarker = googleMapHome?.addMarker(source?.let { MarkerOptions().position(it) })
        srcMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)))
        srcMarker?.setAnchor(0.5f, 0.5f)
        destMarker = googleMapHome?.addMarker(destination?.let { MarkerOptions().position(it) })
        destMarker?.setIcon((BitmapDescriptorFactory.fromResource(R.drawable.ic_addcar_hatchback)))
        destMarker?.setAnchor(0.5f, 0.5f)
        // presenter.drawPolyLine(source, destination, language = LocaleManager.getLanguage(this))
    }

    /* Callback for the google direction api and to draw the polyline between provided
       source and destination location of the user */
    /*override fun polyLine(jsonRootObject: JSONObject, sourceLatLng: LatLng?, destLatLng: LatLng?) {
        var line: Polyline? = null
        line?.remove()
        val routeArray = jsonRootObject.getJSONArray("routes")
        var list: ArrayList<LatLng>
        val pathArray = ArrayList<String>()
        if (routeArray.length() == 0) {
             *//*No route found *//*
            return
        }
        var routes: JSONObject? = null
        for (i in routeArray.length() - 1 downTo 0) {
            routes = routeArray.getJSONObject(i)
            val overviewPolylines = routes.getJSONObject("overview_polyline")
            val encodedString = overviewPolylines.getString("points")
            pathArray.add(encodedString)
            list = decodePoly(encodedString)
            val listSize = list.size
            sourceLatLng?.let { list.add(0, it) }
            destLatLng?.let { list.add(listSize + 1, it) }
            if (i == 0) {
                line = googleMapHome?.addPolyline(PolylineOptions()
                        .addAll(list)
                        .width(8f)
                        .color(ContextCompat.getColor(this, R.color.colorAccent))
                        .geodesic(true))
            }

            val builder = LatLngBounds.Builder()
            val arr = ArrayList<Marker?>()
            arr.add(srcMarker)
            arr.add(destMarker)
            for (marker in arr) {
                builder.include(marker?.position)
            }
            val bounds = builder.build()
            val cu = CameraUpdateFactory.newLatLngBounds(bounds,
                    getScreenWidth(this),
                    getScreenWidth(this).minus(dpToPx(24).toInt()),
                    dpToPx(56).toInt())
            googleMapHome.animateCamera(cu)

        }
        val estimatedDistance = (((routes?.get("legs") as JSONArray).get(0) as JSONObject).get("distance") as JSONObject).get("value") as Int
        try {
            serviceRequestModel.order_distance = estimatedDistance / 1000f
        } catch (e: Exception) {
            serviceRequestModel.order_distance = 0f
        }
    }*/


    var callPolyline: Call<ResponseBody>? = null
    var isPolylineApiCalling = false
    fun drawPolyLine(sourceLat: Double?, sourceLong: Double?, destLat: Double?, destLong: Double?) {
        if (isPolylineApiCalling) return
        val BASE_URL_for_map = "https://maps.googleapis.com/maps/api/"
        val retrofit =
            Retrofit.Builder().baseUrl(BASE_URL_for_map).addConverterFactory(GsonConverterFactory.create()).build()
        val api = retrofit.create(MrSteamApi::class.java)
//        callPolyline?.cancel()
        isPolylineApiCalling = true

        val sourceLatL = location?.latitude ?: sourceLat
        val sourceLongL = location?.longitude ?: sourceLong

        callPolyline = api.getPolYLine(
            "$sourceLatL,$sourceLongL",
            "$destLat,$destLong", Locale.US.language
        )
        callPolyline?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                isPolylineApiCalling = false
                if (response.isSuccessful) {
                    try {
                        val responsePolyline = response.body()?.string()
                        val jsonRootObject = JSONObject(responsePolyline)
                        polyLine(jsonRootObject)
                    } catch (e: IOException) {
                        e.printStackTrace()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }/* else {
                    val errorModel = getApiError(response.errorBody()?.string())
                    getView()?.handleApiError(errorModel.statusCode, errorModel.msg)
                }*/
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                isPolylineApiCalling = false
//                getView()?.apiFailure()
            }
        })
    }

    fun polyLine(jsonRootObject: JSONObject) {
        val routeArray = jsonRootObject.getJSONArray("routes")
        if (routeArray.length() == 0) {
            return
        }
        val routes = routeArray.getJSONObject(0)
        line?.remove()
        val overviewPolyLines = routes.getJSONObject("overview_polyline")
        val encodedString = overviewPolyLines.getString("points")
        val legsJsonObject = (((routes.get("legs") as JSONArray).get(0) as JSONObject))
        estimatedDistance = (legsJsonObject.get("distance") as JSONObject).get("text") as String
        estimatedTime = (legsJsonObject.get("duration") as JSONObject).get("text") as String
        sendBroadcast(Intent("updateETA").putExtra("time", estimatedTime).putExtra("distance", estimatedDistance))
        line?.remove()
        list.clear()
        list.addAll(decodePoly(encodedString))

        /*    if (list.size >= 2) {
                currentLatLng = list[1]
            }*/

        /* if (carMarker == null) {
             carMarker = googleMapHome.addMarker(MarkerOptions().position(currentLatLng
                     ?: LatLng(0.0, 0.0)).anchor(.5f, .5f).rotation(currentBearing).flat(true))
             setMarkerIcon()
         }*/


        line = googleMapHome?.addPolyline(
            PolylineOptions()
                .addAll(list)
                .width(10f)
                .color(ContextCompat.getColor(this, R.color.mrSteamMain))
                .geodesic(true)
        )
        if (list.size >= 2) {
            val polylineModel = PolylineModel(
                encodedString,
                estimatedDistance,
                (legsJsonObject.get("distance") as JSONObject).getInt("value"),
                estimatedTime,
                (legsJsonObject.get("duration") as JSONObject).getInt("value")
            )

            //updateMarkerOfDriver(list[0], polylineModel)
        }
    }

    fun clearPolyline() {
        line?.remove()
    }


    /* decode the encoded path we got from the direction api to draw polyline*/
    private fun decodePoly(encoded: String): ArrayList<LatLng> {
        val poly = java.util.ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0
        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat
            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng
            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }
        return poly
    }

    override fun onBackPressed() {
        when {
            supportFragmentManager.fragments.find { it is ServiceRequestFragment } != null -> {
                // Do nothing
            }
            supportFragmentManager.backStackEntryCount == 1 -> {
                finish()
            }
            else -> super.onBackPressed()
        }
    }
}
