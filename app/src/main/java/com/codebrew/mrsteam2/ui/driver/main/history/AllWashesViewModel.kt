package com.codebrew.mrsteam2.ui.driver.main.history

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Home
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AllWashesViewModel : ViewModel() {
    val washesRes by lazy { SingleLiveEvent<Resource<Home>>() }

    fun allWashes(status: Int) {
        washesRes.value = Resource.loading()

        RetrofitClient.getApi().allWashes(status)
                .enqueue(object : Callback<ApiResponse<Home>> {
                    override fun onFailure(call: Call<ApiResponse<Home>>, throwable: Throwable) {
                        washesRes.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<Home>>,
                            response: Response<ApiResponse<Home>>
                    ) {
                        if (response.isSuccessful) {
                            washesRes.value = Resource.success(response.body()?.data)
                        } else {
                            washesRes.value = Resource.error(
                                    ApiUtils.getError(
                                            response.code(),
                                            response.errorBody()?.string()
                                    )
                            )
                        }
                    }

                })
    }
}