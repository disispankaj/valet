package com.codebrew.mrsteam2.ui.customer.booking.promocode

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import kotlinx.android.synthetic.main.fragment_promo_code.*

class PromoCodeFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_promo_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvAvailableCoupons.layoutManager = LinearLayoutManager(activity as Activity, RecyclerView.VERTICAL, false)
        rvAvailableCoupons.adapter = PromoCodeAdapter()

    }
}