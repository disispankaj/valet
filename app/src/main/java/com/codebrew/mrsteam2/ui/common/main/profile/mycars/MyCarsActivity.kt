package com.codebrew.mrsteam2.ui.common.main.profile.mycars

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.MyCars
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar.AddCarActivity
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.addcar.AddCarViewModel
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_my_cars.*

class MyCarsActivity : AppCompatActivity() {

    private lateinit var adapter: MyCarsAdapter

    private lateinit var viewModel: MyCarsViewModel

    private lateinit var viewModelDelete: AddCarViewModel

    private lateinit var progressDialog: ProgressDialog

    private var items = ArrayList<MyCars>()

    private var deletePos = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_cars)

        listeners()
        initialise()
        liveData()
        setAdapter()
    }

    private fun setAdapter() {
        rvViewMyCars.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,
                false)
        adapter = MyCarsAdapter(items)
        rvViewMyCars.adapter = adapter
    }

    private fun listeners() {
        tbrMyCars.setNavigationOnClickListener {
            finish()
        }

        tvAddNew.setOnClickListener {
            val i1 = Intent(this, AddCarActivity::class.java)
            startActivityForResult(i1, AppRequestCode.ADD_CAR)
        }

        swipeRefreshLayout.setOnRefreshListener {
            if (isConnectedToInternet(this, true))
                viewModel.myCars()
        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[MyCarsViewModel::class.java]
        progressDialog = ProgressDialog(this)
        viewModelDelete = ViewModelProviders.of(this)[AddCarViewModel::class.java]


        if (isConnectedToInternet(this, true))
            viewModel.myCars()
    }


    private fun liveData() {
        viewModel.myCarRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)

                    items.clear()
                    items.addAll(resources.data ?: ArrayList())
                    adapter.notifyDataSetChanged()

                    if (items.isEmpty())
                        tvNoCar.visible()
                    else
                        tvNoCar.gone()
                }
                Status.ERROR -> {
                    swipeRefreshLayout.isRefreshing = false
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    if (!swipeRefreshLayout.isRefreshing)
                        progressDialog.setLoading(true)
                }
            }
        })

        viewModelDelete.deleteCar.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    items.removeAt(deletePos)
                    adapter.notifyItemRemoved(deletePos)
                    adapter.notifyItemRangeChanged(deletePos, items.size)
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, this)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppRequestCode.ADD_CAR) {
                if (isConnectedToInternet(this, true))
                    viewModel.myCars()
            }
        }
    }

    fun deleteCar(position: Int) {
        AlertDialogUtil.getInstance().createOkCancelDialog(this, R.string.delete,
                R.string.sure_delete_car, R.string.yes,
                R.string.no, true,
                object : AlertDialogUtil.OnOkCancelDialogListener {
                    override fun onOkButtonClicked() {
                        deletePos = position
                        if (isConnectedToInternet(this@MyCarsActivity, true))
                            viewModelDelete.deleteCar(items[deletePos].id ?: 0)
                    }

                    override fun onCancelButtonClicked() {
                    }
                }).show()
    }
}