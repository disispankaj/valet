package com.codebrew.mrsteam2.ui.common.loginsignup.login

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.LoginRequest
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.ui.common.loginsignup.SocialViewModel
import com.codebrew.mrsteam2.ui.common.loginsignup.forgotpassword.ForgotPasswordFragment
import com.codebrew.mrsteam2.ui.common.loginsignup.signup.SignupFragment
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.utils.*
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_signup_login.*
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONException
import java.util.*
import kotlin.collections.HashMap

class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel

    private lateinit var viewModelSocial: SocialViewModel

    private lateinit var progressDialog: ProgressDialog

    private var callbackManager = CallbackManager.Factory.create()

    private var hashMap = HashMap<String, Any>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialise()
        liveData()
        spanningLogin()
        listeners()

    }

    private fun listeners() {

        btnLogin.setOnClickListener {
            validationLoginPage()
        }

        tvForgotPassword.setOnClickListener {
            fragmentManager?.beginTransaction()?.replace(
                R.id.layoutFragmentData,
                ForgotPasswordFragment(),
                ForgotPasswordFragment::class.java.name
            )?.addToBackStack(ForgotPasswordFragment::class.java.name)?.commit()
        }

        tvSignUp.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(
                    R.id.layoutFragmentData,
                    SignupFragment(), SignupFragment::class.java.name
                )
                ?.addToBackStack(SignupFragment::class.java.name)?.commit()
        }

        btnFacebook.setOnClickListener {
            facebookLogin()
        }
    }

    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[LoginViewModel::class.java]
        viewModelSocial = ViewModelProviders.of(this)[SocialViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

//        (activity as LoginSignupActivity).ivBack.invisible()
//        (activity as LoginSignupActivity).ivLogo1.invisible()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun loginApiHit() {

        if (isConnectedToInternet(activity as Activity, true)) {
            /*Firebase token*/
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(activity as Activity) { instanceIdResult ->

                val logInRequest = LoginRequest(
                    input = etEmailPhone.text.toString(),
                    password = etPassword.text.toString(),
                    device_token = instanceIdResult.token,
                    country_code = ccpLogin.selectedCountryCodeAsInt,
                    device_type = 1
                )

                viewModel.logIn(logInRequest)
            }
        }

    }


    private fun liveData() {

        viewModel.loginRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    PrefsManager.get().save(SP_USER_DATA, resources.data ?: UserData())
                    PrefsManager.get().save(ACCESS_TOKEN, resources.data?.token ?: "")

                    startActivity(
                        Intent(context, MainActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    )
                    activity?.finishAffinity()
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModelSocial.socialLogin.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    if (resources.data?.is_exist == true) {
                        PrefsManager.get().save(SP_USER_DATA, resources.data.user)
                        PrefsManager.get().save(ACCESS_TOKEN, resources.data.user.token ?: "")

                        startActivity(
                            Intent(context, MainActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        )
                        activity?.finishAffinity()
                    } else {

                        val fragment = SignupFragment()
                        val bundle = Bundle()
                        bundle.putSerializable(FACEBOOK_DATA, hashMap)
                        fragment.arguments = bundle

                        fragmentManager?.beginTransaction()
                            ?.replace(
                                R.id.layoutFragmentData,
                                fragment, SignupFragment::class.java.name
                            )
                            ?.addToBackStack(SignupFragment::class.java.name)?.commit()

                        Toast.makeText(activity, getString(R.string.fill_information), Toast.LENGTH_LONG).show()
                    }
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

    }

    private fun validationLoginPage() {
        tilEmailPhone.error = null
        tilPassword.error = null

        return when {
            etEmailPhone.text.toString().isEmpty() -> {
                invalidString(etEmailPhone, getString(R.string.error_empty_phone_email))
            }
            etPassword.text.toString().isEmpty() -> {
                invalidString(etPassword, getString(R.string.error_empty_password))
            }
            (!EMAIL_FORMAT.matcher(etEmailPhone.text.toString()).matches())
                    && (!PHONE_FORMAT.matcher(etEmailPhone.text.toString()).matches()) -> {
                invalidString(etEmailPhone, getString(R.string.str_phone_email_error))

            }
            /*(!PASSWORD_FORMAT.matcher(etPassword.text.toString()).matches()) -> {
                invalidString(etPassword, getString(R.string.str_password_error))
            }*/
            else -> {
                if (isConnectedToInternet(activity, true))
                    loginApiHit()
                else {
                    Toast.makeText(context,"Internet connection problem",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun spanningLogin() {
        val spanStr = SpannableString(getString(R.string.str_login_sub_heading))
        val str = getString(R.string.app_name)
        spanStr.setSpan(
            StyleSpan(Typeface.BOLD),
            spanStr.indexOf(str), spanStr.indexOf(str) + str.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tvSubHeading.text = spanStr
    }

    private fun facebookLogin() {
        if (isConnectedToInternet(activity, true)) {
            LoginManager.getInstance().logOut()
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email,public_profile"))
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    progressDialog.setLoading(true)
                    val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, _ ->
                        progressDialog.setLoading(false)
                        try {
                            hashMap = HashMap()
                            hashMap["provider_user_id"] = `object`.getString("id")
                            hashMap["provider"] = "Facebook"
                            hashMap["device_type"] = 1
                            hashMap["device_token"] = "dsafakjsfdgasdjgkasfd"
                            hashMap["name"] = `object`.getString("name")
                            hashMap["email"] = `object`.getString("email")

                            if (isConnectedToInternet(activity, true))
                                viewModelSocial.socialLogin(hashMap)


                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,first_name,last_name,gender,picture.type(large),email")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    progressDialog.setLoading(false)
                    LoginManager.getInstance().logOut()
                }

                override fun onError(error: FacebookException) {
                    progressDialog.setLoading(false)
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show()
                }
            })
        }
    }


}