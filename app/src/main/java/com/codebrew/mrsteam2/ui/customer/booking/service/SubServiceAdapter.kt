package com.codebrew.mrsteam2.ui.customer.booking.service

import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Active_sub_service
import kotlinx.android.synthetic.main.item_sub_service.view.*

class SubServiceAdapter(
    private val adapterList: ArrayList<Active_sub_service>?, private val fragment: Fragment?,
    private val isSelectable: Boolean,
    private val isClikee: Boolean
) :
    RecyclerView.Adapter<SubServiceAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(View.inflate(parent.context, R.layout.item_sub_service, null))
    }


    override fun getItemCount(): Int {
        return adapterList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(!isClikee){
            holder.itemView.rlSubService.isEnabled = false
        }
        holder.bind(adapterList?.get(position))

    }

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(item: Active_sub_service?) = with(itemView) {

            tvWash.text = item?.dual_title + " $" + item?.price
            tvWashDesc.text = item?.dual_description
            chkboxWash.isChecked = item?.isSelected ?: false

            if(isClikee){
                rlSubService.setOnClickListener {
                    if (isSelectable) {
                        (fragment as ServiceFragment).subCategoryId = adapterList?.get(adapterPosition)?.id
                            ?: -1
                        for (itemSelection in adapterList ?: ArrayList()) {
                            adapterList?.get(adapterList.indexOf(itemSelection))?.isSelected =
                                (adapterList?.indexOf(itemSelection) == adapterPosition)
                        }

                        notifyDataSetChanged()
                    }
                    else{

                    }
                }

            }


        }

    }

}




