package com.codebrew.mrsteam2.ui.customer.booking.bookmakepayment


import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.customer.booking.promocode.Promo
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RequestViewModel : ViewModel() {
    val request by lazy { SingleLiveEvent<Resource<Wash>>() }

    fun requests(hashMap: HashMap<String, Any>) {

        request.value = Resource.loading()
        RetrofitClient.getApi().requests(hashMap)
                .enqueue(object : Callback<ApiResponse<Wash>> {
                    override fun onFailure(call: Call<ApiResponse<Wash>>, throwable: Throwable) {
                        request.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(call: Call<ApiResponse<Wash>>,
                                            response: Response<ApiResponse<Wash>>) {
                        if (response.isSuccessful) {
                            request.value = Resource.success(response.body()?.data)
                        } else {
                            request.value = Resource.error(                                 ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }

}