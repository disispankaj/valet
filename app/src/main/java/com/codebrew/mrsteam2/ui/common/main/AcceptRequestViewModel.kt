package com.codebrew.mrsteam2.ui.common.main

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AcceptRequestViewModel : ViewModel() {
    val acceptRejectRequest by lazy { SingleLiveEvent<Resource<Wash>>() }

    fun acceptRejectRequest(washRequest: Int, hashMap: HashMap<String, Any>) {
        acceptRejectRequest.value = Resource.loading()


        RetrofitClient.getApi().acceptRejectRequest(washRequest, hashMap)
                .enqueue(object : Callback<ApiResponse<Wash>> {
                    override fun onFailure(call: Call<ApiResponse<Wash>>, throwable: Throwable) {
                        acceptRejectRequest.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<Wash>>,
                            response: Response<ApiResponse<Wash>>) {
                        if (response.isSuccessful) {
                            acceptRejectRequest.value = Resource.success(response.body()?.data)
                        } else {
                            acceptRejectRequest.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }
}