package com.codebrew.mrsteam2.ui.common.main.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.profile.changepassword.ChangePasswordActivity
import com.codebrew.mrsteam2.ui.common.main.profile.editprofile.EditProfileActivity
import com.codebrew.mrsteam2.ui.common.main.profile.mycars.MyCarsActivity
import com.codebrew.mrsteam2.ui.common.main.profile.myoffers.MyOffersActivity
import com.codebrew.mrsteam2.ui.common.main.profile.services.ServicesActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.fragment_main_profile.*


class ProfileFragment : Fragment() {
    private lateinit var viewModel: LogoutViewModel
    private lateinit var progressDialog: ProgressDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(activity!!)
        super.onViewCreated(view, savedInstanceState)

        initialise()
        userData()
        listener()
        liveData()
    }


    private fun initialise() {
        if (PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)?.role == UserType.DRIVER) {
            tvMyCars.gone()
            viewBelowMyCars.gone()
            tvServices.gone()
            viewBelowServices.gone()
            tvMyOffers.gone()
            viewBelowMyOffers.gone()
        }

        viewModel = ViewModelProviders.of(this)[LogoutViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)
    }

    private fun listener() {
        tvEditProfile.setOnClickListener {
            val i1 = Intent(activity as Activity, EditProfileActivity::class.java)
            startActivityForResult(i1, AppRequestCode.EDIT_PROFILE)
        }

        tvChangePassword.setOnClickListener {
            startActivity(Intent(activity as Activity, ChangePasswordActivity::class.java))
        }

        tvMyCars.setOnClickListener {
            startActivity(Intent(activity as Activity, MyCarsActivity::class.java))
        }
        tvServices.setOnClickListener {
            startActivity(Intent(activity as Activity, ServicesActivity::class.java))
        }
        tvMyOffers.setOnClickListener {
            startActivity(Intent(activity as Activity, MyOffersActivity::class.java))
        }

        tvSignOut.setOnClickListener {
            showLogoutDialog()
        }
    }

    private fun userData() {
        val data = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)

        tvName.text = data?.name
        tvPhone.text = data?.phone
        tvEmail.text = data?.email

        loadImage(activity, ivProfilePicture, data?.image_url?.thumbnail
                ?: "", data?.image_url?.original ?: "")
    }

    private fun showLogoutDialog() {
        AlertDialogUtil.getInstance().createOkCancelDialog(activity, R.string.sign_out,
                R.string.logout_dialog_message, R.string.yes,
                R.string.no, true,
                object : AlertDialogUtil.OnOkCancelDialogListener {
                    override fun onOkButtonClicked() {
                        if (isConnectedToInternet(activity as Activity, true))
                            viewModel.logout()

                    }

                    override fun onCancelButtonClicked() {
                    }
                }).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppRequestCode.EDIT_PROFILE) {
                userData()
            }
        }
    }

    private fun liveData() {
        viewModel.outRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    PrefsManager.get().save(SP_USER_DATA, resources.data)
                    progressDialog.setLoading(false)
//<<<<<<< Updated upstream
                    Toast.makeText(activity as Activity, resources.data?.message.toString(), Toast.LENGTH_LONG).show()
//=======
                //    Toast.makeText(activity as Activity, resources.data?.msg.toString(), Toast.LENGTH_LONG).show()
//>>>>>>> Stashed changes
                    logoutUser(activity as Activity)
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }
}