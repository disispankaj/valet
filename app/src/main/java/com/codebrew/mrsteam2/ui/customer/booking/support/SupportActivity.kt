package com.codebrew.mrsteam2.ui.customer.booking.support

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Service
import com.codebrew.mrsteam2.ui.customer.booking.SupportServicesAdapter
import com.codebrew.mrsteam2.utils.GridSpacingItemDecoration
import com.codebrew.mrsteam2.utils.LocaleManager
import com.codebrew.mrsteam2.utils.MrSteamApp
import com.codebrew.mrsteam2.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.activity_support.*


class SupportActivity : AppCompatActivity() {

    private var supportList = ArrayList<Service>()


    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)

        setAdapter()
        if (isConnectedToInternet(this, true)) {
            // presenter.getSupportList()
        }
        setListeners()
    }

    private fun setListeners() {
        tvBack.setOnClickListener { onBackPressed() }
    }

    private fun setAdapter() {
        rvSupportList.layoutManager = GridLayoutManager(this, 2, OrientationHelper.VERTICAL, false)
        rvSupportList.adapter = SupportServicesAdapter(supportList)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.fab_margin)
        rvSupportList.addItemDecoration(GridSpacingItemDecoration(2, spacingInPixels, true))

    }

    /* override fun onSupportListApiSuccess(response: List<Service>?) {
         if (response?.isNotEmpty() == true) {
             flipperSupport.displayedChild = 1
             supportList.addAll(response)
             rvSupportList.adapter.notifyDataSetChanged()
         } else {
             flipperSupport.displayedChild = 2
         }
     }

     override fun showLoader(isLoading: Boolean) {

     }

     override fun apiFailure() {
         rvSupportList?.showSWWerror()
     }

     override fun handleApiError(code: Int?, error: String?) {
         rvSupportList?.showSnack(error.toString())
     }*/

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase))
    }
}