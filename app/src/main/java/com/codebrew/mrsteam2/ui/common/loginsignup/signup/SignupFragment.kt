package com.codebrew.mrsteam2.ui.common.loginsignup.signup

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.network.requestmodels.SignupRequest
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.loginsignup.LoginSignupActivity
import com.codebrew.mrsteam2.ui.common.loginsignup.SocialViewModel
import com.codebrew.mrsteam2.ui.common.loginsignup.login.LoginFragment
import com.codebrew.mrsteam2.ui.common.loginsignup.verifynumber.VerifyNumberFragment
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.utils.*
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_signup_login.*
import kotlinx.android.synthetic.main.fragment_signup.*
import org.json.JSONException
import java.util.*
import kotlin.collections.HashMap


class SignupFragment : Fragment() {
    private var genderIndex = UserGender.MALE
    private var callbackManager = CallbackManager.Factory.create()

    private lateinit var viewModel: SignUpViewModel

    private lateinit var viewModelSocial: SocialViewModel

    private lateinit var progressDialog: ProgressDialog

    private var hashMap = HashMap<String, Any>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_signup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        spanningSignup()
        listeners()
        initialise()
        liveData()
        //   generateKeyHash()
    }

    private fun listeners() {
        etPhone.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                viewBelowPhone.setBackgroundResource(R.color.mrSteamMain)
                viewBelowPhone.alpha = 0.8f
                tvMobileNumberTxt.setTextColor(ContextCompat.getColor(v.context, R.color.mrSteamMain))
                tvMobileNumberTxt.alpha = 0.8f
            } else {
                viewBelowPhone.setBackgroundResource(R.color.edittext_hint)
                viewBelowPhone.alpha = 0.5f
                tvMobileNumberTxt.setTextColor(ContextCompat.getColor(v.context, R.color.edittext_hint))
                tvMobileNumberTxt.alpha = 0.7f
            }
        }

        radioBtnGroupGender.setOnCheckedChangeListener { group, checkedId ->
            genderIndex = when (checkedId) {
                R.id.radioBtnMale -> UserGender.MALE
                R.id.radioBtnFemale -> UserGender.FEMALE
                R.id.radioBtnOthers -> UserGender.OTHER
                else -> UserGender.MALE
            }
        }

        tvLogin.setOnClickListener {
            fragmentManager?.beginTransaction()
                ?.replace(
                    R.id.layoutFragmentData,
                    LoginFragment(), LoginFragment::class.java.name
                )
                ?.addToBackStack(LoginFragment::class.java.name)?.commit()
        }


        btnNext.setOnClickListener {
            validationSignup()
        }

        btnFacebook.setOnClickListener {
            facebookLogin()
        }

    }


    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        viewModelSocial = ViewModelProviders.of(this)[SocialViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

//        (activity as LoginSignupActivity).ivBack.visible()

        if (arguments?.containsKey(FACEBOOK_DATA) == true) {
            hashMap = arguments?.getSerializable(FACEBOOK_DATA) as HashMap<String, Any>

            etUsername.setText(hashMap["name"].toString())
            if (hashMap["email"].toString().isNotEmpty()) {
                etEmail.setText(hashMap["email"].toString())
                etEmail.isClickable = false
                etEmail.isFocusable = false
                tilEmail.isClickable = false
                tilEmail.isFocusable = false
            }
        }
    }


    private fun registerApiHit() {
        if (isConnectedToInternet(activity, true)) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(activity as Activity) { instanceIdResult ->


                val signUpRequest = SignupRequest(
                    provider_user_id = (hashMap["provider_user_id"] ?: "").toString(),
                    provider = (hashMap["provider"] ?: "").toString(),
                    name = etUsername.text.toString(),
                    country_code = ccode.selectedCountryCodeAsInt,
                    phone = etPhone.text.toString(),
                    email = etEmail.text.toString(),
                    gender = genderIndex,
                    password = etPassword.text.toString(),
                    device_token = instanceIdResult.token,
                    device_type = 1
                )

                viewModel.signUp(signUpRequest)
            }
        }
    }


    private fun liveData() {
        viewModel.signupRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)
                    val frag = VerifyNumberFragment()
                    val args = Bundle()
                    args.putString(USER_ID, resources.data?.user?.id.toString())
                    frag.arguments = args

                    fragmentManager?.beginTransaction()
                        ?.replace(
                            R.id.layoutFragmentData, frag,
                            VerifyNumberFragment::class.java.name
                        )
                        ?.addToBackStack(VerifyNumberFragment::class.java.name)?.commit()
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })

        viewModelSocial.socialLogin.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {
                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    if (resources.data?.is_exist == true) {
                        PrefsManager.get().save(SP_USER_DATA, resources.data.user)
                        PrefsManager.get().save(ACCESS_TOKEN, resources.data.user.token ?: "")

                        startActivity(
                            Intent(context, MainActivity::class.java)
                                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        )
                        activity?.finish()
                    } else {
                        Toast.makeText(activity, getString(R.string.fill_information), Toast.LENGTH_LONG).show()
                    }
                }
                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }
                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun validationSignup() {
        tilUsername.error = null
        tilEmail.error = null
        etPhone.error = null
        tilPassword.error = null

        return when {
            etUsername.text.toString().isEmpty() -> {
                invalidStringInput(etUsername, tilUsername, getString(R.string.error_empty_username))

            }
            etPhone.text.toString().isEmpty() -> {
                invalidString(etPhone, getString(R.string.error_empty_phone))

            }
            (!PHONE_FORMAT.matcher(etPhone.text.toString()).matches())
            -> {
                invalidString(etPhone, getString(R.string.str_phone_error))
            }
            etPassword.text.toString().isEmpty() -> {
                invalidStringInput(etPassword, tilPassword, getString(R.string.error_empty_password))

            }
            (!PASSWORD_FORMAT.matcher(etPassword.text.toString()).matches()) -> {
                invalidStringInput(etPassword, tilPassword, getString(R.string.str_password_error))

            }
            etEmail.text.toString().isNotEmpty() && !EMAIL_FORMAT.matcher(etEmail.text.toString()).matches() -> {
                invalidStringInput(etEmail, tilEmail, getString(R.string.str_email_error))
            }
            else -> registerApiHit()
        }
    }

    private fun spanningSignup() {
        val spanStr = SpannableString(getString(R.string.str_signup_term_and_condition))
        val str1 = getString(R.string.str_privacy_policy)
        val str2 = getString(R.string.str_term_of_use)


        var language = PrefsManager.get().getString(LANG, getString(R.string.str_lang_en)) ?: "en"
        if (language.equals("en")) {
            val clickSpanPrivacy = object : ClickableSpan() {
                override fun onClick(widget: View) {

                }
            }
            spanStr.setSpan(
                clickSpanPrivacy, spanStr.indexOf(str1),
                spanStr.indexOf(str1) + str1.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spanStr.setSpan(
                ForegroundColorSpan(Color.parseColor("#4B5461")),
                spanStr.indexOf(str1),
                spanStr.indexOf(str1) + str1.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            val clickSpanTerm = object : ClickableSpan() {
                override fun onClick(widget: View) {

                }
            }
            spanStr.setSpan(
                clickSpanTerm, spanStr.indexOf(str2),
                spanStr.indexOf(str2) + str2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            spanStr.setSpan(
                ForegroundColorSpan(Color.parseColor("#4B5461")),
                spanStr.indexOf(str2),
                spanStr.indexOf(str2) + str2.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

        }

        tvPrivacyPolicy.text = spanStr
    }

    private fun facebookLogin() {
        if (isConnectedToInternet(activity, true)) {
            LoginManager.getInstance().logOut()
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email,public_profile"))
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    progressDialog.setLoading(true)
                    val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                        progressDialog.setLoading(false)
                        try {
                            hashMap = HashMap()
                            hashMap["provider_user_id"] = `object`.getString("id")
                            hashMap["provider"] = "Facebook"
                            hashMap["device_type"] = 1
                            hashMap["device_token"] = "dsafakjsfdgasdjgkasfd"
                            /*hashMap["image"] = ("https://graph.facebook.com/" +
                                    ``object``.getString("id") + "/picture?type=large")*/


                            etUsername.setText(`object`.getString("name"))
                            if (`object`.has("email")) {
                                etEmail.setText(`object`.getString("email"))
                                etEmail.isClickable = false
                                etEmail.isFocusable = false
                                tilEmail.isClickable = false
                                tilEmail.isFocusable = false
                            }

                            if (isConnectedToInternet(activity, true))
                                viewModelSocial.socialLogin(hashMap)


                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,first_name,last_name,gender,picture.type(large),email")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    progressDialog.setLoading(false)
                    LoginManager.getInstance().logOut()
                }

                override fun onError(error: FacebookException) {
                    progressDialog.setLoading(false)
                    Toast.makeText(activity, error.toString(), Toast.LENGTH_LONG).show()
                }
            })
        }
    }

}




