package com.codebrew.mrsteam2.ui.customer.booking


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Service
import kotlinx.android.synthetic.main.item_support_service.view.*

class SupportServicesAdapter(private val response: List<Service>?) : RecyclerView.Adapter<SupportServicesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_support_service, parent, false))

    override fun getItemCount(): Int = response?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(response?.get(position))

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView?.setOnClickListener { Toast.makeText(itemView.context, R.string.coming_soon, Toast.LENGTH_SHORT).show() }
        }

        fun bind(data: Service?) {
            itemView.tvSupportServiceName.text = data?.name
            Glide.with(itemView.context).load(data?.image_url).into(itemView.ivSupportServiceImage)
        }
    }

}