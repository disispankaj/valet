package com.codebrew.mrsteam2.ui.driver.main.history

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.ApisRespHandler
import com.codebrew.mrsteam2.network.common.Status
import com.codebrew.mrsteam2.ui.common.dialogs.ProgressDialog
import com.codebrew.mrsteam2.ui.common.main.home.HomeAdapter
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    private lateinit var viewModel: AllWashesViewModel
    private lateinit var progressDialog: ProgressDialog

    private val recentAdapter = HomeAdapter(null,RECENT_WASH)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        MrSteamApp().getInstance().setLocale(activity!!)
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapterSetting()
        initialise()
        liveData()

    }

    private fun adapterSetting() {
        rvRecentWash.layoutManager = LinearLayoutManager(activity as Activity)
        rvRecentWash.adapter = recentAdapter
    }


    private fun initialise() {
        viewModel = ViewModelProviders.of(this)[AllWashesViewModel::class.java]
        progressDialog = ProgressDialog(activity as Activity)

        if (isConnectedToInternet(activity as Activity, true))
            viewModel.allWashes(2)
    }

    private fun liveData() {
        viewModel.washesRes.observe(this, Observer { resources ->
            resources ?: return@Observer
            when (resources.status) {

                Status.SUCCESS -> {
                    progressDialog.setLoading(false)

                    if (resources.data?.recent_washes_count == 0) {
                        tvNoRecentWash.visible()
                    } else {
                        tvNoRecentWash.gone()
                        recentAdapter.setDataRecent(resources.data?.recent_washes ?: ArrayList())
                    }
                }

                Status.ERROR -> {
                    progressDialog.setLoading(false)
                    ApisRespHandler.handleError(resources.error, activity as Activity)
                }

                Status.LOADING -> {
                    progressDialog.setLoading(true)
                }
            }
        })
    }

}