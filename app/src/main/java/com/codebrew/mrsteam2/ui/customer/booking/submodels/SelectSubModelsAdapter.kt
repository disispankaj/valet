package com.codebrew.mrsteam2.ui.customer.booking.submodels


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.buraq24.customer.webservices.models.homeapi.Product
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.utils.setRoundImageUrl
import kotlinx.android.synthetic.main.item_vehicle_type.view.*

class SelectSubModelsAdapter constructor(private var products: ArrayList<Product?>?, private var prevSelectedPosition: Int) : RecyclerView.Adapter<SelectSubModelsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_vehicle_type, parent, false))
    }

    override fun getItemCount(): Int {
        return products?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(products?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            products?.get(prevSelectedPosition)?.isSelected = true
            itemView?.setOnClickListener {
                products?.get(prevSelectedPosition)?.isSelected = false
                products?.get(adapterPosition)?.isSelected = true
                prevSelectedPosition = adapterPosition
//                    itemSelectedListener?.onItemSelected(adapterPosition)
                notifyDataSetChanged()
            }

        }

        fun onBind(product: Product?) = with(itemView) {
            tvVehicleName.text = product?.name
            ivVehicleImage.setRoundImageUrl(product?.image_url)
            tvVehicleName.isSelected = product?.isSelected ?: false
            viewSelector.visibility = if (product?.isSelected == true) View.VISIBLE else View.GONE
        }
    }

    fun getSelectedBrandProductId(): Int {
        return products?.get(prevSelectedPosition)?.category_brand_product_id ?: 0
    }

    fun getSelectedBrandProductName(): String {
        return products?.get(prevSelectedPosition)?.name ?: ""
    }

    fun getSelectedProduct(): Product? {
        return products?.get(prevSelectedPosition)
    }
}