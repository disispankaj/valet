package com.codebrew.mrsteam2.ui.common.main

import androidx.lifecycle.ViewModel
import com.codebrew.mrsteam2.network.RetrofitClient
import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.common.ApiUtils
import com.codebrew.mrsteam2.network.common.Resource
import com.codebrew.mrsteam2.network.responsemodels.order.CurrentOrder
import com.codebrew.mrsteam2.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WashRequestViewModel : ViewModel() {
    val washRequests by lazy { SingleLiveEvent<Resource<CurrentOrder>>() }

    fun myOrder() {
        washRequests.value = Resource.loading()


        RetrofitClient.getApi().washRequests()
                .enqueue(object : Callback<ApiResponse<CurrentOrder>> {
                    override fun onFailure(call: Call<ApiResponse<CurrentOrder>>, throwable: Throwable) {
                        washRequests.value = Resource.error(ApiUtils.failure(throwable))
                    }

                    override fun onResponse(
                            call: Call<ApiResponse<CurrentOrder>>,
                            response: Response<ApiResponse<CurrentOrder>>
                    ) {
                        if (response.isSuccessful) {
                            washRequests.value = Resource.success(response.body()?.data)
                        } else {
                            washRequests.value = Resource.error(
                                    ApiUtils.getError(response.code(),
                                            response.errorBody()?.string()))
                        }
                    }

                })
    }
}