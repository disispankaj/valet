package com.codebrew.mrsteam2.ui.driver.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.Wash
import com.codebrew.mrsteam2.ui.customer.ratereview.RateReviewActivity
import com.codebrew.mrsteam2.utils.*
import kotlinx.android.synthetic.main.activity_details.*

class DetailActivity : AppCompatActivity() {
    private var data: Wash? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        MrSteamApp().getInstance().setLocale(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        data = intent.getSerializableExtra(DETAIL_DATA) as Wash
        dataSetup()
        listener()
    }

    private fun listener() {
        tbrDetail.setNavigationOnClickListener {
            if (!intent.hasExtra("isHomeDataUpdate")) {
                sendBroadcast(Intent("refreshHomeData"))
            }
            onBackPressed()
        }

        tvRate.setOnClickListener {
            startActivity(
                Intent(this, RateReviewActivity::class.java)
                    .putExtra(DETAIL_DATA, data)
            )
            finish()
        }
    }

    private fun dataSetup() {
        loadImage(
            this, ivDP, data?.user?.image_url?.thumbnail ?: "",
            data?.user?.image_url?.original ?: ""
        )

        tvDetailName.text = data?.user?.name
        tvPlateNum.text = data?.my_car?.plate_number
        tvDate.text = formatDate(data?.created_at, "dd-MMM-yyyy")
        tvAddress.text = data?.address
        tvServices.text = data?.sub_service?.dual_title

        tvPrice.text = data?.payment?.final_charge

        if (data?.ratingByUser != null) {
            if (data?.ratingByUser?.ratings!! > 0) {
                tvRate.visibility = View.GONE
            }
            ratingBar.setRating(data?.ratingByUser?.ratings!!.toFloat())
        }



        when (data?.my_car?.vehicle_type?.toInt()) {
            1 -> tvVehicle.text = getString(R.string.str_hatchback)
            2 -> tvVehicle.text = getString(R.string.str_sedan)
            3 -> tvVehicle.text = getString(R.string.str_mpv)
            else -> tvVehicle.text = getString(R.string.str_suv)

        }

        if (data?.rating == null) {
            ratingBar.rating = 0f
            tvRate.visible()
        } else {
            ratingBar.rating = data?.rating?.toFloat() ?: 0f
            tvRate.gone()
        }
    }
}