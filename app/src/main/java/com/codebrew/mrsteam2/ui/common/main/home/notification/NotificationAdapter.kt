package com.codebrew.mrsteam2.ui.common.main.home.notification

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.notification.NotificationDataItem
import com.codebrew.mrsteam2.utils.loadImage
import com.codebrew.mrsteam2.utils.setTimeLap
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NotificationAdapter(val listener: ClickNotification) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    private var notificationList: ArrayList<NotificationDataItem> = ArrayList()

    private val l1 = ArrayList<String>()

    interface ClickNotification {
        fun onClick(id: String?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v1 = View.inflate(parent.context, R.layout.item_notification, null)
        return ViewHolder(v1)
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(notificationList[position])
    }


    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        init {
            itemView.setOnClickListener {
                listener.onClick(notificationList[adapterPosition].id)

                var data = notificationList[adapterPosition]
                data.read_at = currentDateTime()
                notificationList.set(adapterPosition, data)

                notifyDataSetChanged()
            }
        }

        fun bind(data: NotificationDataItem) = with(itemView) {
            tvNotificationDetail.text = data.data?.message
            tvNotificationTime.text = itemView.context?.setTimeLap(parseDate(data.created_at ?: ""))
            loadImage(context as Activity, ivProfilePicture, data.data?.image?.thumbnail, data.data?.image?.original)
            if (data?.read_at != null) {
                rlParent.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            } else {
                rlParent.setBackgroundColor(ContextCompat.getColor(context, R.color.notification_color))
            }

        }
    }

    fun setData(list: ArrayList<NotificationDataItem>) {
        notificationList.clear()
        notificationList.addAll(list ?: emptyList())
        notifyDataSetChanged()
    }

    fun currentDateTime(): String {
        val c = Calendar.getInstance()
        System.out.println("Current time => " + c.time)

        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return df.format(c.time)
    }

    private fun parseDate(givenDateString: String): Long {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        try {

            val mDate: Date = sdf.parse(givenDateString)
            val timeInMilliseconds = mDate.getTime()
            System.out.println("Date in milli :: $timeInMilliseconds")
            return timeInMilliseconds
        } catch (e: ParseException) {
            e.printStackTrace()
            return 0
        }
    }


}

