package com.codebrew.mrsteam2.utils.location

data class LiveLocation(var userId: String?,
                        var driverId: String?,
                        var lat: Double?,
                        var long: Double?,
                        var orderId: String?,
                        var bearing: Float?)