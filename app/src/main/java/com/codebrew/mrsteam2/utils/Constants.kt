package com.codebrew.mrsteam2.utils

import java.util.regex.Pattern

val NAME_FORMAT: Pattern = Pattern.compile("^[a-zA-Z]+[a-zA-Z ]{2,30}+$")
val EMAIL_FORMAT: Pattern = Pattern.compile("[a-zA-Z0-9_.-]+[@][a-zA-Z_-]+[.][a-z.]+$")
val PHONE_FORMAT: Pattern = Pattern.compile("[1-9]+[0-9]{6,15}+$")
val PASSWORD_FORMAT: Pattern = Pattern.compile("^[A-Za-z0-9](?!.*[ ])(?=.*[@#\$&^]).{6,20}")


const val LANG = "Language"
const val SP_USER_DATA = "SP UserData "
const val ACCESS_TOKEN = "Access Token"
const val OTP = "Otp"
const val USER_ID = "UserData ID"
const val FACEBOOK_DATA = "facebook data"

const val OFFERS = "Offer"
const val OFFERS_DATA = "Offer Data"
const val DETAIL_DATA = "Offer Data"
const val ORDER = "ORDER"
const val ORDER_PROGRESS = "order progress"
const val OFFER_STATUS = "Offer Status"
const val RECENT_WASH = "Recent Wash"
const val UPCOMING_WASH = "Upcoming Wash"

const val REQUEST_DISTANCE = 700
const val ZOOM_LEVEL = 14f
const val SUCCESS_CODE = 200
const val PREF_MAP_VIEW = "pref_map_view"
const val PAYMENT_TYPE = "payment_type"
const val CATEGORY_ID = "category_id"
const val CATEGORY_BRAND_ID = "category_brand_id"
const val TOKEN_ID = "token_id"
const val CATEGORY_PRODUCT_ID = "category_brand_product_id"
const val SERVICES = "services"

const val EXTRA_CAR = "extra car"


object PrefsConstant {
    const val CURRENT_LOCATION = "current_location"
}

object Constants {
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
    const val ADDRESS = "address"
    const val DEFAULT_REASON_CUSTOMER_CANCELLED = "CANCELLING_WHILE_REQUESTING"
    const val PAGE_LIMIT = 20

}

object FCMConstants {
    const val TITLE = "title"
    const val BODY = "body"
    const val MESSAGE = "message"
    const val PUSH_TYPE = "type"


}

object DocType {
    const val PDF = "pdf"
    const val IMAGE = "img"
}

object Services {
    const val TOW = "tow"
    const val GAS = "gas"
    const val DRINKING_WATER = "drinking_water"
    const val WATER_TANKER = "water_tanker"
    const val FREIGHT = "freight"
    const val FIRE_BRIGADE = "fire_brigade"
    const val AMBULANCE = "ambulance"
    const val E_WORKSHOP = "E_WORKSHOP"
}

object CategoryId {
    const val GAS = 1
    const val MINERAL_WATER = 2
    const val WATER_TANKER = 3
    const val FREIGHT = 4
    const val TOW = 5
}

object UserGender {
    const val MALE = 1
    const val FEMALE = 2
    const val OTHER = 3
}

object UserType {
    const val CUSTOMER = "1"
    const val DRIVER = "2"
}


object AppRequestCode {
    const val ADD_CAR = 100
    const val EDIT_PROFILE = 200
    const val CHANGE_PASSWORD = 201
    const val HOME_UPDATE = 202
}

object PaymentType {
    const val CASH = "Cash"
    const val CARD = "Card"
    const val E_TOKEN = "eToken"
}

object ColorType {
    const val GREEN = 1
    const val ORANGE = 2
    const val GREY = 3
    const val RED = 4
    const val BLUE = 5
    const val WHITE = 6
    const val BLACK = 7
    const val BROWN = 8
}


object Events {
    const val ORDER_EVENT = "OrderEvent"
    const val COMMON_EVENT = "CommonEvent"
}

object Broadcast {
    const val NOTIFICATION = "notification"
}


object ServiceStatus {
    val REQUEST = 1
    val CANCLE_BY_USER = 2
    val REJECT_BY_WASHER = 3
    val ACCEPT = 4
    val JOURNEY_STARTED = 5
    val WASHING_STARTED = 6
    val COMPLETED = 7
}


object OrderStatus {
    val SEARCHING = "Searching"
    val ONGOING = "Ongoing"
    val CONFIRMED = "Confirmed"
    val CUSTOMER_CANCEL = "CustCancel"
    val SERVICE_COMPLETE = "SerComplete"
    val SERVICE_INVOICED = "SerInvoiced"
    val SERVICE_TIMEOUT = "SerTimeout"
    val SERVICE_REJECT = "SerReject"
    val DRIVER_CANCELLED = "DriverCancel"
    val SCHEDULED = "Scheduled"
    val DRIVER_PENDING = "DPending"
    val DRIVER_APPROVED = "DApproved"
    val DRIVER_SCHEDULED_CANCELLED = "DSchCancelled"
    val DRIVER_ARROVED = "DApproved"
    val DRIVER_SCHEDULE_SERVICE = "DPending"
    val DRIVER_START = "About2Start"


    val DRIVER_SCHEDULED_TIMEOUT = "DSchTimeout"
    val SYS_SCHEDULED_CANCELLED = "SysSchCancelled"

    val E_TOKEN_START =
        "eTokenSerStart"                         //Driver Starts a etoken Ride for customer(Customer) No status only event
    val CUSTOMER_CONFIRMATION_PENDING_ETOKEN = "SerCustPending" //Waiting for customer confirmation(Customer)
    val CUSTOMER_CANCELLED_ETOKEN = "SerCustCancel"            //Customer Cancels etoken order(Driver)
    val CUSTOMER_CONFIRM_ETOKEN = "SerCustConfirm"            //Customer Confirm etoken order(Driver)
    val C_TIMEOUT_ETOKEN = "CTimeout"                        //Customer Do not confirm or rejects in 2 minutes(Driver)


}

object OrderEventType {
    //    UserData
    val DRIVER_RATED_SERVICE = "DriverRatedService"
    val SERVICE_ACCEPT = "SerAccept"
    val SERVICE_REJECT = "SerReject"
    val SERVICE_COMPLETE = "SerComplete"
    val SERVICE_TIMEOUT = "SerTimeout"
    val CURRENT_ORDERS = "CurrentOrders"
    val CUSTOMER_SINGLE_ORDER = "CustSingleOrder"
    val DRIVER_CANCELLED = "DriverCancel"

    //   Driver
    val SERVICE_REQUEST = "SerRequest"
    val SERVICE_CANCEL = "SerCancel"
    val SERVICE_OTHER_ACCEPT = "SerOAccept"
    val CUSTOMER_RATED_SERVICE = "CustRatedService"
    val DRIVER_SCHEDULE_SERVICE = "DPending"
    val DRIVER_TIME_OUT = "DSchTimeout"
    val DRIVER_ARROVED = "DApproved"
    val DRIVER_START = "About2Start"
    val SYSTEM_CANCELLED = "SysSchCancelled"
    val ONGOING = "Ongoing"
    //types
    val CUSTOMER_CANCEL = "CustCancel"
    val SCHEDULED = "Scheduled"
    val DRIVER_SCHEDULED_CANCELLED = "DSchCancelled"

    // e-tokens
    val E_TOKEN_START =
        "eTokenSerStart"                         //Driver Starts a etoken Ride for customer(Customer) No status only event
    val CUSTOMER_CONFIRMATION_PENDING_ETOKEN = "SerCustPending" //Waiting for customer confirmation(Customer)
    val CUSTOMER_CANCELLED_ETOKEN = "SerCustCancel"            //Customer Cancels etoken order(Driver)
    val CUSTOMER_CONFIRM_ETOKEN = "SerCustConfirm"            //Customer Confirm etoken order(Driver)
    val C_TIMEOUT_ETOKEN = "CTimeout"                        //Customer Do not confirm or rejects in 2 minutes(Driver)

}


object VehicleType {
    const val SMALL = 1
    const val SEDAN = 2
    const val MEDIUM = 3
    const val LARGE_TRUCK = 4
}

object PaymentsType {
    const val OFFER = 1
    const val CASH = 2
    const val CREDIT = 3
}


object SocketConstant {
    //const val LIVE_URL_1 = "http://192.168.102.85:8005/" /*LOCAL*/
    const val LIVE_URL_1 = "http://54.69.127.132:8005/" /*LIVE*/
}