package com.codebrew.mrsteam2.utils

import android.app.Activity
import android.app.Application
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import java.util.*


class MrSteamApp : Application() {


    companion object {
        lateinit var singleton: MrSteamApp
    }

    override fun onCreate() {
        super.onCreate()
        singleton = this
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        PrefsManager.initialize(this)
    }

    fun getInstance(): MrSteamApp {
        if(singleton == null){
            singleton = MrSteamApp()
        }
        return singleton!!
    }


    fun setLocale(activity: Activity) {
        val locale = Locale(PrefsManager.get().getString(LANG, "en"))
        Locale.setDefault(locale)
        val configuration = activity.getResources().getConfiguration()
        val displayMetrics = activity.getResources().getDisplayMetrics()
        configuration.locale = locale
        activity.getResources().updateConfiguration(configuration, displayMetrics)
    }


}