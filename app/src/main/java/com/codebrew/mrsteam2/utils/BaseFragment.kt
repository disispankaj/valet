package com.codebrew.mrsteam2.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    abstract fun onNetworkConnected()

    abstract fun onNetworkDisconnected()

    private val networkChangeReceiver = NetworkChangeReceiver()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val intentFilter = IntentFilter()
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        activity?.registerReceiver(networkChangeReceiver, intentFilter)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(networkChangeReceiver)
    }

    inner class NetworkChangeReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (isConnectedToInternet(context, false)) {
                onNetworkConnected()
            } else {
                onNetworkDisconnected()
            }

        }
    }

}