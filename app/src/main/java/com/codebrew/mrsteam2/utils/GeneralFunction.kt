package com.codebrew.mrsteam2.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.ui.common.splash.SplashActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

/*---Edit Text Validation---*/
fun invalidString(editText: EditText, errorMsg: String) {
    editText.error = errorMsg
    editText.requestFocus()
}

fun invalidStringInput(editText: EditText, tnlEditText: TextInputLayout, errorMsg: String) {
    tnlEditText.error = errorMsg
    editText.requestFocus()
}

fun getScreenWidth(activity: Activity): Int {
    val displayMetrics = DisplayMetrics()
    activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun dpToPx(dp: Int): Float {
    return (dp * Resources.getSystem().displayMetrics.density)
}

fun pxToDp(context: Context, px: Int): Int {
    val displayMetrics = context.resources.displayMetrics
    return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

fun getAccessToken(): String {
    return if (PrefsManager.get().getString(ACCESS_TOKEN, "")?.isEmpty() == true)
        ""
    else
        "Bearer " + PrefsManager.get().getString(ACCESS_TOKEN, "")
}

fun getVehicleResId(categoryId: Int?): Int {
    return when (categoryId) {
        CategoryId.GAS -> R.drawable.notification
        CategoryId.MINERAL_WATER -> R.drawable.notification
        CategoryId.WATER_TANKER -> R.drawable.notification
        CategoryId.FREIGHT -> R.drawable.notification
        CategoryId.TOW -> R.drawable.notification
        else -> R.drawable.notification
    }
}


fun ImageView.setRoundImageUrl(url: String?) {
    val requestOptions = RequestOptions()
        .fitCenter()
        .transform(CircleCrop())
    this.context?.applicationContext?.let { Glide.with(it).load(url).apply(requestOptions).into(this) }
}

fun ImageView.setRoundProfileUrl(url: String?) {
    val requestOptions = RequestOptions()
        .fitCenter()
        .transform(CircleCrop())
        .placeholder(R.drawable.notification)
    this.context?.applicationContext?.let { Glide.with(it).load(url).apply(requestOptions).into(this) }
}

fun ImageView.setRoundProfilePic(file: File?) {
    val requestOptions = RequestOptions()
        .fitCenter()
        .transform(CircleCrop())
        .placeholder(R.drawable.notification)
    this.context?.applicationContext?.let { Glide.with(it).load(file).apply(requestOptions).into(this) }
}

fun getCurentDateStringUtc(): String? {
    val f = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    f.timeZone = TimeZone.getTimeZone("UTC")
    return f.format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).time)
}

fun getFormatFromDate(date: Date, format: String): String? {
    val f = SimpleDateFormat(format, Locale.getDefault())
    return f.format(date)
}

fun getFormatFromDateUtc(date: Date, format: String): String? {
    val f = SimpleDateFormat(format, Locale.US)
    f.timeZone = TimeZone.getTimeZone("UTC")
    return f.format(date)
}


fun View.hideKeyboard() {
    val imm = this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun Context.longToast(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

fun View.showSnack(@StringRes id: Int) {

    try {
        val snackBar = Snackbar.make(this, id, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.ok) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#0078FF"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.showSnack(msg: String) {

    try {
        val snackBar = Snackbar.make(this, msg, Snackbar.LENGTH_LONG)
        val snackBarView = snackBar.view
        val textView = snackBarView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackBar.setAction(R.string.ok) { snackBar.dismiss() }
        snackBarView.setBackgroundColor(Color.parseColor("#27242b"))
        snackBar.setActionTextColor(Color.parseColor("#0078FF"))
        snackBar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun logoutUser(activity: Activity?) {

    /*val notificationManager = activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.cancelAll()*/

    PrefsManager.get().remove(SP_USER_DATA)
    PrefsManager.get().remove(ACCESS_TOKEN)
    activity?.startActivity(
        Intent(activity, SplashActivity::class.java)
            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
    )

    activity?.setResult(Activity.RESULT_CANCELED)
    ActivityCompat.finishAffinity(activity as Activity)
    activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

}


fun Context.getVehicleType(vehicle: String): String {
    return when (vehicle) {
        VehicleType.SMALL.toString() ->
            getString(R.string.str_hatchback)
        VehicleType.MEDIUM.toString() ->
            getString(R.string.str_mpv)
        VehicleType.SEDAN.toString() ->
            getString(R.string.str_sedan)
        VehicleType.LARGE_TRUCK.toString() ->
            getString(R.string.str_suv)
        else ->
            getString(R.string.str_hatchback)
    }
}


fun getFileSizeLess5MB(file: File?): Boolean {
    return (file?.length() ?: 0) < 5125000
}


fun Context.getVehicleColor(type: Int): Int {
    return when (type) {
        ColorType.GREEN -> R.color.car_color_1
        ColorType.ORANGE -> R.color.car_color_2
        ColorType.GREY -> R.color.car_color_3
        ColorType.RED -> R.color.car_color_4
        ColorType.BLUE -> R.color.car_color_5
        ColorType.WHITE -> R.color.car_color_6
        ColorType.BLACK -> R.color.car_color_7
        ColorType.BROWN -> R.color.car_color_8
        else -> R.color.car_color_1
    }
}


fun loadImage(
    activity: Activity?, ivUser: ImageView, thumbnailImage: String?,
    originalImage: String?
) {

    Glide.with(activity as Activity).load(originalImage)
        .apply(RequestOptions().placeholder(R.drawable.ic_profile_picture))
        .thumbnail(Glide.with(activity).load(thumbnailImage)).into(ivUser)

}

val utcFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
val utcFormat2 = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())

fun formatDate(date: String?, format: String?): String {
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(utcFormat.parse(date))
}

fun formatDate2(date: String?, format: String?): String {
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(utcFormat2.parse(date))
}





fun Context.setTimeLap(created_at: Long?): String {
    try {

        var time = ""
        if (created_at != null) {
//            val cal = Calendar.getInstance()
//            cal.timeZone = TimeZone.getTimeZone("UTC")
//            cal.timeInMillis = created_at.toLong()
            val diff = System.currentTimeMillis() - created_at
            val seconds = diff / 1000
            val minutes: Long
            val hours: Long
            val days: Long
            if (seconds < 60) {
                time = this.getString(R.string.now)
            } else {
                minutes = TimeUnit.MILLISECONDS.toMinutes(diff)
                if (minutes < 60) {
                    time = minutes.toString() + " " + this.getString(R.string.min)
                } else {
                    hours = TimeUnit.MILLISECONDS.toHours(diff)
                    if (hours < 24) {
                        time = if (hours == 1L) {
                            hours.toString() + " " + this.getString(R.string.hour)
                        } else
                            hours.toString() + " " + this.getString(R.string.hours)
                    } else {
                        days = TimeUnit.MILLISECONDS.toDays(diff)
                        if (days < 30) {
                            time = if (days == 1L) {
                                days.toString() + " " + this.getString(R.string.day)
                            } else
                                days.toString() + " " + this.getString(R.string.days)

                        } else if (days < 365) {
                            val months = days / 30

                            time = if (months == 1L)
                                months.toString() + " " + this.getString(R.string.month)
                            else
                                months.toString() + " " + this.getString(R.string.months)
                        } else {
                            val years = days / 365
                            time = years.toString() + " " + this.getString(R.string.years)
                        }
                    }
                }
            }
        }

        return time

    } catch (e: Exception) {
        e.printStackTrace()
        return ""
    }


}
