package com.codebrew.mrsteam2.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONException;
import org.json.JSONObject;
import timber.log.Timber;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.codebrew.mrsteam2.utils.GeneralFunctionKt.getAccessToken;

/**
 * Created by Rohit Sharma on 9/8/17.
 */

public class AppSocket {

    private static final AppSocket ourInstance = new AppSocket();
    private Socket mSocket;
    private Timer manualReconnectTimer = new Timer();
    private List<OnMessageReceiver> onMessageReceiverList = new ArrayList<>();

    private List<ConnectionListener> onConnectionListeners = new ArrayList<>();


    private Emitter.Listener onConnect = args -> {
        manualReconnectTimer.cancel();
        Log.e("SOCKET","onConnect");
        Timber.e("AppSocket - onConnect called");
        notifyConnectionListeners(Socket.EVENT_CONNECT);
    };

    private Emitter.Listener onDisconnect = args -> {
        Timber.e("AppSocket - onDisconnect called");
        restartManualReconnection();
        notifyConnectionListeners(Socket.EVENT_DISCONNECT);
    };

    private Emitter.Listener onError = args -> {
        Log.e("SOCKET","onError");
        Timber.e("AppSocket -onError called");
        restartManualReconnection();
        notifyConnectionListeners(Socket.EVENT_ERROR);
    };

    private Emitter.Listener onTimeOut = args -> {
        Log.e("SOCKET","onTimeOut");
        Timber.e("AppSocket -onTimeOut called");
        restartManualReconnection();
        notifyConnectionListeners(Socket.EVENT_CONNECT_TIMEOUT);
    };

    private Emitter.Listener onReconnecting = args -> {
        Log.e("SOCKET","onReconnecting");
        Timber.e("AppSocket -onReconnecting called");
        restartManualReconnection();
        notifyConnectionListeners(Socket.EVENT_RECONNECTING);
    };

    private Emitter.Listener onReconnectError = args -> {
        Log.e("SOCKET","onReconnectError");
        Timber.e("AppSocket -onReconnectError called");
        restartManualReconnection();
        notifyConnectionListeners(Socket.EVENT_RECONNECT_ERROR);
    };

    private AppSocket() {
    }

    public static AppSocket get() {
        return ourInstance;
    }

    public boolean init(Context context) {
        onMessageReceiverList.clear();
        onConnectionListeners.clear();
        try {
            if (mSocket != null) {
                mSocket.off();
                mSocket.close();
            }
            if (!getAccessToken().isEmpty()) {
                IO.Options options = new IO.Options();
                options.forceNew = false;
                options.reconnection = true;
                options.query = "accessToken=" + getAccessToken();
                mSocket = IO.socket(SocketConstant.LIVE_URL_1, options);
                connect();
                mSocket.on(Socket.EVENT_CONNECT, onConnect);
                mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
                mSocket.on(Socket.EVENT_CONNECT_ERROR, onError);
                mSocket.on(Socket.EVENT_ERROR, onError);
                mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onTimeOut);
                mSocket.on(Socket.EVENT_RECONNECTING, onReconnecting);
                mSocket.on(Socket.EVENT_RECONNECT_ERROR, onReconnectError);
                mSocket.on(Socket.EVENT_RECONNECT_FAILED, onReconnectError);
                return true;
            } else {
                return false;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean isConnected() {
        return mSocket.connected();
    }

    private void restartManualReconnection() {
        manualReconnectTimer.cancel();
        manualReconnectTimer = new Timer();
        int MANUAL_RECONNECT_INTERVAL = 10000;
        manualReconnectTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mSocket.io().reconnection(true);
                connect();
                Log.e("SOCKET","ManualReconnection Timer Task");
                Timber.e("AppSocket --> ManualReconnection Timer Task Called");
            }
        }, MANUAL_RECONNECT_INTERVAL);
    }

    public Socket getSocket() {
        if (!mSocket.connected())
            connect();
        return mSocket;
    }

    public void connect() {
        if (!mSocket.connected())
            mSocket.connect();

        Log.i("SockectConnect", "Connected");
    }

    public void disconnect() {
        mSocket.disconnect();
    }

    public void emit(final String event, final Object... args) {
        mSocket.emit(event, args);
    }

    public void on(String event, Emitter.Listener fn) {
        mSocket.on(event, fn);
    }

    public void off() {
        mSocket.off();
    }

    public void off(String event) {
        mSocket.off(event);
    }

    public void off(String event, Emitter.Listener fn) {
        mSocket.off(event, fn);
    }

    /*public void sendMessage(MessageSend message, final OnMessageReceiver msgAck) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(new Gson().toJson(message));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(Events.SEND_MESSAGE, jsonObject, (Ack) args ->
                new Handler(Looper.getMainLooper()).post(() ->
                        msgAck.onMessageReceive(new Gson().fromJson(args[0].toString(), ChatMessage.class))));
    }*/

    public void sendMessageDelivery(String id) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("messageId", id);
            mSocket.emit(Events.ACKNOWLEDGE_MESSAGE, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addConnectionListener(ConnectionListener listener) {
        onConnectionListeners.add(listener);
    }

    public void removeConnectionListener(ConnectionListener listener) {
        onConnectionListeners.remove(listener);
    }

    public void removeAllConnectionListeners() {
        onConnectionListeners.clear();
    }

    private void notifyConnectionListeners(final String status) {
        for (final ConnectionListener listener : onConnectionListeners) {
            new Handler(Looper.getMainLooper()).post(() -> listener.onConnectionStatusChanged(status));
        }
    }

    public void addOnMessageReceiver(OnMessageReceiver receiver) {
        if (onMessageReceiverList.isEmpty()) {
            onReceiveMessageEvent();
        }
        onMessageReceiverList.add(receiver);
    }

    public void removeOnMessageReceiver(OnMessageReceiver receiver) {
        onMessageReceiverList.remove(receiver);
        if (onMessageReceiverList.isEmpty()) {
            mSocket.off(Events.RECEIVE_MESSAGE);
        }
    }

    public void removeAllMessageReceivers() {
        onMessageReceiverList.clear();
        mSocket.off(Events.RECEIVE_MESSAGE);
    }

    private void onReceiveMessageEvent() {
        mSocket.on(Events.RECEIVE_MESSAGE, args -> {
           /* try {
                ChatMessage chat;
                chat = new Gson().fromJson(((JSONArray) args[0]).get(0).toString(), ChatMessage.class);
                notifyMessageReceivers(chat);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        });
    }

    private void notifyMessageReceivers(final String message) {
        for (final OnMessageReceiver receiver : onMessageReceiverList) {
            new Handler(Looper.getMainLooper()).post(() -> receiver.onMessageReceive(message));
        }
    }

    public interface Events {
        String SEND_MESSAGE = "sendMessage";
        String RECEIVE_MESSAGE = "messageFromServer";
        String TYPING = "typing";
        String BROADCAST = "broadcast";
        String ACKNOWLEDGE_MESSAGE = "acknowledgeMessage";
        String ORDER_EVENT = "orderEvent";
        String COMMON_EVENT = "COMMON_EVENT";
    }

    public interface OnMessageReceiver {
        /*Need to update*/
        void onMessageReceive(String message);
    }

    public interface ConnectionListener {
        void onConnectionStatusChanged(String status);
    }
}

