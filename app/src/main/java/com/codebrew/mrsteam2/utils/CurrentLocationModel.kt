package com.codebrew.mrsteam2.utils

data class CurrentLocationModel(val latitude: Double?, val longitude: Double?, val address: String?)