package com.codebrew.mrsteam2.network.responsemodels

data class SignUp(
        val user: UserData,
        val otp: String,
        val token: String,
        val is_exist: Boolean
)
