package com.codebrew.mrsteam2.network

import com.codebrew.mrsteam2.network.common.ApiResponse
import com.codebrew.mrsteam2.network.requestmodels.*
import com.codebrew.mrsteam2.network.responsemodels.*
import com.codebrew.mrsteam2.network.responsemodels.notification.NotificationDataItem
import com.codebrew.mrsteam2.network.responsemodels.order.CurrentOrder
import com.codebrew.mrsteam2.ui.customer.booking.promocode.Promo
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface MrSteamApi {

    companion object {

        private const val LOGIN = "login"
        private const val SOCIAL_LOGIN = "socialLogin"
        private const val REGISTER = "register"
        private const val VERIFY_PHONE = "verifyPhone"
        private const val CHECK_OFFER = "checkOffer"
        private const val RESEND_OTP = "resendOtp"
        private const val FORGOT_PASSWORD = "forgotPassword"
        private const val RESET_PASSWORD = "reset-password"
        private const val LOGOUT = "logout"
        private const val EDIT = "edit"
        private const val CHANGE_PASSWORD = "changePassword"
        private const val MY_CARS = "myCars"
        private const val ALL_OFFERS = "allOffers"
        private const val OFFERS = "offers"
        private const val HOME = "home"
        private const val PROFILE_IMAGE = "profileImage"
        private const val REQUESTS = "wash_requests"
        private const val WASHER_REQUESTS = "wash_requests"
        private const val ACCEPT_REJECT_REQUEST = "wash_requests/{wash_request}"
        private const val ALL_WASHES = "allWashes"
        private const val SERVICES = "services"
        private const val WASH_DETAILS = "wash_requests/{wash_request}"
        private const val UPDATE_DETAILS = "updateDetails"
        private const val VALIDATE_PROMOCODE = "check-promo"
    }


    /*POST*/
    @POST(REGISTER)
    fun signUp(@Body obj: SignupRequest)
            : Call<ApiResponse<SignUp>>

    @FormUrlEncoded
    @POST(REQUESTS)
    fun requests(@FieldMap hashMap: HashMap<String, Any>): Call<ApiResponse<Wash>>

    @POST(VERIFY_PHONE)
    fun verifyNumber(@Body obj: VerifyNumberRequest)
            : Call<ApiResponse<UserData>>

    @POST(LOGIN)
    fun logIn(@Body obj: LoginRequest): Call<ApiResponse<UserData>>

    @FormUrlEncoded
    @POST(SOCIAL_LOGIN)
    fun socialLogin(@FieldMap hashMap: HashMap<String, Any>): Call<ApiResponse<SignUp>>

    @POST(FORGOT_PASSWORD)
    fun forgotPassword(@Body obj: ForgotPasswordRequest)
            : Call<ApiResponse<ForgotPassword>>

    @POST(RESET_PASSWORD)
    fun resetPassword(@Body obj: ResetPasswordRequest)
            : Call<ApiResponse<UserData?>>

    @POST(EDIT)
    fun editProfile(@Body obj: EditProfileRequest)
            : Call<ApiResponse<UserData>>

    @POST(CHANGE_PASSWORD)
    fun changePassword(@Body obj: ChangePasswordRequest)
            : Call<ApiResponse<UserData?>>

    @POST(LOGOUT)
    fun logOut(): Call<ApiResponse<UserData?>>


    @POST(MY_CARS)
    fun addCar(@Body obj: AddCarRequest): Call<ApiResponse<AddCar>>

    @FormUrlEncoded
    @POST(CHECK_OFFER)
    fun checkOffer(@FieldMap hashMap: HashMap<String, Any>): Call<ApiResponse<CheckOffer>>


    @POST(OFFERS)
    fun addOffers(@Body obj: AddOfferRequest): Call<ApiResponse<Offers>>


    @POST(ALL_WASHES)
    fun allWashes(@Query("status") status: Int): Call<ApiResponse<Home>>

    @FormUrlEncoded
    @POST(UPDATE_DETAILS)
    fun updateDetails(@FieldMap hashMap: HashMap<String, Any>): Call<ApiResponse<Wash>>


    /*GET*/
    @GET(RESEND_OTP)
    fun resendOtp(@Query("phone") phone: String, @Query("country_code") country_code: String)
            : Call<ApiResponse<ResendOtp>>

    @GET(MY_CARS)
    fun myCars(): Call<ApiResponse<ArrayList<MyCars>>>

    @GET(HOME)
    fun home(): Call<ApiResponse<Home>>

    @GET(ALL_OFFERS)
    fun allOffers(): Call<ApiResponse<ArrayList<Offers>>>

    @GET(OFFERS)
    fun myOffers(): Call<ApiResponse<ArrayList<Offers>>>

    @GET(WASHER_REQUESTS)
    fun washRequests(): Call<ApiResponse<CurrentOrder>>

    @GET(WASH_DETAILS)
    fun washDetails(@Path("wash_request") wash_requests: Int): Call<ApiResponse<Wash>>

    @GET(SERVICES)
    fun services(): Call<ApiResponse<ArrayList<Services>>>


    /*PUT*/
    @PUT("myCars/{myCar}")
    fun editCar(@Path("myCar") myCar: Int, @Body obj: AddCarRequest): Call<ApiResponse<AddCar>>

    @FormUrlEncoded
    @PUT(ACCEPT_REJECT_REQUEST)
    fun acceptRejectRequest(
        @Path("wash_request") wash_requests: Int,
        @FieldMap hashMap: HashMap<String, Any>
    ): Call<ApiResponse<Wash>>

    /*DELETE*/
    @DELETE("myCars/{myCar}")
    fun deleteCar(@Path("myCar") myCar: Int): Call<ApiResponse<AddCar>>

    @Multipart
    @POST(PROFILE_IMAGE)
    fun profileImage(@PartMap map: HashMap<String, RequestBody>): Call<ApiResponse<ImageModel>>


    @GET("directions/json?sensor=false&mode=driving&alternatives=false&units=metric&key=AIzaSyDFFIxaqdT4owCUufjABb5T0bIuqvbAaPs")
    fun getPolYLine(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("language") language: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("reviews")
    fun submitReview(@FieldMap map: HashMap<String, String>): Call<ApiResponse<Wash>>


    @GET("notifications")
    fun notification(): Call<ApiResponse<ArrayList<NotificationDataItem>>>


    @PUT("read-notification/{id}")
    fun readNotification(@Path("id") string: String): Call<ApiResponse<Any>>


    @GET(VALIDATE_PROMOCODE)
    fun validatePromoCode(@Query("min_amount") min_price: String, @Query("promo") promo: String): Call<ApiResponse<Promo>>

}