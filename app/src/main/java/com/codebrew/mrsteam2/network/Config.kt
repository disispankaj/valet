package com.codebrew.mrsteam2.network

object Config {


    private var BASE_URL = ""
    private var IMAGE_PATH = ""

    private val appMode = AppMode.TEST

    internal val baseURL: String
        get() {
            init(appMode)
            return BASE_URL
        }

    internal val imagePath: String
        get() {
            init(appMode)
            return IMAGE_PATH
        }

    private fun init(appMode: AppMode) {

        when (appMode) {
            AppMode.TEST -> {
               // BASE_URL = "http://18.220.7.33/api/"
                BASE_URL = "http://192.168.102.85:8000"
                IMAGE_PATH = "http://45.232.252.44:8000"
            }
            AppMode.LOCAL -> {
                BASE_URL = "http://18.220.7.33/api/"
                IMAGE_PATH = "http://192.168.102.109:8000"
            }
            AppMode.DEV -> {
                BASE_URL = "http://192.168.102.85:8000/"
                IMAGE_PATH = "http://45.232.252.37:8000"
            }
            AppMode.LIVE -> {
                BASE_URL = "http://18.220.7.33/api/"
                IMAGE_PATH = "https://s3.us-east-2.amazonaws.com/ejarinow/"
            }
        }
    }

    private enum class AppMode {
        LOCAL, DEV, TEST, LIVE
    }
}