package com.codebrew.mrsteam2.network.responsemodels.order

data class Rating(
        var order_rating_id: Int,
        var ratings: Int,
        var comments: String,
        var created_by: String,
        var created_at: String
)