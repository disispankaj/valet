package com.codebrew.mrsteam2.network.responsemodels

class CheckOffer {

    var id: Int? = null
    var offer_id: Int? = null
    var user_id: Int? = null
    var starts_at: String? = null
    var expires_at: String? = null
    var status: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var is_having_offer: Boolean? = null
    var price: Int? = null
    var sub_service: SubService? = null
}