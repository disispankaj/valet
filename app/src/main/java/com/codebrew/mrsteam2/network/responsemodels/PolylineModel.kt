package com.codebrew.mrsteam2.network.responsemodels

data class PolylineModel(
        var points: String?,
        var distanceText: String?,
        var distanceValue: Int?,
        var timeText: String?,
        var timeValue: Int?)
