package com.codebrew.mrsteam2.network.responsemodels.notification

import com.codebrew.mrsteam2.network.responsemodels.ImageModel

data class Data(
	val wash_request_id: Int? = null,
	val wash_status: String? = null,
	val image: ImageModel? = null,
	val message: String? = null
)
