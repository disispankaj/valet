package com.codebrew.mrsteam2.network.responsemodels

import java.io.Serializable

class Active_sub_service : Serializable {

    var id: Int? = null
    var service_id: Int? = null
    var title: String? = null
    var description: String? = null
    var status: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var isSelected = false
    var ar_title: String? = null
    var ar_description: String? = null
    var price: Int? = null
    var duration: Int? = null
    var dual_title: String? = null
    var dual_description: String? = null

}