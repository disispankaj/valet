package com.codebrew.mrsteam2.network.responsemodels

data class Brand(
        var category_brand_id: Int?,
        var sort_order: Int?,
        var category_id: Int?,
        var name: String?,
        var image: String?,
        var image_url: String?
) {
    override fun toString(): String {
        return name ?: ""
    }
}