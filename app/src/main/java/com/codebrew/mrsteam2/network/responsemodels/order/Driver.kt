package com.codebrew.mrsteam2.network.responsemodels.order

data class Driver(
        var phone_number: Long?,
        var user_id: Int?,
        var name: String?,
        var user_detail_id: Int?,
        var user_type_id: Int?,
        var category_id: Int?,
        var category_brand_id: Int?,
        var profile_pic: String?,
        var latitude: Double?,
        var longitude: Double?,
        var profile_pic_url: String?,
        var rating_count: Int?,
        var rating_avg: String
)