package com.codebrew.mrsteam2.network.responsemodels

data class AddCar(
        val model: String,
        val year: String,
        val plate_number: String,
        val vehicle_type: String,
        val user_id: String,
        val created_at: String,
        val updated_at: String,
        val id: Int
)
