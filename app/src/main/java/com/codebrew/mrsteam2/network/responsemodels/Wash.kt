package com.codebrew.mrsteam2.network.responsemodels

import com.codebrew.mrsteam2.network.responsemodels.order.*
import com.codebrew.mrsteam2.network.responsemodels.order.Brand
import java.io.Serializable
import java.math.BigInteger


class Wash : Serializable {

    var id: Int? = null
    var user_id: Int? = null
    var washer_id: Int? = null
    var my_car_id: Int? = null
    var sub_service_id: Int? = null
    var lat: Double? = null
    var long: Double? = null
    var address: String? = null
    var booking_type: String? = null
    var required_at: String? = null
    var status: Int? = null
    var payment_type: Int? = null
    var created_at: String? = null
    var updated_at: String? = null
    var user: UserData? = null
    var washer: UserData? = null
    var my_car: MyCars? = null
    var sub_service: SubService? = null
    var time_left_in_mins: Int? = null

    var time: Int? = null
    var socket: String? = null
    var name: String? = null
    var distance: Float? = null
    var request_id: Int? = null
    var wash_request_id: Int? = null
    var need_confirmation: Boolean? = null
    var message: String? = null

    var order_id: BigInteger? = null
    var order_token: String? = null
    var customer_user_id: Int? = null
    var customer_user_detail_id: Int? = null
    var customer_organisation_id: Int? = null
    var customer_user_type_id: Int? = null
    var driver_user_id: Int? = null
    var driver_user_detail_id: Int? = null
    var driver_organisation_id: Int? = null
    var driver_user_type_id: Int? = null
    var category_id: Int? = null
    var category_brand_id: Int? = null
    var category_brand_product_id: Int? = null
    var continuous_order_id: Int? = null
    var order_status: String? = null
    var pickup_address: String? = null
    var pickup_latitude: Double? = null
    var pickup_longitude: Double? = null
    var dropoff_address: String? = null
    var dropoff_latitude: Double? = null
    var dropoff_longitude: Double? = null
    var order_timings: String? = null
    var future: Int? = null
    var cancel_reason: String? = null
    var cancelled_by: String? = null
    var organisation_coupon_user_id: Int? = null
    var coupon_user_id: Int? = null
    var track_path: String? = null
    var track_image: String? = null
    var payment: Payment? = null
    var coupon_user: Any? = null
    var organisation_coupon_user: OrganisationCouponUser? = null
    var brand: Brand? = null
    var driver: Driver? = null
    var cRequest: Timings? = null
    var ratingByUser: Rating? = null
    var ratingByDriver: Rating? = null
    val my_turn: String? = null
    var timeProgress: Int? = null
    var is_reviewed: Boolean? = false
    var rating: String? = null

}