package com.codebrew.mrsteam2.network.responsemodels

import java.io.Serializable

class Offers : Serializable {
    var id: Int? = null
    var name: String? = null
    var ar_name: String? = null
    var description: String? = null
    var ar_description: String? = null
    var price: Int? = null
    var valid_days: Int? = null
    var status: String? = null
    var is_deleted: Int? = null
    var created_at: String? = null
    var updated_at: String? = null
    var dual_name: String? = null
    var dual_description: String? = null
    var pivot: Pivot? = null
    var offer: Offer? = null
    var is_purchased:Boolean = false
    var active_details: ArrayList<ActiveDetails>? = null
    var active_sub_services: ArrayList<ActiveDetails>? = null

}
class Offer : Serializable {
    var id: Int? = null
    var price: Int? = null
    var valid_days: Int? = null
    var is_deleted: Int? = null
    var name: String? = null
    var description: String? = null
    var ar_description: String? = null
    var is_purchased:Boolean = false
    var status: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var dual_name: String? = null
    var dual_description: String? = null
}


class Pivot : Serializable {
    var offer_id: Int? = null
    var sub_service_id: Int? = null
    var status: String? = null
    var service_count: Int? = null
    var created_at: String? = null
    var updated_at: String? = null
}

class ActiveDetails : Serializable {
    var id: Int? = null
    var user_offer_id: Int? = null
    var sub_service_id: Int? = null
    var total_services: Int? = null
    var remaining_services: Int? = null
    var duration: Int? = null
    var status: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var sub_service: SubService? = null
    var service_id: Int? = null
    var title: String? = null
    var ar_title: String? = null
    var description: String? = null
    var ar_description: String? = null
    var price: Int? = null
    var is_deleted: Int? = null
    var dual_title: String? = null
    var dual_description: String? = null
    var pivot: Pivot? = null
    var service: Service? = null
}
