package com.codebrew.mrsteam2.network.requestmodels

import com.codebrew.mrsteam2.network.responsemodels.ImageModel

data class AddCarRequest(
        val myCar: Int? = null,
        val model: String,
        val year: String,
        val plate_number: String,
       /* val color: Int,*/
        val vehicle_type: Int,
        var image: ImageModel? = null
)