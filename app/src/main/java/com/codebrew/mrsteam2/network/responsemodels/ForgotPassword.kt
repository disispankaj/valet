package com.codebrew.mrsteam2.network.responsemodels

import com.google.gson.annotations.SerializedName


data class ForgotPassword(
        @SerializedName("otp")
        val otp: Int? = 0,
        @SerializedName("user_id")
        val user_id: Int? = 0
)