package com.codebrew.mrsteam2.network.requestmodels

import com.codebrew.mrsteam2.network.responsemodels.Active_sub_service
import com.codebrew.mrsteam2.network.responsemodels.CheckOffer
import com.codebrew.mrsteam2.network.responsemodels.Services
import com.codebrew.mrsteam2.utils.REQUEST_DISTANCE
import java.util.*
import kotlin.collections.ArrayList

class ServiceRequestModel {
    companion object {
        val BOOK_NOW = 1
        val SCHEDULE = 2
    }

    var dropoff_address: String? = ""
    var dropoff_latitude: Double? = 0.0
    var dropoff_longitude: Double? = 0.0
    var carId: Int? = -1
    var carName: String? = ""
    var serviceId: Int? = -1
    var serviceName: String = ""
    var services: ArrayList<Services>? = null
    var servicePosition: Int? = -1
    var subService: Int? = -1
    var subserviceId: ArrayList<Active_sub_service>? = null
    var paymentType: Int? = 0
    var tokenConnekta: String?=""
    var checkOffer: CheckOffer? = null
    var future: Int? = -1
    var orderTime: Date? = Date()
    var order_timings: String? = ""
    var order_timings_text: String? = ""

    var category_id: Int? = -1
    var category_brand_id: Int? = -1
    var category_brand_product_id: Int? = -1
    var product_quantity: Int? = 1
    var pickup_address: String? = ""

    var brandName: String? = ""
    var productName: String? = ""
    var pickup_latitude: Double? = 0.0
    var pickup_longitude: Double? = 0.0

    var payment_type: String? = ""
    var distance: Int? = REQUEST_DISTANCE
    var price_per_item: Float? = 0f
    var final_charge: Double? = 0.0
    var organisation_coupon_user_id: Int? = null
    var coupon_user_id: Int? = null
    var isCurrentLocation = false
    var material_details: String? = ""
    var details: String? = ""
    var product_weight: Int? = 0
    var order_distance: Float? = 0f
    var images: ArrayList<String> = ArrayList()

}