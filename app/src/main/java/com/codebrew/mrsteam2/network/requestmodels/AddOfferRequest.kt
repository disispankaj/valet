package com.codebrew.mrsteam2.network.requestmodels

data class AddOfferRequest(
    val offer_id: Int?,
    val token: String
)