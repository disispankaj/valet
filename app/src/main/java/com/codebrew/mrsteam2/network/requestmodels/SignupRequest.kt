package com.codebrew.mrsteam2.network.requestmodels

data class SignupRequest(
        val provider_user_id: String,
        val provider: String,
        val name: String,
        val country_code: Int,
        val phone: String,
        val gender: Int,
        val password: String,
        var email: String? = null,
        val device_type: Int,
        val device_token: String?
)