package com.codebrew.mrsteam2.network.requestmodels

data class LoginRequest(
        val input: String,
        val password: String,
        val device_type: Int,
        var country_code: Int,
        val device_token: String
)