package com.codebrew.mrsteam2.network

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.common.AppError
import com.codebrew.mrsteam2.utils.logoutUser
import timber.log.Timber


object ApisRespHandler {

    private var alertDialog: AlertDialog.Builder? = null

    fun handleError(error: AppError?, activity: Activity?) {
        error ?: return

        Timber.w(error.toString())

        when (error) {
            is AppError.ApiError -> {
                if (alertDialog == null)
                    errorMessage(activity, error.message)
            }

            is AppError.ApiUnauthorized -> {
                if (alertDialog == null)
                    sessionExpired(activity, error.message)
            }

            is AppError.ApiAccountBlock -> {
                if (alertDialog == null)
                    accountDeleted(activity, error.message)
            }

            is AppError.ApiAccountRuleChanged -> {
                if (alertDialog == null)
                    sessionExpired(activity, error.message)
            }

            is AppError.ApiFailure -> {
                if (alertDialog == null)
                    errorMessage(activity, error.message)
            }

            is AppError.ApiInternalServerError ->{
                if(alertDialog == null)
                    errorMessage(activity,error.message)
            }
        }
    }


    private fun sessionExpired(activity: Activity?, message: String?) {
        try {
            alertDialog = AlertDialog.Builder(activity as Activity)
            alertDialog?.setCancelable(false)
            alertDialog?.setTitle(activity.getString(R.string.alert))
            alertDialog?.setMessage(message)
            alertDialog?.setPositiveButton(activity.getString(R.string.ok)) { _, _ ->
                logoutUser(activity)
                alertDialog = null
            }
            alertDialog?.show()
        } catch (ignored: Exception) {
        }
    }

    private fun accountDeleted(activity: Activity?, message: String?) {
        try {


        } catch (ignored: Exception) {
        }
    }

    private fun errorMessage(activity: Activity?, message: String?) {
        try {
            alertDialog = AlertDialog.Builder(activity as Activity)
            alertDialog?.setCancelable(false)
            alertDialog?.setTitle(activity.getString(R.string.alert))
            alertDialog?.setMessage(message)
            alertDialog?.setPositiveButton(activity.getString(R.string.ok)) { _, _ ->
                alertDialog = null
            }
            alertDialog?.show()

        } catch (ignored: Exception) {
        }

    }
}