package com.codebrew.mrsteam2.network.responsemodels.homeapi

import com.buraq24.customer.webservices.models.homeapi.Category
import com.buraq24.customer.webservices.models.homeapi.Details
import com.buraq24.customer.webservices.models.homeapi.HomeDriver
import com.codebrew.mrsteam2.network.responsemodels.Versioning
import com.codebrew.mrsteam2.network.responsemodels.Wash


data class ServiceDetails(
        val categories: List<Category>?,
        val drivers: List<HomeDriver>?,
        val Details: Details?,
        val currentOrders: List<Wash>?,
        val lastCompletedOrders: List<Wash>?,
        val Versioning: Versioning?
)