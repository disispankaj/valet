package com.codebrew.mrsteam2.network.requestmodels

data class ResetPasswordRequest(
        val user_id: Int,
        val otp: Int,
        val password: String
)