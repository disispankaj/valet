package com.codebrew.mrsteam2.network.requestmodels

import com.codebrew.mrsteam2.network.responsemodels.ImageModel

data class EditProfileRequest(
        val name: String,
        val country_code: String? = null,
        val phone: String? = null,
        val gender: Int,
        var email: String? = null,
        var image: ImageModel? = null
)