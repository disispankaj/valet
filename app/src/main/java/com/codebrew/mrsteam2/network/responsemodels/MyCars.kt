package com.codebrew.mrsteam2.network.responsemodels

import java.io.Serializable

class MyCars : Serializable {

    var id: Int? = null
    var user_id: Int? = null
    var model: String? = null
    var year: Int? = null
    var color: String? = null
    var image: String? = null
    var thumbnail_image: String? = null
    var image_url: ImageModel? = null
    var plate_number: String? = null
    var vehicle_type: String? = null
    var status: String? = null
    var is_deleted: Int? = null
    var created_at: String? = null
    var updated_at: String? = null

}

