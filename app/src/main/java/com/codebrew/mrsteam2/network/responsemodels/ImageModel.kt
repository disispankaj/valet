package com.codebrew.mrsteam2.network.responsemodels

import java.io.Serializable

class ImageModel : Serializable {
    var original: String? = null
    var thumbnail: String? = null
}