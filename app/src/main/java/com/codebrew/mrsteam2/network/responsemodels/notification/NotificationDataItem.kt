package com.codebrew.mrsteam2.network.responsemodels.notification

data class NotificationDataItem(
	val data: Data? = null,
	val updated_at: String? = null,
	var read_at: Any? = null,
	val created_at: String? = null,
	val id: String? = null,
	val notifiableId: Int? = null,
	val type: String? = null,
	val notifiableType: String? = null

)
