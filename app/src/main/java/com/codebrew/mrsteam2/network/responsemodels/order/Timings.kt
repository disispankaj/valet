package com.codebrew.mrsteam2.network.responsemodels.order

data class Timings(
        var accepted_at: String?,
        var confirmed_at: String?,
        var started_at: String?,
        var updated_at: String?
)