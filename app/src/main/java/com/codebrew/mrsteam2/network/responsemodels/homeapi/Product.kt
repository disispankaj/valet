package com.buraq24.customer.webservices.models.homeapi

data class Product(
        var category_brand_product_id: Int?,
        var name: String?,
        var description: String?,
        var sort_order: Int?,
        var actual_value: Float?,
        var alpha_price: Float?,
        var price_per_quantity: Float?,
        var price_per_distance: Float?,
        var price_per_weight: Float?,
        val image_url: String? = null,
        var isSelected: Boolean? = false,
        var min_quantity: Int?,
        var max_quantity: Int
)