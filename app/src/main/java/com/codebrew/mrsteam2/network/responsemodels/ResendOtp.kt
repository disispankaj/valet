package com.codebrew.mrsteam2.network.responsemodels

import com.google.gson.annotations.SerializedName


data class ResendOtp(
        @SerializedName("otp")
        val otp: String,
        @SerializedName("user_id")
        val user_id: String
)