package com.codebrew.mrsteam2.network

import com.codebrew.mrsteam2.utils.getAccessToken
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit



//const val BASE_URL = "http://192.168.102.85:8300/api/"    /*LOCAL*/
const val BASE_URL = "http://54.69.127.132/api/"    /*LIVE*/

//>>>>>>> Stashed changes

object RetrofitClient {

    private var api: MrSteamApi

    init {
        val retrofit = initRetrofitClient()
        api = retrofit.create(MrSteamApi::class.java)
    }

    fun getApi(): MrSteamApi = api

    private fun initRetrofitClient(): Retrofit {
        //System.setProperty("http.keepAlive", "false")

        val client = OkHttpClient.Builder()
            .connectTimeout(45, TimeUnit.SECONDS)
            .readTimeout(45, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()

                val accessToken = getAccessToken()
                println("ACCESS_TOKEN "+accessToken)
                if (accessToken.isNotEmpty())
                    request.addHeader("Authorization", accessToken)
                chain.proceed(request.build())
            }
            .build()

        val gson = GsonBuilder()
            .setLenient()
            .create()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
    }
}