package com.codebrew.mrsteam2.network.common

data class ApiResponse<out T>(
        val msg: String? = null,
        val message: String? = null,
        val status: Int? = null,
        val data: T? = null
)