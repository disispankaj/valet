package com.codebrew.mrsteam2.network.responsemodels

import android.animation.ValueAnimator
import com.google.android.gms.maps.model.Marker

class HomeMarkers {
    var marker: Marker? = null
    var moveValueAnimate: ValueAnimator? = null
    var rotateValueAnimator: ValueAnimator? = null
}