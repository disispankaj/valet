package com.codebrew.mrsteam2.network.responsemodels

data class VersioningDetail(
        var force: Float,
        var normal: Float,
        var user: VersioningDetail
)