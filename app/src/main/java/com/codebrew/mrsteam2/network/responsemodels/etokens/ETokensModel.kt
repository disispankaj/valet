package com.buraq24.customer.webservices.models.etokens

import com.codebrew.mrsteam2.network.responsemodels.etokens.Brand

data class ETokensModel(
        var history: List<History>?,
        var brands: List<Brand>?
)