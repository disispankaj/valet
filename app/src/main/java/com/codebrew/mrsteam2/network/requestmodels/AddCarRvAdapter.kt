package com.codebrew.mrsteam2.network.requestmodels

data class AddCarRvAdapter(
        val type: Int? = null,
        var isSelected: Boolean = false
)