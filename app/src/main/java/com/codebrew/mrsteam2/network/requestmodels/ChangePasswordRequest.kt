package com.codebrew.mrsteam2.network.requestmodels

data class ChangePasswordRequest(
        val old_password: String,
        val password: String,
        val password_confirmation: String
)