package com.codebrew.mrsteam2.network.responsemodels

class Washer {
    var id: Int? = null
    var name: String? = null
    var role: String? = null
    var email: String? = null
    var phone: String? = null
    var country_code: String? = null
    var gender: String? = null
    var email_verified_at: String? = null
    var language: String? = null
    var image: String? = null
    var thumbnail_image: String? = null
    var otp: String? = null
    var address: String? = null
    var lat: String? = null
    var long: String? = null
    var timezone: String? = null
    var device_token: String? = null
    var device_type: String? = null
    var push_notifications: Int? = null
    var is_phone_verified: Int? = null
    var is_deleted: Int? = null
    var status: String? = null
    var last_logged_in: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var image_url: String? = null
    var full_phone: String? = null
}

