package com.codebrew.mrsteam2.network.responsemodels

class Home {
    val offers_count: Int? = null
    val unread_notifications_count: Int? = null
    val offers: ArrayList<Offers>? = null
    val upcoming_washes_count: Int? = null
    val upcoming_washes: ArrayList<Wash>? = null
    val recent_washes_count: Int? = null
    val recent_washes: ArrayList<Wash>? = null
}
