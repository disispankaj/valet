package com.codebrew.mrsteam2.network.requestmodels

data class VerifyNumberRequest(
        val otp: String?,
        val user_id: String?
)