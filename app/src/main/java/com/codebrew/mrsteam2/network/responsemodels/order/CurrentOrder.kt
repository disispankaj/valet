package com.codebrew.mrsteam2.network.responsemodels.order

import com.codebrew.mrsteam2.network.responsemodels.Wash

data class CurrentOrder(
        var searching_requests: List<Wash>,
        var upcoming_requests: List<Wash>,
        var accepted_requests: List<Wash>
)