package com.codebrew.mrsteam2.network.responsemodels

import java.io.Serializable

class Services : Serializable {

    var id: Int? = null
    var name: String? = null
    var price: Int? = null
    var duration: Int? = null
    var is_deleted: Int? = null
    var status: String? = null
    var created_at: String? = null
    var updated_at: String? = null
    var active_sub_services: ArrayList<Active_sub_service>? = null
    var isSelected = false
    var ar_name: String? = null
    var dual_name: String? = null
}
