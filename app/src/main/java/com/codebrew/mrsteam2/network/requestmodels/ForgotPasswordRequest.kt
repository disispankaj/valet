package com.codebrew.mrsteam2.network.requestmodels

data class ForgotPasswordRequest(
        val input: String,
        val country_code: Int
)