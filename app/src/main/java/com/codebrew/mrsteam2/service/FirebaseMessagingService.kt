package com.codebrew.mrsteam2.service

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import com.codebrew.mrsteam2.R
import com.codebrew.mrsteam2.network.responsemodels.UserData
import com.codebrew.mrsteam2.ui.common.main.MainActivity
import com.codebrew.mrsteam2.utils.FCMConstants
import com.codebrew.mrsteam2.utils.PrefsManager
import com.codebrew.mrsteam2.utils.SP_USER_DATA
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject
import java.util.*

class FirebaseMessagingService : FirebaseMessagingService() {


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        /*   val jsonObject = JSONObject(remoteMessage?.data)
           Log.e(TAG, remoteMessage?.data.toString())*/
        sendNotification(remoteMessage)

    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun sendNotification(remoteMsg: RemoteMessage?) {

        val notification= JSONObject(remoteMsg?.data)
        val msg = notification.getString(FCMConstants.MESSAGE)
        val title = notification.getString(FCMConstants.TITLE)
        val type = notification.getInt(FCMConstants.PUSH_TYPE)

        val requestID = Calendar.getInstance().timeInMillis.toInt()
        val channelID = "10"


        /*Stack builder home activity*/
        val userType = PrefsManager.get().getObject(SP_USER_DATA, UserData::class.java)?.role

        val stackBuilder = TaskStackBuilder.create(this)

        stackBuilder.addParentStack(MainActivity::class.java)
        val homeIntent = Intent(this, MainActivity::class.java)

        stackBuilder.addNextIntent(homeIntent)

        /*Final activity to open*/
        var intent = Intent()

        when (type) {
            /* PushType.INVITE_USER -> {
                 intent = Intent(this, PropertyDetailActivity::class.java)
                 intent.putExtra(PROPERTY_ID_PARAM, notification.get("property").toString())
                 intent.putExtra(INVOICE_DATE, notification.get("invoiceDate").toString())
                 intent.putExtra(CONTRACT_PERIOD, notification.get("contractPeriod").toString())
                 intent.putExtra(INVITE_ID, notification.get("inviteId").toString())
             }*/
        }

        stackBuilder.addNextIntent(intent)

        /*val pendingIntent = PendingIntent.getActivity(this, requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)*/

        /*Flags*/
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        homeIntent.action = System.currentTimeMillis().toString()

        val pendingIntent = stackBuilder.getPendingIntent(requestID, PendingIntent.FLAG_UPDATE_CURRENT)


        /*val pendingIntent = PendingIntent.getActivity(this, requestID,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)*/

        val notificationBuilder = NotificationCompat.Builder(this, channelID)
                .setContentTitle(title) //Header
                .setContentText(msg) //Content
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
            notificationBuilder.color = ContextCompat.getColor(this, R.color.colorAccent)
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(channelID, getText(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH)

            notificationManager.createNotificationChannel(mChannel)
        }

        notificationManager.notify(requestID, notificationBuilder.build())
    }

}